/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.BorderLayout;

import javax.swing.*;

public final class StringAreaProperty extends PropertyInterface
  {
  private final String key;
  private final JPanel workPanel;
  private final JTextArea inputField;
  private String value;
  
  public StringAreaProperty(String key, String label, String defaultValue, int columns, int rows )
    {
    this.key = key;
    value = defaultValue;

    workPanel = new JPanel(new BorderLayout());
    
    workPanel.add(new JLabel(label),BorderLayout.WEST);

    inputField=new JTextArea(value,rows,columns);

    workPanel.add(new JScrollPane(inputField),BorderLayout.CENTER);
    }
    
  /**
   * This is what will go into the main panel.
   */
  public JComponent getComponentToDisplay() 
    {
    return workPanel;
    }

  public String getKey ()
    {
    return key;
    }

  /**
   * Implements the interface.
   */
  public void setStringValue ( String value )
    {
    this.value = value;
    inputField.setText(value);
    }
  
  /**
   * Implements the interface.
   */
  public String getStringValue ()
    {
    return value = inputField.getText();
    }

  /**
   * Implements the interface.
   */
  public Object getValue ()
    {
    return value;
    }
  }
