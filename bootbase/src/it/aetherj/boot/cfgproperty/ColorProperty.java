/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public final class ColorProperty extends PropertyInterface implements ActionListener
  {
  private static final String classname="ColorProperty.";
  
  private final String key;
  private final String label;
  private final JPanel workPanel;
  private final JTextField sampleField;
  private final JButton showChooserBtn;
  private final Color defaultValue;
  private Color colorValue;
  
  public ColorProperty(String key, String label, Color defaultValue )
    {
    if ( defaultValue == null ) throw new IllegalArgumentException (classname+"ColorProperty() defaultValue==null");

    this.key = key;
    this.label = label;
    this.defaultValue = defaultValue;

    showChooserBtn=new JButton(getIcon("colors.gif"));
    showChooserBtn.setMargin(new Insets(1,1,1,1));
    showChooserBtn.setToolTipText("Premi qui per cambiare il colore");
    showChooserBtn.addActionListener(this);

    sampleField = new JTextField(" Esempio ");    

    FlowLayout layout = new FlowLayout(FlowLayout.TRAILING,1,1);
    workPanel = new JPanel(layout);
    workPanel.add(new JLabel(label));
    workPanel.add(sampleField);
    workPanel.add(showChooserBtn);
    
    colorValue = defaultValue;
    sampleField.setBackground(colorValue);
    }
    



  /**
   * This is what will go into the main panel.
   */
  public JComponent getComponentToDisplay() 
    {
    return workPanel;
    }

  public String getKey ()
    {
    return key;
    }

  /**
   * Implements the interface.
   */
  public void setStringValue ( String value )
    {
    try
      {
      int intColor = Long.decode(value).intValue();
      colorValue = new Color((intColor >> 16) & 0xFF, (intColor >> 8) & 0xFF, intColor & 0xFF);
      }
    catch ( Exception exc )
      {
      colorValue = defaultValue;
      }
      
    sampleField.setBackground(colorValue);
    }
  
  /**
   * Implements the interface.
   */
  public String getStringValue ()
    {
    return "0x"+Integer.toHexString(colorValue.getRGB());
    }

  /**
   * Implements the interface.
   */
  public Object getValue ()
    {
    return colorValue;
    }


  public void actionPerformed(ActionEvent event)
    {
    Color aColor = JColorChooser.showDialog( workPanel, label, colorValue );    
    if ( aColor != null ) colorValue = aColor;
    sampleField.setBackground(colorValue);
    }

    
  }
