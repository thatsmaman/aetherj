/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.BorderLayout;

import javax.crypto.Cipher;
import javax.swing.*;

/**
 * The point of a password property is to allow input of a "secret" field and the
 * storage of it in a "difficult to decode" form.
 * Of course, since the key to code and decode is in here...
 */
public final class PasswordProperty extends PropertyInterface
  {
  private final String key;
  private final JPanel workPanel;
  private final JPasswordField inputField;
  private String value;
  
  private Cipher acipher;
  
  public PasswordProperty(String key, String label, String defaultValue, int columns )
    {
    this.key = key;
    value = defaultValue;

    workPanel = new JPanel(new BorderLayout());
    
    workPanel.add(new JLabel(label),BorderLayout.WEST);

    inputField=new JPasswordField(value,columns);

    workPanel.add(inputField,BorderLayout.CENTER);
    
    try
      {
      acipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
    }
    
  /**
   * This is what will go into the main panel.
   */
  public JComponent getComponentToDisplay() 
    {
    return workPanel;
    }

  public String getKey ()
    {
    return key;
    }

  /**
   * Implements the interface.
   */
  public void setStringValue ( String value )
    {
    this.value = value;
    inputField.setText(value);
    }
  
  /**
   * Implements the interface.
   */
  public String getStringValue ()
    {
    char [] raw = inputField.getPassword();
    return new String(raw);
    }

  /**
   * Implements the interface.
   */
  public Object getValue ()
    {
    return value;
    }
  }
