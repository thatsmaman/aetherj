/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.awt.BorderLayout;
import java.util.Locale;

import javax.swing.*;

public final class LocaleProperty extends PropertyInterface
  {
  private final String key;
  private final JPanel workPanel;
  private final JComboBox inputField;
  private final DefaultComboBoxModel comboModel;
  private Locale value;
  
  public LocaleProperty(String key, String label, Locale defaultValue )
    {
    this.key = key;
    
    if ( defaultValue == null ) defaultValue = Locale.getDefault();
    
    value = defaultValue;

    workPanel = new JPanel(new BorderLayout());
    
    workPanel.add(new JLabel(label),BorderLayout.WEST);

    Locale[] locales = Locale.getAvailableLocales();
    comboModel = new DefaultComboBoxModel();
    for (int index=0; index<locales.length; index++)
      comboModel.addElement(new BetterLocale(locales[index]));

    inputField=new JComboBox(comboModel);
    
    setStringValue(value.toString());

    workPanel.add(inputField,BorderLayout.CENTER);
    }
    
  /**
   * This is what will go into the main panel.
   */
  public JComponent getComponentToDisplay() 
    {
    return workPanel;
    }

  public String getKey ()
    {
    return key;
    }

  /**
   * Implements the interface.
   */
  public void setStringValue ( String value )
    {
    if ( value == null || value.length() < 1 ) return;

    int rowCount = comboModel.getSize();
    for (int index=0; index<rowCount; index++)
      {
      BetterLocale element = (BetterLocale)comboModel.getElementAt(index);
      if ( ! value.equals(element.getLocaleCode()) ) continue;
      
      this.value = element.getLocale();
      comboModel.setSelectedItem(element);
      }
    }
  
  /**
   * Implements the interface.
   */
  public String getStringValue ()
    {
    BetterLocale selected = (BetterLocale)inputField.getSelectedItem();
    return selected.getLocaleCode();
    }

  /**
   * Implements the interface.
   */
  public Object getValue ()
    {
    BetterLocale selected = (BetterLocale)inputField.getSelectedItem();
    return selected.getLocale();
    }

private final class BetterLocale 
  {
  private final Locale locale;
  
  public BetterLocale ( Locale locale )
    {
    this.locale = locale;
    }
    
  public final String getLocaleCode()
    {
    return locale.toString();
    }
    
  public final Locale getLocale()
    {
    return locale;
    }
    
  public final String toString()
    {
    return locale.getDisplayCountry()+ " "+ locale.getDisplayLanguage();
    }
  }
  
    
    
  }
