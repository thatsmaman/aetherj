/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;

import java.net.URL;

import javax.swing.ImageIcon;

import it.aetherj.boot.ComponentProvider;

/**
 * A property has in itself both the visible pat of the gui and the storage.
 */
public abstract class PropertyInterface implements ComponentProvider
  {
  /**
   * Returns the key defined for this property.
   * This is the key that will be used to store and retrieve the Property
   */
  public abstract String getKey ();

  /**
   * The propertyStorage will load something and then call this method for each
   * property registered, so the gui can be updated.
   * PACKAGE access since only PopertyStorage should use it.
   * @param value
   */
  public abstract void setStringValue ( String value );
  
  /**
   * Used to fill up the Property do do the actual save.
   * So, this call does get the value from the gui and stores it for future calles !.
   * PACKAGE access since only PopertyStorage should use it.
   * @return 
   */
  public abstract String getStringValue ();

  /**
   * Returns the last saved value, the gui may show something different, but it does not matter.
   * @return 
   */
  public abstract Object getValue ();



  /**
   * Utility.
   */
  protected final ImageIcon getIcon ( String fileName )
    {
    URL imageUrl = getClass().getResource(fileName);
    
    ImageIcon icon = null;
    if ( imageUrl != null ) icon = new ImageIcon (imageUrl);

    if ( icon == null ) 
      {
      System.out.println ("Cannot find icon name="+fileName);
      icon =  new ImageIcon();
      }
    
    return icon;
    }

  
  }
