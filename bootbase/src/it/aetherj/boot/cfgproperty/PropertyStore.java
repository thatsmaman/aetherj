/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot.cfgproperty;
import java.io.*;
import java.util.*;

import it.aetherj.boot.SharedUtils;
 
/**
 * This accepts a bunch of peroperty and take care of saving/loading.
 * The store can store a bunch of propertyGroup, each with its own prefix.
 */
public class PropertyStore 
  {
  private static final String classname="PropertyStorage.";

  private final File propertyFile;
  private final String propertyHeader;
  private final HashMap<String,PropertyGroup> panelMap;
  
  /**
   * Build a new propeorty storage.
   * 
   * @param propertyFile    The file where to load/save the properties, absolute path.
   * @param propertyHeader  The string to put in the file as a header
   */
  public PropertyStore( File propertyFile, String propertyHeader )
    {
    this.propertyFile = propertyFile;
    this.propertyHeader = propertyHeader;
    panelMap = new HashMap<String,PropertyGroup>();
    }
  
  /**
   * Adds one property to the managed list.
   */
  public void addPropertyGroup ( PropertyGroup aPanel )
    {
    if ( aPanel == null ) return;
    
    panelMap.put(aPanel.getPrefix(),aPanel);
    }
    
    
  /**
   * Load the properties from the filename.
   */
  public final void load ()
    {
    FileInputStream inFile=null;

    try
      {
      inFile = new FileInputStream(propertyFile);
      Properties javaProp = new Properties();
      javaProp.load(inFile);     
      
      for (PropertyGroup aPanel : panelMap.values() )
        aPanel.load(javaProp);
      }
    catch ( Exception exc )
      {
      System.err.println(classname+"load() exc="+exc);
      }
      
    SharedUtils.close(inFile);
    }
    
  /**
   * Saves the poperties to the filename.
   */
  public final void save ()
    {
    FileOutputStream outFile=null;

    
    try
      {
      File parentDir = propertyFile.getParentFile();
      if ( ! parentDir.exists() ) parentDir.mkdirs();
      
      outFile = new FileOutputStream(propertyFile);
      Properties javaProp = new Properties();
      
      for (PropertyGroup aPanel : panelMap.values() )
        javaProp.putAll(aPanel.save());

      javaProp.store(outFile,propertyHeader);
      }
    catch ( Exception exc )
      {
      System.err.println(classname+"save() exc="+exc);
      }
      
    SharedUtils.close(outFile);
    }
  }
  
  
