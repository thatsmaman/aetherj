package it.aetherj.boot.dbase;

import java.sql.Statement;
import java.util.HashMap;

/**
 * This is used when I need to retusn something from a sql execute.
 * NOTE: finalize trap is not needed anymore since ALL statements MUST be closed into dbase handling.
 * It is useful to have the two types of constructor anyway since I may want to handle exception
 * or I may not want to do it.
 * NOTE: At the moment there is no support for multiple returns values, only the first one is handled.
 * The idea is that we have a current value for what is being returned...
 * When I will have the next() then that should update the current values...
 */
public class Ris
  {
  private static final String classname="Ris.";
  
  private final Dbase     dbase;
  private final Statement statement;
  private final Exception exception;

  private int       curUpdateCount;     // Number of rows updated, -1 means error or resultset, 0, means nothing
  private boolean   curIsResultset;     // False is an int result , true is a resultset
  private HashMap<String,Object> returnValues;


  /**
   * When you are executing a pure executeUpdate you use this constructor to catch the result.
   * @param aDbase the dbase this statement is executing on
   * @param aStatement the statement itself
   * @param updateCount the value returned by statement.executeUpdate(expression)
   */
  public Ris(Dbase aDbase, Statement aStatement, int updateCount )
    {
    dbase     = aDbase;
    statement = aStatement;
    exception = null;
    curUpdateCount = updateCount;
    curIsResultset = false;
    }

  /**
   * When you do not know what the result will be then you must use this one.
   * Note that there may be more than one "result" and this is a bit messy.
   * @param aDbase
   * @param aStatement
   * @param isResultset the value returned by statement.execute(expression)
   */
  public Ris(Dbase aDbase, Statement aStatement, boolean isResultset )
    {
    dbase     = aDbase;
    statement = aStatement;
    exception = null;
    curIsResultset = isResultset;
    syncUpdateCount();    
    }

  /**
   * Use this one when there is an exception and no result is available.
   * @param aDbase
   * @param aStatement
   * @param aExc the exception being thrown.
   */
  public Ris(Dbase aDbase, Statement aStatement, Exception aExc )
    {
    dbase     = aDbase;
    statement = aStatement;
    exception = aExc;
    curIsResultset = false;
    curUpdateCount = -1;
    }

  /**
   * If you also need to resurn touples of key/values than use this one.
   * @param key
   * @param value
   */
  public final void addReturnValue (String key, Object value )
    {
    if ( returnValues == null ) 
      returnValues = new HashMap<>();

    returnValues.put(key,value);
    }

  public final Object getReturnValue ( String key )
    {
    if ( returnValues == null ) return null;

    return returnValues.get(key);
    }


  /**
   * Test if this ris has failed or not.
   * Failure is defined as simply having had an exception, other tests are not well defined.
   * @return
   */
  public final boolean hasFailed ()
    {
    return exception != null;
    }

    
  public final Exception getException ()
    {
    return exception;
    }

  
  /**
   * Returns the update count. At the moment returns just the first value of update count.
   * @return -1 is result is resultset or error, 0 if no update, >0 as number of rows updated.
   */
  public final int getUpdateCount ()
    {
    return curUpdateCount;  
    }

  /**
   * Returns if the current result is a resultset.
   * @return true if it is, false if it is an update.
   */
  public final boolean getResultType ()
    {
    return curIsResultset;
    }

  /**
   * Assuming that this ris currently points to a resultset then return it.
   * @return null if current is not a resultset, a Brs, so I can catch errors -)
   */
  public final Brs getResultSet ()
    {
    if ( ! curIsResultset ) return null;

    try
      {
      return new Brs (dbase,statement,statement.getResultSet());
      }
    catch ( Exception exc )
      {
      return new Brs(dbase,statement,exc);
      }
    }

  /**
   * When you are doing a generic execute() you should call this one so the current values are in sync.
   * @return
   */
  private final void syncUpdateCount ()
    {
    // This is a preset that assume we are dealing with a resultset or an error.
    curUpdateCount = -1;

    // if this is a resultset then we are done
    if ( curIsResultset ) return;

    try
      {
      curUpdateCount = statement.getUpdateCount();
      }
    catch ( Exception exc )
      {
      System.err.println("Ris.getUpdateCount(): Exception="+exc.toString());
      }
    }
    
  /**
   * Close is useful if you know you have finished
   */
  public final void close () 
    {
    try
      {
      dbase.closeStatement(statement);
      }
    catch ( Exception exc )
      {
      System.err.println(classname+"close() exc="+exc);
      exc.printStackTrace();
      }
    }


  /**
   * Shows something useful about this Ris.
   * If we had a failure retun the failure, othervise return how many records have been updated.
   */
  public String toString ()
    {
    if ( exception != null ) 
      return exception.toString();
    else
      return ""+getUpdateCount();
    }
  }