package it.aetherj.boot.dbase;

import java.sql.Types;

public class DbaseQuirksPostgress extends DbaseQuirks
  {
  public DbaseQuirksPostgress (String jclass, String family )
    {
    super(jclass,family);
    }
  
  
  @Override
  public String getCreateVarbinary (int length)
    {
    return " BYTEA ";
    }
  
  @Override
  public boolean isTypeCompatible (int my_type, int db_type)
    {
    switch ( my_type )
      {
      case Types.VARBINARY:
        return db_type == Types.BINARY;
        
      case Types.DECIMAL:
        return db_type == Types.NUMERIC;

      case Types.BOOLEAN:
        return db_type == Types.BIT;

      default:
        return my_type == db_type;
      }
    }
  

  @Override
  public boolean isIdentifierUcase ()
    {
    return false;
    }
  
  }
