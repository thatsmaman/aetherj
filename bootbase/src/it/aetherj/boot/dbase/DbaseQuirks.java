package it.aetherj.boot.dbase;


/**
 * This, original version is for HSQLDB
 * @author damiano
 */
public class DbaseQuirks
  {
  public final String dbJclass;
  public final String dbFamily;
  
  public DbaseQuirks ( String jclass, String family )
    {
    dbJclass = jclass;
    dbFamily = family;
    }

  public String getCreateVarbinary (int length)
    {
    return " VARBINARY("+length+") ";
    }
  
  public boolean isTypeCompatible (int my_type, int db_type)
    {
    return my_type == db_type;
    }
  
  /**
   * If the dbase automatically convert all unquoted identifiers to upper case then this is true (HSQLDB and others)
   * It is false if the conversion is to lower case (Postgress)
   * @return
   */
  public boolean isIdentifierUcase ()
    {
    return true;
    }
  

  }
