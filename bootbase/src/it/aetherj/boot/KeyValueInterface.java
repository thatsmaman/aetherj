/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot;

/**
 * This is used in the IdValue an KeyValue to provide a uniform interface to the value.
 * The use is in the combo where you want to retain the ID and be able to handle both
 * cases of keys being string or integers.
 */
public interface KeyValueInterface <E>
  {
  /**
   * Returns the key as a String, if it is an ID it is always not null.
   * Even for a KeyValue it should be not null, but who knows...
   * @return 
   */
  public E getKey ();
    
  }
