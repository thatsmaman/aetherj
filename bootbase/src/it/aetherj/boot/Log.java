/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, Italy
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.boot;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;

import javax.swing.*;


/**
 * Questa classe gestisce il logging, DEVE essere fatta partire al BOOT
 * Praticamente la seconda cosa che fa boot dopo avere inizializzato se stesso.
 * It IS essential that the logger do NOT use anything of the whole system.
 * NOTE: Logging IS different than debugging, if you wish to have selective debugging you MUST
 * implement your own class that does what is needed.
 */
public final class Log implements ComponentProvider, PrintlnProvider
  {
  public static final String frameTitle="System Boot Log"; 
  
  private static final int REDIRECT_OUT=1;
  private static final int REDIRECT_ERROR=2;
  private static final int LINES_MAX=2000;
  private static final int progress_max = 25;
  
  private final JFrame logFrame;
  private boolean      logFrameAlwaysVisible;  // if I need to show popup the log frame must always be visible

  private JFrame       feedbackFrame;       // Used to give some feedback on the loading
  private JProgressBar progressBar;
  private Timer        feedbackTimer;
  private JTextField   progressField;       // You can write a number here to show the progress
  private int          boot_index;

  private LimitedTextArea outArea,errArea,userArea;
  private JTabbedPane tabbedPane;

  private volatile boolean bootComplete=false;
  private volatile int exitCode;      // This is the exit code that I should use to exit.
  
  
  
  
  /**
   * Constructor.
   * Package access.
   * There is an option to avoid out/err redirect since it may happen that the mechnism do not work due to
   * a swing thread lockup. (Basically there is the write but you never see)
   * In such extreme case (that it may happen) you start the program using the command line and ask NOT ro redirect out/err
   */
  public Log ( boolean redirectOutErr )
    {
    logFrame = newOutputFrame();

    if ( redirectOutErr ) 
      {
      RedirectMessage redirOut = new RedirectMessage(REDIRECT_OUT);
      redirOut.setName("stdout redirect");
      redirOut.setDaemon(true);
      redirOut.start();
      
      RedirectMessage redirErr = new RedirectMessage(REDIRECT_ERROR);
      redirErr.setName("stderr redirect");
      redirErr.setDaemon(true);
      redirErr.start();
      }

    newBootFeedbackFrame();
    }




  /**
   * Creates the new feedback frame that will tell the user that something is happening.
   */
  private void newBootFeedbackFrame()
    {
    feedbackFrame = new JFrame ("Boot .. ");
    feedbackFrame.setIconImage(getIconImage());
    feedbackFrame.addWindowListener(new CloserListener());

    progressBar = new JProgressBar();
    progressBar.setMinimum(0);
    progressBar.setMaximum(progress_max); 
    
    JPanel workPanel = (JPanel)feedbackFrame.getContentPane();
    workPanel.add(new JLabel("Program is starting"),BorderLayout.NORTH);
    workPanel.add(progressBar,BorderLayout.CENTER);
    workPanel.add(newProgressStepPanel(),BorderLayout.SOUTH);
    
    workPanel.add(new JLabel("Program is starting"),BorderLayout.NORTH);
    workPanel.add(progressBar,BorderLayout.CENTER);
    
    feedbackFrame.pack();
    feedbackFrame.setLocationRelativeTo(null);    
    feedbackFrame.setVisible(true);

    // Time to start the timer that will show the log if something goes wrong.    
    feedbackTimer = new Timer(500,new BootingProgress());
    feedbackTimer.start();
    }

  private JPanel newProgressStepPanel ()
    {
    JPanel risul = new JPanel(new FlowLayout(FlowLayout.LEFT,1,1));
    risul.add(new JLabel("Step"));
    progressField = new JTextField(5);
    risul.add(progressField);
    return risul;
    }

  /**
   * Setup a simple frame where I can redirect output and error when needed.
   * This also sets the Look and Feel
   */
  private JFrame newOutputFrame ()
    {
    outArea = new LimitedTextArea(LINES_MAX);
    errArea = new LimitedTextArea(LINES_MAX);
    userArea = new LimitedTextArea(LINES_MAX);
    
    // I have too many of them and are used not together, I hope :-)
    tabbedPane = new JTabbedPane();
    tabbedPane.add("User",userArea.getComponentToDisplay());
    tabbedPane.add("Output",outArea.getComponentToDisplay());
    tabbedPane.add("Error",errArea.getComponentToDisplay());

    JFrame aFrame = new JFrame (frameTitle);
    aFrame.setIconImage(getIconImage());
    aFrame.addWindowListener(new CloserListener());
    aFrame.getContentPane().add(tabbedPane);

    aFrame.setSize (500, 400);
    aFrame.setLocationRelativeTo(null);    
    
    return aFrame;
    }

  private Image getIconImage ()
    {
    URL url = getClass().getResource("log.png");

    if ( url == null ) 
      return new BufferedImage(22,22,BufferedImage.TYPE_INT_ARGB);
    
    ImageIcon icon = new ImageIcon (url);
    
    return icon.getImage();
    }
  /**
   * Returns the tabbed pane that holds all the info, if you want to put it
   * somewhere else than in the standard log window.
   */
  public JComponent getComponentToDisplay ()
    {
    return tabbedPane;
    }
  
  /**
   * Steps are normally numbers, they can even jump backward, it is duty of the programmer to
   * have meaningful steps...
   * @param step_index
   */
  public void setProgressStep ( String step_index )
    {
    progressField.setText(step_index);
    }

  public void setProgressStep ( int step_index )
    {
    setProgressStep(Integer.toString(step_index));
    }
    
  /**
   * Sets the exit code that boot should use to exit.
   * If you install a security manager that checks for System.exit you have to allow
   * this exit otherwise it will be impossible to exit on error.
   * Make It a random so libraries cannot try to sneak past an exit.
   */
  public void setExitCode ( int exitCode )
    {
    this.exitCode = exitCode;
    }
    
  /**
   * This is used to either hide or show the log window.
   * Since this is something that the main program does it also means that
   * the main program is alive and therefore the feedback window/timer must stop.
   */
  public void setVisible ( boolean showFrame )
    {
    // the logFrame must be visible if any of the two is true
    logFrame.setVisible(logFrameAlwaysVisible || showFrame );
    
    // this is enough to let the feedback boot hide
    bootComplete = true;
    }

  /**
   * Return the bounds: size and position of this log frame
   * @return
   */
  public Rectangle getBounds ()
    {
    return logFrame.getBounds();
    }
  
  /**
   * Set this frame bounds
   * @param bounds
   */
  public void setBounds ( Rectangle bounds )
    {
    if ( bounds == null ) return;
    
    if ( bounds.width < 100 )
      bounds.width=100;
    
    if ( bounds.height < 100 )
      bounds.height=100;
    
    if ( bounds.x < 0 )
      bounds.x=0;
    
    if ( bounds.y < 0 )
      bounds.y=0;
    
    logFrame.setBounds(bounds);
    }
    
  /**
   * If you want to print to the user area, this is for you.
   */
  public final void userPrintln ( String message )
    {
    userArea.println(message);
    }


  public final void errorPrintln ( String message )
    {
    errArea.println(message);      
    }
  
  @Override
  public void println(String message)
    {
    userArea.println(message);
    }
  
  @Override
  public void println(String message, Throwable exception)
    {
    errArea.println(message, exception);
    }

  /**
   * Prints an exception.
   * It is printed using System.err.println(), so it will go to the "console" if you do not redirect out/err to a GUI.
   * The idea is that if you are tracing and want to see the exception than this MUST work !.
   */
  public final void exceptionPrint ( String message, Throwable exc )
    {
    System.err.println(exceptionExpand(message,exc));
    }
  



  /**
   * This expands a message and an exception into something that has both
   * the message, the exception message and the stack trace.
   */
  public static final String exceptionExpand ( String message, Throwable exc )
    {
    StringBuffer aBuffer = new StringBuffer(1000);
    
    if ( message != null )
      {
      aBuffer.append("User Message=");
      aBuffer.append(message);
      aBuffer.append("\n");
      }
    
    if ( exc == null )
      {
      // Nothing else to add to the message.
      return aBuffer.toString();
      }

    aBuffer.append("Exception Class="+exc.getClass());
    aBuffer.append("\n");

    aBuffer.append("--> toString=");
    aBuffer.append (exc.toString());
    aBuffer.append("\n");
    
    aBuffer.append("--> Stack=");
    fillStackTrace(aBuffer,exc.getStackTrace());
    aBuffer.append("\n");

    return aBuffer.toString();
    }


  /**
   * Creates a printing stacktrace from the given elements.
   * @param elements the StacktraceElements to add to the buffer.
   * @param buffer the string buffer where to add the elements.
   */
  private static void fillStackTrace (StringBuffer buffer,  StackTraceElement[] elements )
    {
    for ( int index=0; index<elements.length; index++ )
      {
      buffer.append(elements[index]);
      buffer.append("\n");
      }
    }


  /**
   * This will SHOW an exception in terms of popup window.
   * The user will be really aware about it !
   * It returns true if there was an exception to show, false othervise.
   * Note that if there is no exception then nothing will be done.
   */
  public final boolean exceptionShow ( String message, Throwable exc )
    {
    if ( exc == null ) return false;

    String showMsg = exceptionExpand (message,exc);

    messageShow(showMsg,JOptionPane.ERROR_MESSAGE);    
      
    return true;
    }

  public boolean messageShow ( String message )
    {
    return messageShow ( message,JOptionPane.INFORMATION_MESSAGE );
    }

  /**
   * Shows the given message to the user in a reliable way.
   * @param message the message to show
   * @param p_type one of JOptionPane.INFORMATION_MESSAGE JOptionPane.ERROR_MESSAGE
   * @return true meaning that a message was shown.
   */
  public boolean messageShow ( String message, int p_type )
    {
    if ( SwingUtilities.isEventDispatchThread() )
      showMessageDialog(message, "Copiate questo messaggio", p_type);
    else
      SwingUtilities.invokeLater(new SwingShowMessage(message,"(fix) Copiate questo messaggio",p_type));    

    return true;
    }

  /**
   * This MUST be on a SWING THread
   * The logFrame MUST be visible and remain visible !!
   */
  private void showMessageDialog ( String message, String header, int msg_type )
    {
    logFrameAlwaysVisible = true;
    logFrame.setVisible(true);
    
    JTextArea msgArea = new JTextArea(message,10,75);    // It it a text area so you can COPY the message !
    msgArea.setLineWrap(true);
    JScrollPane scrollPane = new JScrollPane(msgArea);
    
    JOptionPane.showMessageDialog(logFrame,scrollPane,header,msg_type);
    }


  /**
   * A simple utility class to do things properly.
   * The issue here is to queue all the message so they do not HANG the swing thread.
   * So, basically, do NOT use JOptionPane.showMessageDialog alone, use this wrapper.
   */
  private final class SwingShowMessage implements Runnable
    {
    private final String message;
    private final String header;
    private final int msg_type;
    
    SwingShowMessage ( String p_message, String p_header, int p_type )
      {
      message  = p_message;
      header   = p_header;
      msg_type = p_type;
      }
    
    @Override
    public void run ()
      {
      showMessageDialog(message,header,msg_type);
      }
    }


/**
 * I use my handler so I am sure that I know when to close the program or not.
 */
private final class CloserListener extends WindowAdapter
  {
  public void windowClosing(WindowEvent event)
    {
    if ( bootComplete ) 
      {
      // if the boot is complete then I just need to hide the frame
      JFrame aFrame = (JFrame)event.getSource();
      aFrame.setVisible(false);
      }
    else
      {
      // Boot is not complete, do a system exit
      messageShow("Boot abort EXIT request\nSystem.exit("+exitCode+")");
      System.exit(exitCode);
      }
    }
  }


/**
 * When the timer expires what happens is that the feedback window disappear and
 * the log window appears, hopefully showing what went wrong.
 * NOTE you must NOT change the exitOnWindowClose flag !
 */
private final class BootingProgress implements ActionListener
  {
  @Override
  public void actionPerformed(ActionEvent e)
    {
    progressBar.setValue(boot_index++);
  
    if ( boot_index > progress_max || bootComplete )
      {
      feedbackTimer.stop();
      
      // I wish to dismiss the feedback frame in any case
      feedbackFrame.setVisible(false);
      
      // the logFrame is visible if the boot is not complete
      logFrame.setVisible( ! bootComplete );
      }
    }
  } 


/**
 * This is the thread that copies all from what is written and sends it to the console.
 */
private final class RedirectMessage extends Thread
  {
  private final String classname="RedirectMessages.";
  private final int      redirectWhat;
  private BufferedReader inputReader;
  private LimitedTextArea  writeHere;
    
  RedirectMessage ( int redirectWhat )
    {
    this.redirectWhat = redirectWhat;
    }

  /**
   * The thread starts here.
   */
  public void run () 
    {
    for (;;)
      {
      bindStreams();
      copyData();
      }
    }

  /**
   * This tryes to bind the streams and the windows.
   * If this fails I need to know about it.
   */
  private void bindStreams()
    {
    try
      {
      PipedOutputStream sendDataHere = new PipedOutputStream();
      PipedInputStream  readDataHere = new PipedInputStream(sendDataHere);

      InputStreamReader readDataStream = new InputStreamReader(readDataHere);
      inputReader = new BufferedReader(readDataStream);

      PrintStream sendPrintHere = new PrintStream (sendDataHere);

      if ( redirectWhat == REDIRECT_OUT )
        {
        writeHere = outArea;
        System.setOut(sendPrintHere);
        }
        
      if ( redirectWhat == REDIRECT_ERROR )
        {
        writeHere = errArea;
        System.setErr(sendPrintHere);
        }
      }
    catch ( Exception exc )
      {
      // This is really something that shold never happen and therefore I want to know about it
      exceptionShow (classname+"bindStreams: Exception=",exc);  
      }
    }

  /**
   * This copies data, when some exception occours it will come out of it
   */
  private void copyData ()
    {
    try
      {
      String aLine;
      while ( (aLine=inputReader.readLine()) != null ) writeHere.println(aLine);
      }
    catch ( Exception exc )
      {
      // This is produced even by a simple "close" on stdout, so I cannot use exceptionShow()
      // No point to do a full stack trace, it is always the same and due to the close.
      // A simple message is enough.
//      exceptionPrint (classname+"copyData()",exc);  
      System.err.println(classname+"copyData() input closed");
      }
    }
  }

  } // END




