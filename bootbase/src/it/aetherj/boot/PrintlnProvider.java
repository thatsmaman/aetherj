package it.aetherj.boot;

/**
 * This is mostly used to direct logging to proper places
 * You may implement this and send logs to different places
 * @author damiano
 */
public interface PrintlnProvider
  { 
  void println(String message);
   
  void println(String message, Throwable exception);
  }
