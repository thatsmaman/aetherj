package it.aetherj.boot;

/**
 * I need a debugging class, the old Log cannot do what I need unfortunately.
 * The main reason is that I need to have a way to extend this one and attach the debug constants to it.
 * So, this one is basically a repository for utilities, to make it useful you MUST extend it and
 * add a few constants that define what subsys you are logging.
 * 
 * Another thing that I found out is that there really are just two classes.
 * Either we are just tracing what happens on the program and we just use the isDbg and the following writeln
 * or we know that there is something strange and then it is always a warning.
 * So, the log level is not used as it was in old days.
 */
public class Debug 
  {
  private static final String classname="Debug.";
  
  protected final Log log;

  private volatile int dbgMask;

  /**
   * COnstructor, the log is needed to do the actual printing.
   * @param log
   */
  public Debug(Log log)
    {
    if ( log == null ) throw new IllegalArgumentException(classname+"Constructor: ERROR lof==null");
    
    this.log=log;
    dbgMask=0;
    }
    
  /**
   * Sets the mask of subsystems that we want to log.
   */
  public final void setMask ( int dbgMask )
    {
    this.dbgMask=dbgMask;
    // This is needed since you may have competing entity trying to set a new mask.
    println(classname+"setMask() new dbgMaks="+dbgMask);
    }
    
  /**
   * Use this one to check if the given subsys is being logged.
   * @return 
   * @param subsys
   */
  public final boolean isDbg(int subsys)
    {
    return (dbgMask & subsys) != 0;
    }
  
  /**
   * If you want to print a normal message use this one.
   * @param message
   */
  public final void println ( String message )
    {
    log.userPrintln(message);
    }
     
  /**
   * If you want to print a WARNING message use this one.
   * @param message
   */
  public final void warning ( String message )
    {
    log.userPrintln("WARNING: "+message);
    }
  
    
  }