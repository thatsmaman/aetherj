/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

import java.awt.Desktop;
import java.awt.event.*;
import java.io.File;
import java.net.URI;

import javax.swing.*;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.xml.XmlConfiguration;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.config.ConfigProperties;

public class GwtWebServer extends WebServer 
  {
  private static final String classname="GwtWebServer";
  
  private JTextField clientUrl;

  public GwtWebServer(Stat stat)
    {
    super(stat);
    }
  
  protected JPanel getNorthPanel()
    {
    JPanel risul = new JPanel();
    
    JButton startBrowser = new JButton("Start Browser");
    startBrowser.addActionListener(new StartBrowser());
    
    clientUrl = new JTextField(30);  // MUST be before super(stat)
    
    risul.add(startBrowser);
    risul.add(clientUrl);
    
    return risul;
    }
  
  protected void startWebServer()
    {
    println(classname+" Run Web Server");         
    
    try
      {
      System.setProperty("jetty.home",webHomeDir.getCanonicalPath());
       
      File jetty_log_dir = new File(webHomeDir,"logs");
      System.setProperty("jetty.logs",jetty_log_dir.getCanonicalPath());
  
      webPort = stat.aeParams.gwtWebTcpPort;

      if ( webPort == 0 )
        {
        webPort = stat.utils.findFreeTcpPort();
        // save it to config, regardless of value
        stat.aeParams.gwtWebTcpPort = webPort;
        }
      
      if ( webPort == 0 )
        {
        println(classname+".startWebServer: ABORT > Cannot allocate TCP port");
        return;
        }
      
      println("GWT port="+webPort);   
      
      // should be OK even if it is not in Swing thread..
      clientUrl.setText(getLocalServerAddress());
 
//      ErrorHandler ha = new ErrorHandler();

      JettyPanelLog plog = new JettyPanelLog(stat, logpanel);

//      org.eclipse.jetty.util.log.Log.setLog(plog);
      
      webServer=new Server();
      
      Resource r = Resource.newResource(new File(webHomeDir,"jetty.xml"));
      XmlConfiguration configuration = new XmlConfiguration(r); 
      configuration.configure(webServer);      

      ServerConnector externalConnector = new ServerConnector(webServer);
      externalConnector.setPort(webPort);
      externalConnector.setName("GWTconnector");
      webServer.addConnector(externalConnector);
      
      HandlerCollection collection = new HandlerCollection();
      newHandlers(collection);
      webServer.setHandler(collection);
            
      webServer.start ();
      
      logpanel.println("--- Web Server Started ---");   
      
      printAppInfo(webAppContext);
  
      // when server is started start a default copy of the browser
      browserAutostart();
      
      // wait until the started thread ends
      webServer.join();
      }
    catch ( Exception exc )
      {
      logpanel.println(classname+"startServer()",exc);
      }
    
    logpanel.println("startWebServer: END");         
    }
  
  protected String getAppPrefix ()
    {
    return "gwtwar";
    }
    


  private void browserAutostart ()
    {
    boolean browserAutostart = stat.config.getProperty(ConfigProperties.KEY_webBrowserAutostart, true);
    
    if ( ! browserAutostart ) return;

    StartBrowser starter = new StartBrowser();
    starter.actionPerformed(new ActionEvent(this,1,"run"));
    }

private final class StartBrowser implements ActionListener, Runnable
  {
  public void actionPerformed(ActionEvent e)
    {
    Thread starter = new Thread(this);
    starter.setName("Web Browser Starter");
    starter.setDaemon(true);
    starter.start();
    }
  
  public void run()
    {
    // I should jump to the correct browser page
    if ( ! Desktop.isDesktopSupported() )
      {
      logpanel.println(classname+"actionPerformed: ERROR: NO DesktopSupport ! ");
      return;
      }
      
    Desktop desktop = Desktop.getDesktop();
    if ( ! desktop.isSupported(Desktop.Action.BROWSE))
      {
      logpanel.println(classname+"actionPerformed: ERROR: Desktop.Action.BROWSE NOT supported ! ");
      return;
      }
    
    try
      {
      String address = getLocalServerAddress();

      URI uri = new URI(address+"/");
      
      desktop.browse(uri);
      }
    catch ( Exception exc )
      {
      logpanel.println(classname+"run: ",exc);
      }
    }
  }

  
  
  }
