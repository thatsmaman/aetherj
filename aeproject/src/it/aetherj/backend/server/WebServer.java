/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

import java.awt.BorderLayout;
import java.io.*;

import javax.swing.*;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.*;
import org.eclipse.jetty.webapp.WebAppContext;

import it.aetherj.backend.Stat;
import it.aetherj.boot.*;

/**  
 * NOTE that this is the abstract part...
 */
public abstract class WebServer implements ComponentProvider, PrintlnProvider
  {
  private static final String classname="WebServer.";
  
  protected final Stat stat;
  private final JPanel workPanel;  
  
  protected final LimitedTextArea logpanel;
  protected final File webHomeDir;
  
  protected Server webServer;
  private HandlerCollection rootHandler;       // All handlers here are run
  protected WebAppContext webAppContext;
	protected int webPort; 

  public WebServer(Stat stat)
    {
    this.stat = stat;

    logpanel = new LimitedTextArea(2000);
    webHomeDir = new File(stat.boot.getProgramDir(),"websrv");

    workPanel = newWorkPanel();
    }
  
	public String getLocalServerAddress ()
		{
		String address = "http://localhost";

		if ( webPort != 80 ) address += ":"+webPort;
		
		return address;
		}
	
	public final void println(String message)
  	{
  	logpanel.println(message);
  	}

  public final void println(String message,Throwable exc)
    {
    logpanel.println(message,exc);
    }

	private JPanel newWorkPanel()
    {
    JPanel risul = new JPanel(new BorderLayout());
    
    risul.add(getNorthPanel(),BorderLayout.NORTH);
    risul.add(logpanel.getComponentToDisplay(),BorderLayout.CENTER);        
    
    return risul;
    }

  protected abstract JPanel getNorthPanel ( );
  
  /**
   * Children must provide the app prefix to load the WAR peoperly
   * Eg: gwtwar, mimwar
   * @return 
   */
  protected abstract String getAppPrefix ();


  /**
   * Actually start this server as a daemon.
   */
  public void startServer()
    {
    Thread aThread = new Thread(new StartWebServer());
    // MUST set the correct classloader since now Jetty do "some magic" with it...
    aThread.setContextClassLoader(stat.getClassLoader());
    aThread.setName(getAppPrefix()+" Web Server");
    aThread.setDaemon(true);
    aThread.setPriority(Thread.MIN_PRIORITY);
    aThread.start();
    }

  
  private void printHandlers ()
    {
    ContextHandlerCollection contesti = rootHandler.getChildHandlerByClass(ContextHandlerCollection.class);
    
    for ( Handler handler : contesti.getHandlers() )
      logpanel.println("handler: "+handler);
  
    }

  public JComponent getComponentToDisplay()
    {
    return workPanel;      
    }
  
  protected void newHandlers(HandlerCollection collection) throws Exception
    {
    collection.addHandler(newRequestLogHandler());
    
    webAppContext = newWebAppFromWar();

    if ( webAppContext == null )
      webAppContext = newWebAppFromDir();
    
    if ( webAppContext == null )
      throw new IllegalArgumentException("NO WAR or war directory available for application");

    // Give the following objects to the webapp context
    webAppContext.setAttribute("SystemStat",stat);
    webAppContext.setAttribute("WebLogArea",logpanel);
    collection.addHandler(webAppContext);
    }

  
  protected WebAppContext newWebAppFromWar () throws IOException
    {
    File warFile = new File(webHomeDir,getAppPrefix()+".war");
    
    if ( ! warFile.canRead() )
      return null;
    
    logpanel.println("newWebAppFromWar: WAR file "+warFile);
    
    WebAppContext webapp = new WebAppContext();
    webapp.setContextPath("/");
    webapp.setWar(warFile.getCanonicalPath());
    
    webapp.setDefaultsDescriptor(new File(webHomeDir,"webdefault.xml").getCanonicalPath());

//    WebAppClassLoader loader = new WebAppClassLoader(stat.getClassLoader(),webapp);
//    webapp.setClassLoader(loader); 
//    webapp.setParentLoaderPriority(true);
    
    return webapp;
    }
  

  /**
   * Note that some of the magic is time dependent, meaning that an app context is NOT fully populated when you wish
   * This is especially true for classloading
   * @param ctx
   */
  protected void printAppInfo ( WebAppContext ctx )
    {
    logpanel.println("printAppInfo");
    logpanel.println("   getClassPath="+ctx.getClassPath());
    logpanel.println("   getExtraClasspath="+ctx.getExtraClasspath());
    logpanel.println("   getBaseResource="+ctx.getBaseResource());
    logpanel.println("   getDescriptor="+ctx.getDescriptor());
    logpanel.println("   getDefaultsDescriptor="+ctx.getDefaultsDescriptor());
    }
  
  /**
   * This tries to start the app from a directory, if it exists
   */
  protected WebAppContext newWebAppFromDir () throws IOException
    {
    File warDir = new File(stat.boot.getProgramDir(),getAppPrefix());
    
    if ( ! warDir.exists() )
      return null;
    
    logpanel.println("newWebAppFromDir: war directory "+warDir);
    
    String warDirPath = warDir.getCanonicalPath();
    
    WebAppContext webapp = new WebAppContext(warDirPath,"/");
    webapp.setDescriptor(warDirPath+"/WEB-INF/web.xml");
    webapp.setDefaultsDescriptor(new File(webHomeDir,"webdefault.xml").getCanonicalPath());

//    webapp.setParentLoaderPriority(true);

// this is not really needed, if you set the Thread context classloader
//    WebAppClassLoader loader = new WebAppClassLoader(stat.getClassLoader(),webapp);
//    webapp.setClassLoader(loader); 
        
    return webapp;
    }
  
  
  /**
   * Try to stop the server, this MUST be on a NON  swing thread
   */
  public void stopServer()
    {
    logpanel.println("SHUTDOWN"); 
    
    try
      {
      // Doing a graceful stop result surely in a hangup if the server is still active.
      if ( webServer != null ) 
        webServer.stop();
      
      logpanel.println("STOPPED");         
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow(classname+"stopServer()",exc);
      }
    }
  
  
  protected RequestLogHandler newRequestLogHandler ()
    {
    RequestLogHandler risul = new RequestLogHandler();

    WebRequestLog requestLog = new WebRequestLog(stat,logpanel);

    risul.setRequestLog(requestLog);  
    
    return risul;
    }
	
	protected abstract void startWebServer();
	
private final class StartWebServer implements Runnable
  {
  public void run ()
    {
    startWebServer();
    }  
  }





  


  } // END main class





