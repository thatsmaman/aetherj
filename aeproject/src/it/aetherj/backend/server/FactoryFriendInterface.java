/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

/**
 * This interface defines methods that make a class factory friend.
 * The methods allows a Factory to work properly and it is a seal of correctness.
 * It is mostly to properly identify what classes are implementing.
 * A class that implements this inteface has a method that returns a unique id
 * There are already various ways to return unique id in Java, but this one gives a chance to 
 * return a human readable id that does not overlap with java behaviour.
 */
public interface FactoryFriendInterface
  {
  /**
   * Things should be what they are, this is a classname and it is unique meaning that the package is not accounted for
   * You CANNOT change the classname since it may be used to select it starting from a DB
   * Basically it is a concrete implementation of an abstract class.
   * @return
   */
  public String getUniqueClassname();
  
  /**
   * The unique ID is something like the port name of a Licom or
   * the modem brand for a Lisetup of the modem type and so on so forth.
   * It can return null if the id is not assigned, yet.
   * @return
   */
  public String getUniqueId ();
  
  /**
   * ALl concrete instances must be able to be initialized, after constructor.
   * The reason to separate construction from initialization is that during initialization
   * you may need items that would be constructed later.
   * On constructor you make the environment, on initialize you set it up !
   * @param params
   */
  public void initialize (FactoryParamMap params) throws Exception;
  }
