/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

import java.io.File;

import javax.swing.*;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.xml.XmlConfiguration;

import it.aetherj.backend.Stat;

public class MimWebServer extends WebServer
  {
  private static final String classname="MimWebServer";

  public MimWebServer(Stat stat)
    {
    super(stat);
    }
  
  protected JPanel getNorthPanel()
    {
    JPanel risul = new JPanel();
    
    risul.add(new JLabel("Mim Webserver"));
    
    return risul;
    }

  protected String getAppPrefix ()
    {
    return "mimwar";
    }

  
  protected void startWebServer()
    {
    println("Avvio server Web");         
    
    try
      {
      System.setProperty("jetty.home",webHomeDir.getCanonicalPath());
        
      webPort = stat.aeParams.getBackendPort();

      if ( webPort == 0 )
        {
        webPort = stat.utils.findFreeTcpPort();
        // save it to configuration, regardless of value
        stat.aeParams.setBackendPort(webPort);
        }
      
      if ( webPort == 0 )
        {
        println(classname+".startWebServer: ABORT > Cannot allocate TCP port");
        return;
        }

      println("Mim port="+webPort);   

      webServer=new Server();
      Resource r = Resource.newResource(new File(webHomeDir,"jetty.xml"));
      XmlConfiguration configuration = new XmlConfiguration(r); 
      configuration.configure(webServer);      

      ServerConnector externalConnector = new ServerConnector(webServer);
      externalConnector.setPort(webPort);
      externalConnector.setName("MimConnector");
      webServer.addConnector(externalConnector);
      
      HandlerCollection collection = new HandlerCollection();
      newHandlers(collection);
      webServer.setHandler(collection);
            
      webServer.start ();
      
      println("Server Web avviato correttamente");   
      
      printAppInfo(webAppContext);
      
      // wait until the started thread ends
      webServer.join();
      }
    catch ( Exception exc )
      {
      println(classname+"startServer()",exc);
      }
    
    println("startWebServer: END");         
    }


  }
