/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.server;

import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.component.LifeCycle;

import it.aetherj.backend.Stat;
import it.aetherj.boot.LimitedTextArea;

public final class WebRequestLog implements RequestLog
  { 
  private final Stat stat; 
  private final LimitedTextArea logArea;

  private boolean isRunning;
  
  public WebRequestLog(Stat stat, LimitedTextArea logArea)
    {
    this.stat = stat;
    this.logArea = logArea;
    }

  public void log(Request request, Response response)
    {
    StringBuilder risul = new StringBuilder(80);
    risul.append(request.getRequestURL());
    
    String query = request.getQueryString();
    if ( query != null ) risul.append(query);
    
    logArea.println(risul.toString());
    }

  public void start()
    {
    isRunning = true;
    }

  public void stop()
    {
    isRunning = false;
    }

  public boolean isRunning()
    {
    return isRunning;
    }

  public boolean isStarted()
    {
    return isRunning;
    }

  public boolean isStarting()
    {
    return false;
    }

  public boolean isStopping()
    {
    return false;
    }

  public boolean isStopped()
    {
    return isRunning == false;
    }

  public boolean isFailed()
    {
    return false;
    }

  public void addLifeCycleListener(LifeCycle.Listener listener)
    {
    }

  public void removeLifeCycleListener(LifeCycle.Listener listener)
    {
    }
  }
