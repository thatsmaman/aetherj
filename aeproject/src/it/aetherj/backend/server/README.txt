#-------------------------------------------------------------------------------
# Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------
How to manage the ssl 

In jetty.xml add

 <New id="sslContextFactory" class="org.eclipse.jetty.http.ssl.SslContextFactory">
    <Set name="KeyStore"><Property name="jetty.home" default="." />/keystore</Set>
    <Set name="KeyStorePassword">damiano</Set>
    <Set name="KeyManagerPassword">damiano</Set>
    <Set name="TrustStore"><Property name="jetty.home" default="." />/keystore</Set>
    <Set name="TrustStorePassword">damiano</Set>
  </New>

  <Call name="addConnector">
    <Arg>
      <New class="org.eclipse.jetty.server.ssl.SslSelectChannelConnector">
        <Arg><Ref id="sslContextFactory" /></Arg>
        <Set name="Port">443</Set>
        <Set name="maxIdleTime">30000</Set>
        <Set name="Acceptors">2</Set>
        <Set name="AcceptQueueSize">100</Set>
      </New>
    </Arg>
  </Call>    
  
note that you define a keystore in jetty.home with name keystore

The following command loads a PEM encoded certificate in the jetty.crt file into a JSSE keystore:

 keytool -keystore keystore -import -alias jetty -file jetty.crt -trustcacerts
 
 See
 https://wiki.eclipse.org/Jetty/Howto/Configure_SSL 
