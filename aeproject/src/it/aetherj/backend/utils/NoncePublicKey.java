/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.utils;

import java.util.*;

/**
 * The map is first indexed by public key and then, inside, there are a list of nonces
 */
public class NoncePublicKey
  {
  private final HashMap<String,NonceTime>nonceMap;
  
  NoncePublicKey (long now_s, String nonce)
    {
    nonceMap = new HashMap<>();
    
    put(now_s, nonce);
    }
  
  public NonceTime get(String nonce)
    {
    return nonceMap.get(nonce);
    }
  
  public void put (long now_s, String nonce)
    {
    nonceMap.put(nonce, new NonceTime(now_s));
    }
  
  public int size()
    {
    return nonceMap.size();
    }
  
  public Set entrySet()
    {
    return nonceMap.entrySet();
    }
  }
