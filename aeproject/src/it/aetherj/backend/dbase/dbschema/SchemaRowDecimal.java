/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase.dbschema;

import java.sql.Types;

/**
 * The scale of a numeric is the count of decimal digits in the fractional part, to the right of the decimal point. 
 * The precision of a numeric is the total count of significant digits in the whole number, that is, 
 * the number of digits to both sides of the decimal point.  
 * So the number 23.5141 has a precision of 6 and a scale of 4
 */
public class SchemaRowDecimal  extends SchemaRow
  {
  String scalePrecision="";
  
  public SchemaRowDecimal(String col_name)
    {
    super(col_name,Types.DECIMAL);
    }

  public SchemaRowDecimal setScalePrecision(int scale, int precision)
    {
    scalePrecision = "("+scale+","+precision+")";
    
    return this;
    }
  
  @Override
  protected String getColTypeString ()
    {
    return "DECIMAL"+scalePrecision;
    }
  
  }
