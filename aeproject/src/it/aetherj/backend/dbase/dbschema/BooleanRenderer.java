/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase.dbschema;

import java.awt.Component;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.images.ImageProvider;

/**
 * This extends JLabel so you are returning the same label all the time.
 * I need a boolean renderer since we have three states to represent, true,false,null
 */
final class BooleanRenderer extends JLabel implements TableCellRenderer
  {
  private static final long serialVersionUID = 1L;
  
  private final Stat stat;
  private final ImageIcon  trueIcon,falseIcon,nullIcon;
  
  BooleanRenderer ( Stat stat )
    { 
    this.stat = stat;
    this.trueIcon = stat.imageProvider.getIcon(ImageProvider.CKTRUE);
    this.falseIcon = stat.imageProvider.getIcon(ImageProvider.CKFALSE);
    this.nullIcon = stat.imageProvider.getIcon(ImageProvider.CKNULL);
    
    // Needed to be able to paint the colors.
    super.setOpaque(true);
    }

    
  /**
   * See TableCellRenderer for the method documentation.
   */
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
    if ( isSelected )
      setBackground(table.getSelectionBackground());
    else
      setBackground(table.getBackground());
    
    if ( value == null )
      {
      setIcon(nullIcon);
      return this;
      }
    
    if ( ! (value instanceof Boolean) )
      {
      // This is a strange thing, better show it in some way.
      setText(value.toString());
      return this;
      }

    if ( ((Boolean)value).booleanValue() )
      setIcon(trueIcon);
    else
      setIcon(falseIcon);
          
    return this;
    }
  }
