/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase.dbschema;

import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;

import javax.swing.*;
import javax.swing.table.TableColumnModel;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.gui.WinposStore;
import it.aetherj.backend.gui.utils.ComponentProviderSaveWinpos;
import it.aetherj.boot.dbase.*;

/** 
 * This provides the container where to put dbase table creation...
 * I need it since I may need to configure the dbase from scratch..
 * NOTE: There is no more limit on the size of a VARCHAR, at least for hsqldb.
 * You have to look at the table model to understand better what is happening.
 */
public final class SchemaManager implements ComponentProviderSaveWinpos, ExecuteProvider
  {
  private static final String classname="SchemaManager.";
  
  private final Stat stat;
  
  private final JComponent workPanel;

  private SchemaTableModel workTableModel;
  private JTable workTable;
  private JButton previewBtn,executeBtn;
  
  private final JTextArea reportArea;
  private final JTextArea errorArea;

  private boolean isPreview;
  private boolean allTableSchemaCorrect;   // if true then table schema is correct

  private AeDbase dbase;
  
  /**
   * Constructor.
   * The idea is that this manager can act for both ammi and stor dbase.
   * I need to pass an ID instead of the actual dbase since it Fmay change during the
   * lifetime of the program, so I need to pick the current value.
   * @param dbaseId the dbase this manager is acting on.
   */
  public SchemaManager(Stat stat )
    {
    this.stat = stat;
    
    reportArea=new JTextArea(20,20);
    errorArea=new JTextArea(20,20);
    
    workPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,newChoicePanel(),newReportPanel());
    }
    

  /**
   * Adds the given SchemaProvider to the ones being managed.
   */
  public final void addProvider ( SchemaProvider provider )
    {
    workTableModel.add(provider);
    }
    
  private JComponent newChoicePanel ()
    {
    JPanel aPanel = new JPanel(new BorderLayout());

    workTableModel = new SchemaTableModel(stat);
    workTable = new JTable(workTableModel);

    workTable.addMouseListener(new TableMouseAdapter());
    workTable.getTableHeader().addMouseListener(new HeaderMouseAdapter());
    
    workTable.setDefaultRenderer(Boolean.class,new BooleanRenderer(stat));
    
    TableColumnModel colModel = workTable.getColumnModel();
    colModel.getColumn(SchemaTableModel.COL_table).setMaxWidth(50);
    colModel.getColumn(SchemaTableModel.COL_view).setMaxWidth(50);
    colModel.getColumn(SchemaTableModel.COL_constr).setMaxWidth(50);
    colModel.getColumn(SchemaTableModel.COL_index).setMaxWidth(50);
    

//    aPanel.add( workTable.getTableHeader(), BorderLayout.NORTH);
//    aPanel.add( workTable, BorderLayout.CENTER);

    aPanel.add( new JScrollPane(workTable), BorderLayout.CENTER);

    aPanel.add(newButtonPanel(),BorderLayout.SOUTH);

    return aPanel;
    }
    
    
  private final JPanel newButtonPanel ()
    {
    ExecuteButtonPress handler = new ExecuteButtonPress();
    
    executeBtn=new JButton("Execute");
    executeBtn.addActionListener(handler);
    
    previewBtn=new JButton("Preview");
    previewBtn.addActionListener(handler);
    
    JPanel aPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
    aPanel.add (previewBtn);    
    aPanel.add (executeBtn);    
      
    return aPanel;
    }
  
  /**
   * Panel where results are written
   */
  private JComponent newReportPanel ()
    {
    JTabbedPane tabbed = new JTabbedPane();

    tabbed.addTab("Report Area", new JScrollPane(reportArea));
    tabbed.addTab("Error Area", new JScrollPane(errorArea));
    
//    reportArea.setLineWrap(true);

//    return new JSplitPane(JSplitPane.VERTICAL_SPLIT,new JScrollPane(reportArea),new JScrollPane(errorArea));
    
    return tabbed;
    }
    
  @Override
  public JComponent getComponentToDisplay ()
    {
    return workPanel;
    }
    
  /**
   * Check in all the saved tables to see if the schema is correct
   * This will drop all views + indexes + foreign keys, check the tables, adjust them, and then recreate 
   * views, keys, foreign keys
   */
  public void checkTableSchemaCorrect ( AeDbase dbase ) 
    {
    this.dbase = dbase;
    
    // assume that the final result is a good schema
    allTableSchemaCorrect = true;
    
    try
      {
      dropAllNonEssentials();
      
      for (SchemaProvider provider : workTableModel  )
        {
        if ( dbTableCreated(provider) )
          continue;
        
        if ( !provider.isTableSchemaCorrect(this)) 
          allTableSchemaCorrect = false;
        }
      
      createAllNonEssentials();
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow(classname+"isTableSchemaCorrect",exc);
      allTableSchemaCorrect = false;
      }

    if ( ! allTableSchemaCorrect )
      stat.console.showMessageDialog(classname+"isTableSchemaCorrect: FAIL check table creation panel");          
    
    }
  
  /**
   * This checks if the table needs to be created fresh new and if so, does it
   * @param provider
   * @return
   * @throws SQLException 
   */
  private boolean dbTableCreated (SchemaProvider provider) throws SQLException
    {
    String table_name = provider.getTableName();

    // this means that the table is freshly created, nothing else to do
    if ( table_name == null ) return true;
    
    if ( dbase.dbTableExist(table_name) )
      return false;
    
    return provider.createTable(this);
    }

  private void dropAllNonEssentials ()
    {
    for (SchemaProvider provider : workTableModel  )
      {
      provider.dropView(this);
      provider.dropIndex(this);
      provider.dropConstraint(this);
      }
    }
    
  private void createAllNonEssentials ()
    {
    for (SchemaProvider provider : workTableModel  )
      {
      provider.createView(this);
      provider.createIndex(this);
      provider.createConstraint(this);
      }
    }

  public boolean isTableSchemaCorrect ()
    {
    return allTableSchemaCorrect;
    }

  /**
   * Execute the given select statement and return the result
   * NOTE: you MUST close the resultset once you have done with it !
   * @param select
   * @return can be null if illegal params
   */
  @Override
  public Brs executeSelect ( String select )
    {
    println (select);
    
    return dbase.select(select);
    }
 
  @Override
  public DbaseQuirks getDbaseQuirks ( ) 
    {
    return dbase.getDbaseQuirks();
    }
  
  /**
   * Executes a query and logs what happens to the log window
   */
  @Override
  public boolean executeDbaseDone ( String command )
    {
    if ( command == null ) 
      return true;
      
    println (command);
    
    // If we are doing a preview, just stop here.
    if ( isPreview ) 
      return false;
    
    Ris ris = dbase.execute(command);

    if ( ris.hasFailed() )
      printlne (command,"   "+ris);
    else
      println ("-> "+ris);
    
    ris.close();
    
    return ! ris.hasFailed();
    }

  /**
   * Utility to simplify the code
   */
  @Override
  public void println ( String text )
    {
    reportArea.append(text);
    reportArea.append("\n");
    }

  @Override
  public void appendSqlInstruction ( String instruction )
    {
    stat.sqlquery.appendSqlInstruction(instruction);      
    }


  /**
   * Utility to simplify the code
   */
  private void printlne ( String command, String error )
    {
    errorArea.append(command);
    errorArea.append("\n");
    errorArea.append(error);
    errorArea.append("\n");
    }

  /**
   * Note that you first do all DROP and then do all CREATE
   * note also that in the the order of dropped things is relevant.
   */
  private void executeButtonFun ()
    {
    if ( ! isPreview )
      {
      int risul = JOptionPane.showConfirmDialog(null,"ATTENZIONE: Le operazioni richieste MODIFICANO il database, confermi ?");
      if ( risul != JOptionPane.OK_OPTION ) return;
      }
  
    // This clears up previous old commands, if any.
    reportArea.setText("");  
    errorArea.setText("");
    
    int rowCount = workTableModel.getRowCount();
    for ( int index=0; index<rowCount; index++ )
      {
      SchemaProvider item = workTableModel.get(index);
      
      if ( item.isDropConstraint() ) item.dropConstraint(this);
      if ( item.isDropView()       ) item.dropView(this);
      if ( item.isDropIndex()      ) item.dropIndex(this);
      if ( item.isDropTable()      ) item.dropTable(this);
      }
  
    for ( int index=0; index<rowCount; index++ )
      {
      SchemaProvider item = workTableModel.get(index);
      
      if ( item.isCreateTable()      ) item.createTable(this);
      if ( item.isCreateView()       ) item.createView(this);
      if ( item.isCreateConstraint() ) item.createConstraint(this);
      if ( item.isCreateIndex()      ) item.createIndex(this);
      }
    
    }

/**
 * Button handler, it is a Class just to avoid exposing the action performed.
 */
private final class ExecuteButtonPress implements ActionListener
  {
  @Override
  public void actionPerformed(ActionEvent event)
    {
    isPreview = event.getSource() == previewBtn;
    
    executeButtonFun();
    }
  }


/**
 * This is being used to handle the clicking on the table, especially for the actionDefinition
 */
private final class TableMouseAdapter extends MouseAdapter
  {
  @Override
  public void mouseClicked(MouseEvent event)
    {
    // We want the normal button click
    if ( event.getButton() != MouseEvent.BUTTON1 ) 
      return;
    
    // No need to check if it is one or two, actually it is really better not to check, it is faster.
    //if ( event.getClickCount() != 1 ) return;

    // We would like to know if we are selecting into the action table
    int col = workTable.getSelectedColumn();
    if ( col < 0 ) 
      return;
    
    // I need to know if there is something there...
    int row = workTable.getSelectedRow();
    if ( row < 0 ) 
      return;
    
    switch ( col )
      {
      case SchemaTableModel.COL_table:
      case SchemaTableModel.COL_view:
      case SchemaTableModel.COL_constr:
      case SchemaTableModel.COL_index:
        workTableModel.toggleValueAt(row,col);
        workTableModel.fireTableCellUpdated(row,col);
        break;
    
      case SchemaTableModel.COL_kname:
        workTableModel.toggleRow(row);
        workTableModel.fireTableDataChanged();
        break;
        
      default:
        // do nothing
      }
    }    
  }

/**
 * When ones click on the header than all the rows for the given col gets toggled.
 */
private final class HeaderMouseAdapter extends MouseAdapter
  {
  @Override
  public void mouseClicked(MouseEvent event)
    {
    // We want the normal button click
    if ( event.getButton() != MouseEvent.BUTTON1 ) 
      return;
    
    // No need to check if it is one or two, actually it is really better not to check, it is faster.
    //if ( event.getClickCount() != 1 ) return;
    Point evpoint = event.getPoint();
    
    // NOTE: I am not sure that it works OK when the user swaps columns.
    int col = workTable.getTableHeader().columnAtPoint(evpoint);
    
    switch ( col )
      {
      case SchemaTableModel.COL_table:
      case SchemaTableModel.COL_view:
      case SchemaTableModel.COL_constr:
      case SchemaTableModel.COL_index:
        workTableModel.toggleColumn(col);
        workTableModel.fireTableDataChanged();
        break;
        
      default:
        // do nothing
      }
    }    
  }

  @Override
  public void saveWindowPosition(WinposStore wpstore)
    {
    }


  } 
