/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase.labels;

import java.sql.SQLException;
import java.util.*;

import javax.swing.JLabel;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.boot.dbase.*;
import it.aetherj.shared.Aeutils;
 

/**
 * There is only one factory for labels in the system.
 * Now, with the gui in here labeling must be done different, this primarly to avoid deadlock.
 * There is one real rule and this is that ...
 * 1) Label getter must NOT wait for Dbase access
 * 
 * From this follows the following.
 * 1) ALL labels for the currently selected languages MUST be loaded at initialize of the factory.
 *    If you want to change language you restart the application.
 * 2) In due time a thread will read/write from the DB and synchronize labels but this is no guaranteee
 *    that new labels will be used.
 *    
 * It is theoretically possible to have automagic label update on language change but it is way too
 * complicated and not even Windows does it (if you start a program in one language you get that one)
 * Label change would mean wrapping GUI objects into a wrapper that is registered....
 */
public class LabelFactory
  {
  private static final String classname="LabelFactory.";
  
  private final Stat stat;
  private final HashMap<String,String> dictionary_single;
	private final HashMap<String,String> dictionary_full;   // the key is english_label locale and holds the FULL set of data
  private final HashMap<String,String> mappedMap;   // map of labels/page that has already been mapped
  
  private volatile ArrayList<Mlabel> queuedLabels;     // list of labels that needs to b stored in DB
  private Thread updaterThread;
  
  public LabelFactory(Stat stat)
    {
    this.stat = stat;
    this.dictionary_single = new HashMap<String,String>();
		this.dictionary_full   = new HashMap<String,String>();
    this.mappedMap         = new HashMap<String,String>();

    queuedLabels = new ArrayList<Mlabel>(100);
    }
    
     
	private void initializeSpecificDictionary () throws SQLException
		{
		// The first thing to do is to load all labels known to the system for the current language
		Brs ars = stat.dbase.getLabelsAndTranslation(stat.config.getLabelsLocale());
		
		while ( ars.next() ) 
			{
			String label_english = ars.getString(Dbkey.label_english);
			String label_localized = ars.getString(Dbkey.lblmap_value);
			dictionary_single.put(label_english,label_localized);
			}
		
		ars.close();
		
		}


	private void initializeFullDictionary () throws SQLException
		{
    String query = "SELECT * FROM mlang.label_tbl LEFT OUTER JOIN mlang.lblmap_tbl ON lblmap_label_id=label_id ";
    
    Dprepared prepared = stat.dbase.getPreparedStatement(query);
    Brs ars = prepared.executeQuery();
		
		while ( ars.next() ) 
			{
			String label_english = ars.getString(Dbkey.label_english);
			String label_lang = ars.getString(Dbkey.lblmap_lang);
			String label_localized = ars.getString(Dbkey.lblmap_value);
			
			String label_key = label_lang == null ? label_english : label_english+label_lang;
			
			dictionary_full.put(label_key,label_localized);
			}
		
		ars.close();
		}


  /**
   * This should go into dbase and load up all labels for the current language.
   * The rest of the program can then safely use labels without waiting for db
   */
  public void initialize()
    {
    try
      {
      if ( ! stat.dbase.isConnected() )
        {
        stat.log.userPrintln(classname+"initialize: dbase not connected, aboting");  
        return;
        }
        
			initializeSpecificDictionary();
			initializeFullDictionary();
		  
      stat.log.println(classname+"initialize: labels loaded");  

      Aeutils.newThreadStart(new DbaseUpdater(), classname);
      }
    catch ( Exception exc )
      {
      stat.log.exceptionPrint(classname+"initialize",exc);
      }
    }
    
		
	/**
	 * This assumes that the server loads all dictionaries on startup
	 */
	public synchronized String getLabel ( String from_page, String label_english, String want_lang )
		{
		// if for some reason the language is null try to recover
		if ( want_lang == null ) return getLabel(from_page, label_english	);
		
    // if it is a null one, strange but possible
    if (label_english == null) return "";
    
    // if it is an empty one, possible if setting unused controls
    if (label_english.length() < 1) return "";
    
    // this is used to report of page existence
    addLabel(label_english,from_page);

		// pick up using specific dictionary
		String label_key = label_english+want_lang;
		
    String risul = dictionary_full.get(label_key);
		
    if ( risul != null ) return risul;

		return label_english;
		}
    
  /**
   * This is what is called from around the program.
   * If a label exist
   *   a) Update its usage time if not done already (and put it into the update list)
   *   b) If the path dos not exist update it and put into the path update queue
   * If label does not exist
   *   a) Register it in the map and put it into the update queue
   *   b) Add also the path
   *   WARNING: TODO Labels here ARE case sensitive, make sure the DBASE is also case sensitive !!!!
   */
  public synchronized String getLabel(String from_page, String label_english)
    {
    // if it is a null one, strange but possible
    if (label_english == null) return "";
    
    // if it is an empty one, possible if setting unused controls
    if (label_english.length() < 1) return "";
    
    // this is used to report of page existence
    addLabel(label_english,from_page);

    String risul = dictionary_single.get(label_english);
    if ( risul != null ) return risul;
    
    return label_english;
    }
  
  private void addLabel ( String label_english, String from_class )
    {
    String key = label_english+from_class;
    
    // if this label is already added to the map of mapped labels then there is nothing to do
    if ( mappedMap.containsKey(key) ) return;
    
    // add the key to the map values, thre is really no value to save
    mappedMap.put(key,null);
    
    Mlabel label          = new Mlabel();
    label.label_english   = label_english;
    label.label_from_page = "/Jrnc/"+from_class;
    label.label_value     = null;
    queuedLabels.add(label);
    }
  
  
    
  public JLabel newJLabel (String path, String label_english)
    {
    return new JLabel(getLabel(path,label_english));
    }
    
  /**
   * Used for debugging, be careful since it prints all labels actually stored
   * @return
   */
  public String toString ()
    {
    StringBuilder builder = new StringBuilder(10000);
    
    builder.append("Current locale="+stat.config.getLabelsLocale());
    builder.append("\n");
    
    return builder.toString();
    }


  private void saveCurrentLabels (AeDbase dbase)
    {
    // test if nothing to do at the moment
    if ( queuedLabels == null || queuedLabels.size() < 1 ) return;
    
    // set aside the current queue
    ArrayList<Mlabel> workList = queuedLabels;
    
    // now detach the previous array list from the current class, let me try in a not sync way
    queuedLabels = new ArrayList<Mlabel>(100);
    
    Locale locale = stat.config.getLabelsLocale();
    
    try
      {
      int labelCount = workList.size();
      
      // now I can work on it, hopefully
      for ( int index=0; index<labelCount; index++ )
        {
        Mlabel label = workList.get(index);
        dbase.labelSave(locale,label.label_english,label.label_from_page,label.label_value);
        }
      
      stat.log.userPrintln(classname+"saveCurrentLabels: saved "+labelCount+" labels ");
      }
    catch ( Exception exc )
      {
      stat.log.exceptionPrint(classname+"saveCurrentLabels: ",exc);        
      }
    }



/**
 * This should check what there is to do and then do it.
 * The main issue is shared data access to arraylist....
 */
private class DbaseUpdater implements Runnable
  {
  public void run()
    {
    AeDbase adbase = stat.newWorkDbase();
    
    if ( adbase.isClosed() )
      {
      stat.println(classname+"DbaseUpdater: ERROR dbase is closed, return");
      return;
      }
      
    // there is no point to work immediately after init, sleep a bit !
    while ( ! stat.waitHaveShutdownReq_s(30) )
      saveCurrentLabels(adbase);
    
    adbase.close();
    }

  }


  }
