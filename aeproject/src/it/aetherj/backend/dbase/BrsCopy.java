/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase;

import java.sql.*;
import java.util.ArrayList;

import it.aetherj.backend.Stat;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;

/**
 * This should provide the needed support to copy a Brs (source) into a destination table.
 * Note that the destination table is probably going to be in a separate database.
 * Note it should work even if the destination is on the same database (concurrency issues ???)
 *
 * basically to allow this the statement should be crafted depending on source and destination rs.
 * The most interesting issue is the one of speeding up work, avoid redoing all the mapping if not needed.
 * To do this a number of assumptions are made
 * - A BrsCopy will work with a well defined database/table and will not change it once allocated.
 * - The structure of the given Brs in the copy MUST remain the same from call to call.
 *
 * NOTE: An type mapping: it may happen that the src/dest table do not have identical set of types, but compatible
 * it should be possible to alter the compatibility mapping so it is possible to do teh insert even if the types
 * are not exactly the same.
 *
 * NOTE: It is quite interesting to have the possibility to create the destination table on the fly...
 * I have decided against it, there are just too many cases for it and too much magic going on.
 */
public final class BrsCopy 
  {
  private static final String classname="BrsCopy.";
  
  private final Stat   stat;
  private final AeDbase destDbase;
  private final String destTableName;
  private final ArrayList<DbaseCol> destCols;

  private ArrayList<DbaseCol> srcCols;
  private ArrayList<DbaseCol> mergedCols;    // The columns that we can actually get from src and copy to dest
  private String insertQuery;
  
  /**  
   * Constructor:
   * @param destTableName the table where to insert the data.
   * @param destDbase the database providing the given table.
   * @param stat
   */
  public BrsCopy( Stat stat, AeDbase destDbase, String destTableName )
    {
    this.stat = stat;
    this.destDbase = destDbase;
    this.destTableName = destTableName;
    
    destCols = getDestCols();
    }

  public AeDbase getDbase ()
    {
    return destDbase;
    }
  
  /**
   * Returns true if the service is offline.
   * It returns true if no destDbase is given or if no destCols have been found.
   * @return 
   */
  public boolean isOffline ()
    {
    return (destDbase == null) || (destCols == null);
    }
   
  public int copy ( Brs aRs ) throws SQLException
    {
    return copy(aRs,null); 
    }
 
  /**
   * Inserts the given Brs into the given table of given database.
   * If there is NO cached mapping a new one will be generated and stored.
   * (remember of multithreading issues)
   * Remember the issue of closing resultset...
   * Possibly the batching... but up to a point to avoid memory issues, let's lay 100 records a time.
   * @return the number of rows inserted
   */
  public int copy ( Brs aRs, PrintlnProvider printer ) throws SQLException
    {
    if ( printer == null )
      printer = stat.log;
    
    if ( aRs.hasFailed() ) 
      {
      printer.println(classname+"copy(aRs)",aRs.getException());
      return 0;
      }
        
    int insertCount=0;
    srcCols     = getSrcCols( aRs.getMetaData() );
    mergedCols  = newMergedCols ();
    insertQuery = newInsertQuery();
    
    Dprepared dstatement = destDbase.getPreparedStatement(insertQuery);
    PreparedStatement statement = dstatement.getPreparedStatement();
    while (aRs.next() )
      {
      fillOneRow ( statement, aRs );
      statement.addBatch();
      insertCount++;
      }
  
    try
      {
      // This will ensure that all insert are queued one after the other.
      destDbase.synchronizedExecuteBatch(statement);
      }
    catch ( SQLException exc )
      {
      for ( Throwable aex : exc )
        printer.println("exc: ",aex);
      }
    
    return insertCount;
    }
  
  
  /**
   * Fill one row of the statement
   */
  private void fillOneRow ( PreparedStatement statement, Brs srcRs ) throws SQLException
    {
    int colCount=mergedCols.size();
    for (int index=0; index<colCount; index++ )
      {
      DbaseCol aCol=mergedCols.get(index);
      Object obj = srcRs.getObject(aCol.srcIndex);
      statement.setObject(index+1,obj,aCol.colType);
      }
    }

  /**
   * This creates the insert statement from the merged cols.
   * NOTE: The result must be cached.
   */
  private String newInsertQuery ()
    {
    StringBuffer risul=new StringBuffer(200);
  
    risul.append("INSERT INTO "+destTableName+" ( ");
    
    int colCount=mergedCols.size();
    int lastColumnIndex = colCount-1;
    
    for (int index=0; index<colCount; index++ )
      {
      DbaseCol aCol=mergedCols.get(index);
      risul.append(aCol.colName);
      if ( index< lastColumnIndex ) risul.append(",");
      }
      
    risul.append(" ) VALUES ( ");
    
    for (int index=0; index<colCount; index++ )
      {
      risul.append("?");
      if ( index< lastColumnIndex ) risul.append(",");
      }
    
    risul.append(" ) ");
    
    return risul.toString();
    }
    
    
    
  /**
   * Go trough all destCols and try to pick the corresponding src col.
   * If there is NO source for a destination then it is NOT copied.
   * NOTE: Transform this to produce an array, it will be faster to scan.
   */
  private ArrayList<DbaseCol> newMergedCols ()
    {
    int colCount=destCols.size();

    ArrayList<DbaseCol> risul = new ArrayList<DbaseCol>(colCount);
    
    for (int index=0; index<colCount; index++ )
      {
      DbaseCol aCol=destCols.get(index);
      
      if ( ! srcColMatches ( aCol )) continue;
      
      risul.add(aCol);
      }
      
    return risul;
    }



  /**
   * Check if there is a match from the source cols for this dest col.
   */
  private boolean srcColMatches ( DbaseCol destCol ) 
    {
    DbaseQuirks quirk = destDbase.getDbaseQuirks();
    
    int colCount=srcCols.size();
    for (int index=0; index<colCount; index++ )
      {
      DbaseCol srcCol=srcCols.get(index);
      
      if ( ! destCol.isNameEqual(srcCol.colName) ) continue;
      
      if ( ! quirk.isTypeCompatible(srcCol.colType, destCol.colType )  )
        {
        stat.log.userPrintln(classname+"srcColMatches() STRANGE col="+destCol.colName+" tbl="+destTableName+" dty="+destCol.colType+" sty="+srcCol.colType);
        continue;
        }
      
      destCol.srcIndex = index+1;
      return true;
      }
    
    stat.log.userPrintln(classname+"srcColMatches() NO match in src for dest col="+destCol.colName+" tbl="+destTableName);
    return false;
    }


  /**
   * Gets the columns from a resultset meta data.
   * @throws java.sql.SQLException
   * @return 
   * @param meta
   */
  private ArrayList<DbaseCol> getSrcCols ( ResultSetMetaData meta ) throws SQLException
    {
    if ( meta == null ) 
      throw new IllegalArgumentException(classname+"getSrcCols() meta==null");
    
    ArrayList<DbaseCol> risul = new ArrayList<DbaseCol>(30);
    int colCount = meta.getColumnCount();
    
    for (int index=0; index<colCount; index++ )
      {
      String colName = meta.getColumnName(index+1);
      int colType = meta.getColumnType(index+1);
      risul.add(new DbaseCol(colName,colType));
      }

    return risul;    
    }
    
    
    
  /**
   * Given the destDbase and destTable try to get the columns information
   * @return an array of cols or null if the dbase cannot be found or some failure.
   */
  public ArrayList<DbaseCol> getDestCols ()
    {
    // It may be possible, reasonably that the dbase is null.
    if ( destDbase==null ) return null;
    
    Brs aRs = destDbase.getTableSchema(destTableName);
    if ( aRs.hasFailed() ) 
      {
      System.err.println (classname+"getDestCols() getTableSchema("+destTableName+") exc="+aRs.getException());
      aRs.close();
      return null;
      }
      
    ArrayList<DbaseCol> risul = new ArrayList<DbaseCol>(30);
    while ( aRs.next() )
      {
      String colName = aRs.getString("COLUMN_NAME");
      // DATA_TYPE returns the Types.XXX type
      Integer data_type = aRs.getInteger("DATA_TYPE");
      if ( data_type != null )
        risul.add(new DbaseCol(colName,data_type.intValue()));
      else
        risul.add(new DbaseCol(colName,Types.OTHER));
      }
    aRs.close();

    if ( risul.size() < 1 ) 
      System.err.println (classname+"getDestCols() getTableSchema("+destTableName+") STRANGE no columns...");
      
    return risul;
    }
  
  /**
   * Tiny utility that can be used from within configBsh, prints the columns.
   */
  public void printCols ( ArrayList<DbaseCol> cols )
    {
    for (DbaseCol col : cols )
      System.out.println ("col: "+col);
    }
  
/**
 * Holds information about a dbase column
 */
public static final class DbaseCol
  {
  final String colName;
  final int colType;
  
  // This is the index where you can find the source col in the rouce resultset.
  int srcIndex;
  
  DbaseCol ( String colName, int colType )
    {
    this.colName = colName;
    this.colType = colType;
    }
  
  /**
   * I am in BrsCopy and all names are "dbase case equivalent", no quoted names
   * So, comparison should be done in ignore case mode
   * @param other
   * @return
   */
  public boolean isNameEqual ( String other )
    {
    if ( colName == null || other == null ) 
      return false;

    return colName.equalsIgnoreCase(other);
    }
    
  @Override
  public String toString ()
    {
    return "cn="+colName+" ty="+colType+" si="+srcIndex;
    }
  }
  
  
  
  }
