/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbase;

import java.io.*;
import java.sql.*;

import it.aetherj.backend.Stat;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;


/**
 * This should check current DB version and do all the steps that are needed to update the schema to the proper value
 * 1) Retrieve current dbase version
 * 2) apply the transition from step N to step N+1
 * repeat the above until there is no update file to execute
 */
public class DbaseSchemaUpdater 
  {
  private static final String classname="DbaseSchemaUpdater.";
  private static final int max_update_iterations = 99;
  
  private final Stat stat;
  private final PrintlnProvider logpanel;
  
  public static final String first_dbase_version = "20080808";   // YYYYMMDD
  
  public DbaseSchemaUpdater(Stat stat, PrintlnProvider logpanel)
    {
    this.stat = stat;
    this.logpanel = logpanel;
    }

  /**
   * Returns current dbase version
   * @return a string that represents the current DB version, always not null value
   */
  private String getCurrentDbaseVersion ()
    {
    String risul = first_dbase_version; 
    
    AeDbase dbase = stat.newWorkDbase();
    
    if ( ! dbase.isConnected() )
      {
      logpanel.println("NO dbase available, aborting");
      return "";        
      }
    
    Brs brs = dbase.select("SELECT * FROM "+DbProperty.property_tbl+" WHERE "+DbProperty.property_id+"='"+DbProperty.property_dbase_version+"'");
    
    if ( brs.next() ) risul = brs.getString(DbProperty.property_value);
    brs.close();
    
    if ( risul == null ) risul = first_dbase_version;
      
    dbase.close();
    
    return risul;
    }


  /**
   * Read a script line from a file, a script line is any number of lines that terminates with GO
   * WARNING: Do NOT add spaces on empty lines, it will be considered a script but it is not 
   */
  private String readScriptLine ( BufferedReader input ) throws IOException
    {
    StringBuilder risul = new StringBuilder(2000);
    String line = null;
    boolean appendNewline=false;
    
    while ( (line = input.readLine()) != null  )
      {
      // skip out empty reading lines
      if ( line.length() < 1 ) continue;
      
      // skip comments
      if ( line.startsWith("//")) continue;
      
      // execute the statement if it has a single GO initself
      if ( line.equals("GO")) break;
      
      // this is the second line, so I must append a newline to the previous
      if ( appendNewline ) risul.append("\n");

      risul.append(line); 
      
      appendNewline=true;
      }
    
    // if there is nothing in it just return null  
    if ( risul.length() < 1 ) return null;
    
    return risul.toString();
    }

  /**
   * I ned to connect, execute and disconnect
   * @param scriptLine
   * @throws SQLException 
   */
  private boolean executeOneLineFailed ( String scriptLine ) throws SQLException
    {
    logpanel.println("  line="+scriptLine);
    
    AeDbase dbase = stat.newWorkDbase();
    
    if ( ! dbase.isConnected() )
      {
      logpanel.println("NO dbase available, aborting");
      return true;        
      }

    Statement statement = dbase.getStatement();
    
    statement.addBatch(scriptLine);
    statement.executeBatch();
    statement.clearBatch();

    statement.close();

    dbase.close();
    
    return false;
    }
  
  
  
	/**
	 * CREATE DEFAULT, CREATE FUNCTION, CREATE PROCEDURE, CREATE RULE, CREATE TRIGGER, and CREATE VIEW statements 
	 * cannot be combined with other statements in a batch. 
	 * The CREATE statement must start the batch. 
	 * All other statements that follow in that batch will be interpreted as part of the definition of the first 
	 * CREATE statement.
	 * Therefore I need a way to read a script and ISOLATE the above and run them in a single instruction
	 * @param reader
	 */
	private boolean executeBatchChunk ( BufferedReader reader ) throws Exception
		{

    logpanel.println("executeBatchChunk: CALL");

    String scriptLine;
    boolean haveScript=false;  // assume there is nothing to do
    while ( (scriptLine=readScriptLine(reader)) != null )
      {
      haveScript=true; 

      if ( executeOneLineFailed(scriptLine) )
        break;
      
      }

    logpanel.println("executeBatchChunk: END");

		return haveScript;
		}

  /**
   * I have a dbase version, let me see if there is an update script for this database version
   * It MUST return false if there is nothing more to do on the dbase even if there is a fatal update error
   * @return true if the caller should continue the database update
   */
  private boolean continueDatabaseUpdate ( String current_db_version ) throws Exception
    {
    File scriptFile = new File(stat.boot.getProgramDir(),"dbase-update-script/update-"+current_db_version+".sql");
    
    // if the file does not exist then there is nothing to update
    if ( ! scriptFile.canRead() ) return false;
    
    logpanel.println("continueDatabaseUpdate: open script "+scriptFile);
    BufferedReader reader = new BufferedReader(new FileReader(scriptFile));
    
    boolean haveScript=false;  // assume there is nothing to do
		boolean chunkExecuted;
		
		do
		  {
      chunkExecuted = executeBatchChunk(reader);
			
			haveScript = haveScript | chunkExecuted;
			
	  	} while (chunkExecuted);

    reader.close();
    
    // If I had a script then after one update I have to check if there is a further update !
    return haveScript;
    }


  /**
   * Updates dbase schema to be in sync with the application
   * @return the current database version, so you can display it
   * @throws Exception
   */
  public String updateSchema() throws Exception
    {
    String previous_db_version=null;
    int index;
    
    logpanel.println(classname+"updateSchema: CALL");

    for (index=0; index<max_update_iterations; index++)
      {
      String current_db_version = getCurrentDbaseVersion();
      
      if ( current_db_version.length() < 1 )
        {
        // It should always be possible to get the dbase version !
        // NOTE: Maybe I should push a version ? but really, which one ?????
        logpanel.println(classname+"run: ERROR current_db_version="+current_db_version);
        return "DBASE ERROR";
        }

      if ( previous_db_version != null && previous_db_version.equals(current_db_version))
        {
        logpanel.println(classname+"run: ERROR UNCHANGING current_db_version="+current_db_version);
        return current_db_version;          
        }
        
      logpanel.println("  Checking Dbase "+current_db_version);      
    
      // if I should not continue to update the dbase then just get out
      if ( ! continueDatabaseUpdate(current_db_version)) 
        return current_db_version;
      
      // now remember that the previous is equal the current to activate the antilock mechanism
      previous_db_version = current_db_version;
      }
      
    // If this happens probably the update script ends up in a cicular chain of update numbers
    logpanel.println(classname+"run: ERROR having more than "+max_update_iterations+" cycles, really unlikely !!!");

    return previous_db_version;
    }
  }
