/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.utils;

import java.awt.*;

import javax.swing.*;

/**
 * Utility class to make handling of gridbag easier
 */
public class SwingGridBagLayout
  {
  public final GridBagConstraints gbc = new GridBagConstraints();

  private final GridBagLayout layout;
  private final JPanel risul;
  
  public int anchorEast = GridBagConstraints.EAST;   // you can set more specific value if you wish so
  public int anchorWest = GridBagConstraints.WEST;

  
  public SwingGridBagLayout ()
    {
    layout = new GridBagLayout();
    risul = new JPanel(layout);
    
    gbc.gridx=0; 
    gbc.gridy=0;
    }
  
  public SwingGridBagLayout addComponentIncx ( Component component )
    {
    risul.add(component,gbc);
    
    gbc.gridx++;    
    
    return this;
    }

  public SwingGridBagLayout addComponentEastIncx ( JComponent aComponent )
    {
    return addComponentIncx(aComponent,anchorEast);
    }

  public SwingGridBagLayout addComponentWestIncx ( JComponent aComponent )
    {
    return addComponentIncx(aComponent, anchorWest);
    }

  public SwingGridBagLayout addComponentIncx ( Component aComponent, int anchor )
    {
    int saveAnchor = gbc.anchor;
    gbc.anchor = anchor;
    layout.setConstraints(aComponent, gbc);
    risul.add(aComponent);
    gbc.gridx++;    
    gbc.anchor = saveAnchor;
    
    return this;
    }

  public void setGridwidth ( int w_x )
    {
    gbc.gridwidth=w_x;
    }
  
  /**
   * See GridBagConstraints.fill
   * @param fill
   */
  public void setFill ( int fill )
    {
    gbc.fill = fill;
    }
  
  public SwingGridBagLayout newline ()
    {
    gbc.gridx=0;
    gbc.gridy++;
    
    return this;
    }
  
  
  public JPanel getJPanel ()
    {
    return risul;
    }

  }
