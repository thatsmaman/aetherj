/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.utils;

import java.awt.BorderLayout;

import javax.imageio.ImageIO;
import javax.swing.*;

import it.aetherj.backend.gui.WinposStore;
import it.aetherj.boot.ComponentProvider;

/**
 * Used to have an icon set on the Window and toolbar
 */
public class ConsoleFrame extends JFrame
  {
  private static final long serialVersionUID = 1L;

  public ConsoleFrame(String name, int do_something_on_close)
    {
    super(name);

    try
      {
      setIconImage(ImageIO.read(this.getClass().getResource("aetherlogo.png")));
      } 
    catch (Exception ex)
      {
      // do something
      }

    super.setDefaultCloseOperation(do_something_on_close);
    }

  public ConsoleFrame(String name)
    {
    // normally I want to do nothing, I catch the logic 
    this(name,WindowConstants.DO_NOTHING_ON_CLOSE);
    }

  public void saveWindowPosition(WinposStore wpstore)
    {
    wpstore.putWindowBounds(super.getTitle(),super.getBounds());
    }

  public void setBounds ( WinposStore wpstore )
    {
    super.setBounds(wpstore.getWindowBounds(super.getTitle()));
    }
  
  
  public void setCenterPanel ( JComponent provider )
    {
    if ( provider == null )
      return;
    
    JPanel workPanel = (JPanel)this.getContentPane();

    workPanel.removeAll();
    
    workPanel.add(provider,BorderLayout.CENTER);
    }
  
  public void setCenterPanel ( ComponentProvider provider )
    {
    if ( provider == null )
      return;
    
    setCenterPanel(provider.getComponentToDisplay());
    }
    
  
  
  }
