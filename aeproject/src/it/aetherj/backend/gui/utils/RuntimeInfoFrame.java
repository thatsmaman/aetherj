/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.utils;

import java.awt.*;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.shared.*;


/**
 * This is meant to display and update information about the program, mostly to diagnose memory leaking
 */
public class RuntimeInfoFrame
	{
  private final String classname="RuntimeInfoFrame";
  private final Stat  stat;

  private final ConsoleFrame workFrame;  
  private final JPanel       workPanel;  
	private final JTextField   memMaxField,memTotalField,memFreeField,memUsedField,spinnerField;
	private final JTextField   activeThreadsField;
	
	private final Thread      infoThread;
  
  /**
   * What will be available is a panel where languages are managed.
   */
  public RuntimeInfoFrame(Stat stat)
    {
    this.stat = stat;

    workFrame = new ConsoleFrame ("Backend Information Panel",JFrame.HIDE_ON_CLOSE);

    workPanel = (JPanel)workFrame.getContentPane();

		memMaxField   = newJTextField(15);
		memMaxField.setToolTipText("Maximum memory that can be allocated by the JVM in this system");
		
		memTotalField = newJTextField(15);
		memTotalField.setToolTipText("Memory currently allocated from the OS");
		
		memFreeField  = newJTextField(15);
		memTotalField.setToolTipText("Memory that is free to be used up to the above limit");
		
		memUsedField  = newJTextField(15);
		memUsedField.setToolTipText("Memory that is currently being used");

		activeThreadsField  = newJTextField(15);
		activeThreadsField.setToolTipText("Active threads in the current thread group");

		spinnerField  = newJTextField(5);

		workPanel.add(newWorkPanel(),BorderLayout.CENTER);

		workFrame.pack();
		workFrame.setLocationRelativeTo(null);   // place the window in the center of screen
		
		infoThread=Aeutils.newThreadStart(new InfoRunner(), "Backend Information");
    }

	
	public void setVisible ( boolean visible )
		{
		workFrame.setVisible(visible);			
		}


	private JTextField newJTextField ( int columns )
		{
		return new JTextField(columns);			
		}

	private JLabel getJLabel(String text)
		{
		return new JLabel(text);			
		}

	private JPanel newWorkPanel ()
		{
		SwingGridBagLayout gbr = new SwingGridBagLayout();
		gbr.gbc.fill = GridBagConstraints.HORIZONTAL;
    
    gbr.addComponentEastIncx(getJLabel("Memory Max"));
    gbr.addComponentWestIncx(memMaxField).newline();

    gbr.addComponentEastIncx(getJLabel("Memory Total"));
    gbr.addComponentWestIncx(memTotalField).newline();

    gbr.addComponentEastIncx(getJLabel("Memory Free"));
    gbr.addComponentWestIncx(memFreeField).newline();

    gbr.addComponentEastIncx(getJLabel("Memory Used"));
    gbr.addComponentWestIncx(memUsedField).newline();

    gbr.addComponentEastIncx(getJLabel("Active Threads"));
    gbr.addComponentWestIncx(activeThreadsField).newline();

    gbr.addComponentEastIncx(getJLabel("Spinner"));
    gbr.addComponentWestIncx(spinnerField).newline();


		return gbr.getJPanel();
		}


	private void runner () throws InterruptedException
		{
		int spinner = 0;
		
		Runtime runtime = Runtime.getRuntime();
		
		for(;;)
			{
			long freeMem  = runtime.freeMemory() / 1024;
			long totalMem = runtime.totalMemory() / 1024;		
			long maxMem   = runtime.maxMemory() / 1024;		
			long memUsed  = totalMem - freeMem;
			
			memTotalField.setText(totalMem+" Kbytes");
			memMaxField.setText(maxMem+" Kbytes");
			memFreeField.setText(freeMem+" Kbytes");
			memUsedField.setText(memUsed+" Kbytes");
			
			activeThreadsField.setText(Integer.toString(Thread.activeCount()));
			
			spinnerField.setText(Integer.toString(spinner++));				
			
			Thread.sleep(5000);
			}
		}
		

private final class InfoRunner implements Runnable
  {
	public void run()
		{
		try
			{
			runner();
			}
		catch ( Exception exc )
			{
			stat.log.exceptionPrint(classname+"run", exc);
			}
		}
  }

	}
