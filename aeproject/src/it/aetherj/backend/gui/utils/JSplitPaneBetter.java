/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.utils;

import java.awt.*;

import javax.swing.JSplitPane;

public class JSplitPaneBetter extends JSplitPane
	{
  private static final long serialVersionUID = 1L;

  public JSplitPaneBetter(int newOrientation, Component component, Component component1)
		{
		super(newOrientation, component, component1);
		}

	boolean isPainted;
	boolean hasProportionalLocation;
	double  proportionalLocation;
	
	public void setDividerLocation(double proportionalLocation) 
		{
    if ( isPainted) 
			{       
			super.setDividerLocation(proportionalLocation);
			return;			
			}

		hasProportionalLocation = true;
		this.proportionalLocation = proportionalLocation;
		}

    public void paint(Graphics g) 
			{
      if (!isPainted) 
				{       
				if (hasProportionalLocation) super.setDividerLocation(proportionalLocation);
				isPainted = true;
        }
		super.paint(g);
    } 

	}
