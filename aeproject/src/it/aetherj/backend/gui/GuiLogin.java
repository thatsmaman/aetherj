/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.gui.images.ImageProvider;
import it.aetherj.backend.gui.utils.ComponentProviderSaveWinpos;
 

/**
 * Used to preset the login page and then authenticate
 */
public final class GuiLogin implements ComponentProviderSaveWinpos,ActionListener
  {
  private static final String classname="GuiLogin.";
  
  private final Stat stat;

  private JPanel         workPanel;
  private JButton        loginButton;
  private JTextField     usernameField;
  private JPasswordField passwordField;
  private JLabel         messageLabel;
  private Integer        curAeusrId;
	private String         curUsername;
  
  public GuiLogin(Stat stat)
    {
    this.stat = stat;
    this.workPanel = newWorkPanel();
    }

  /**
   * Returns the current wana_id that has access to the system
   * @return
   */
  public Integer getWanaId ()
    {
    return curAeusrId;
    }

  /**
   * Used to decide if there is an authenticated user
   * @return
   */
  public boolean hasWanaId ()
    {
    return getWanaId() != null;
    }
  
  
  public String getUsername()
		{
		return curUsername;			
		}

  /**
   * You can "logout" a user by calling this method.
   */
  public void logoutWana ()
    {
    curAeusrId = null;  
		curUsername = null;

    // after logout it is correct to show the login window
    stat.console.setContent(this);
    }


  /**
   * Small utility to wrap the fact that it is actually duty of Login to check if we are authenticated or not
   */
  public void showGuiIfAuthenticated ( ComponentProviderSaveWinpos newGui )
    {
    if ( getWanaId() == null )
      stat.console.setContent(this);
    else
      stat.console.setContent(newGui);
    }


    
  private JPanel newCenterPanel()
    {
    GridBagLayout layout = new GridBagLayout();
    JPanel risul = new JPanel(layout);
//    risul.setOpaque(false);
		risul.setBorder(new CompoundBorder(new LineBorder(Color.red),new EmptyBorder(10,10,10,10)));
		Color yellow = new Color(0xfa,0xce, 0x78);
		risul.setBackground(yellow);

    GridBagConstraints gbc = new GridBagConstraints();

    // ==========================================
		JLabel label = new JLabel(stat.imageProvider.getIcon(ImageProvider.LOGOMEDIUM));
		label.setBorder(new EmptyBorder(0,0,10,0));
    gbc.gridx=0; gbc.gridy=0;  gbc.gridwidth=3;
    layout.setConstraints(label, gbc);
    risul.add(label);

    // ==========================================
		JLabel logoLabel = new JLabel(stat.imageProvider.getIcon(ImageProvider.LOGOMEDIUM));
		logoLabel.setBorder(new EmptyBorder(0,0,10,10));
    gbc.gridx=0; gbc.gridy++;  gbc.gridheight=5; gbc.gridwidth=1;
    layout.setConstraints(logoLabel, gbc);
    risul.add(logoLabel);

    // ==========================================
    JLabel alabel = stat.labelFactory.newJLabel(classname,"Login on Site Support");
    alabel.setFont(alabel.getFont().deriveFont(Font.BOLD,14f));
    alabel.setBorder(new EmptyBorder(0,0,5,0));
    gbc.gridx++;  gbc.gridheight=1; gbc.gridwidth=2; gbc.anchor=GridBagConstraints.WEST;
    layout.setConstraints(alabel, gbc);
    risul.add(alabel);

    // ===========================================
    messageLabel = stat.labelFactory.newJLabel(classname,"Insert Username/Password and press Login button");
//    alabel.setFont(alabel.getFont().deriveFont(Font.BOLD,14f));
    messageLabel.setBorder(new EmptyBorder(4,0,4,0));
    gbc.gridx=1; gbc.gridy++; gbc.gridwidth=2; gbc.anchor=GridBagConstraints.WEST;
    layout.setConstraints(messageLabel, gbc);
    risul.add(messageLabel);

    // ========================================
    alabel = stat.labelFactory.newJLabel(classname,"Username");
    alabel.setBorder(new EmptyBorder(4,0,4,4));
    gbc.gridx=1; gbc.gridy++; gbc.anchor=GridBagConstraints.EAST; gbc.gridwidth=1;
    layout.setConstraints(alabel, gbc);
    risul.add(alabel);

    usernameField = new JTextField("",15);
    gbc.gridx++; gbc.anchor=GridBagConstraints.WEST;
    layout.setConstraints(usernameField, gbc);
    risul.add(usernameField);

    // =======================================
    
    alabel = stat.labelFactory.newJLabel(classname,"Password");
    alabel.setBorder(new EmptyBorder(4,0,4,4));
    gbc.gridx=1; gbc.gridy++; gbc.anchor=GridBagConstraints.EAST; 
    layout.setConstraints(alabel, gbc);
    risul.add(alabel);

    passwordField = new JPasswordField(15);
    gbc.gridx++; gbc.anchor=GridBagConstraints.WEST;
    layout.setConstraints(passwordField, gbc);
    risul.add(passwordField);


    // ============================================
    loginButton = new JButton(stat.labelFactory.getLabel(classname,"Login"));
    loginButton.addActionListener(this);
    gbc.gridx=2; gbc.gridy++; gbc.anchor=GridBagConstraints.WEST; 
    layout.setConstraints(loginButton, gbc);
    risul.add(loginButton);
    
    return risul;
    }
  
  private JPanel newWorkPanel()
    {
    GridBagLayout layout = new GridBagLayout();
    JPanel risul = new JPanel(layout);
    risul.setToolTipText(classname);
    risul.setBackground(new Color(0xececec));
    
    GridBagConstraints gbc = new GridBagConstraints();

    Component compo = Box.createVerticalGlue();
    gbc.gridx=0; gbc.gridy=0;  gbc.gridwidth=3;
    layout.setConstraints(compo, gbc);
    risul.add(compo);

    JComponent inner = newCenterPanel();
    gbc.gridx=1; gbc.gridy=1;  gbc.gridwidth=3;
    layout.setConstraints(inner, gbc);
    risul.add(inner);
    
    return risul;
    }

  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }
    
  public void actionPerformed(ActionEvent event)
    {
    String password = new String(passwordField.getPassword());

    // attempt login
    new GuiLoginCheck(password).execute();
    }

  @Override
  public void saveWindowPosition(WinposStore wpstore)
    {
    // there is nothing to do
    }


private class GuiLoginCheck extends SwingWorker
  {
  private String password;
  
  GuiLoginCheck ( String password )
    {
    this.password = password;
    }
    
  protected Object doInBackground()
    {
    try
      {
      AeDbase dbase = stat.newWorkDbase();
  
      curUsername = usernameField.getText();

      curAeusrId = stat.aedbFactory.isGoodUsernamePassword(dbase,curUsername,password);
      
      dbase.close();
      }
    catch ( Exception exc )
      {
      stat.log.exceptionPrint(classname+"GuiLoginCheck",exc);
      }
    return null;
    }

  protected void done()
    {
    try
      {
      if ( curAeusrId == null )
        {
        messageLabel.setText("Incorrect Username / Password");
        return;
        }
      
      passwordField.setText(null);
      messageLabel.setText("Insert Username / Password");
      stat.console.setContent(stat.backendPanel);
      }
    catch ( Exception exc )
      {
      stat.log.exceptionPrint(classname+"done",exc);
      }
    }
  }



  
  }
