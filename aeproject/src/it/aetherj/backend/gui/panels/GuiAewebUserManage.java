/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.panels;

import java.awt.event.*;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.AedbWebuser;
import it.aetherj.backend.gui.WinposStore;
import it.aetherj.backend.gui.utils.*;
import it.aetherj.shared.Aeutils;
 

/**
 * Used to change basic info on a web user
 */
public final class GuiAewebUserManage implements ComponentProviderSaveWinpos,ActionListener
  {
  private static final String classname="GuiAewebUserManage.";
  
  private final Stat stat;

  private final JPanel      workPanel;
  private final JTextField  idField,loginField,passwordField;
  private final JLabel      messageLabel;

  private JButton        saveButton;
  private Integer        curAeusrId;
  
  public GuiAewebUserManage(Stat stat)
    {
    this.stat = stat;
  
    idField = new JTextField(5);
    loginField = new JTextField(20);
    passwordField = new JTextField(40);
    messageLabel = new JLabel();

    saveButton = new JButton(stat.labelFactory.getLabel(classname,"Save"));
    saveButton.addActionListener(this);

    workPanel = newWorkPanel();
    }
  
  private JLabel newJLabel(String text)
    {
    //return stat.labelFactory.newJLabel(classname, text);
    return new JLabel(text);
    }
    
  private JPanel newWorkPanel()
    {
    SwingGridBagLayout layout = new SwingGridBagLayout();

    layout.addComponentEastIncx(newJLabel("user ID"));
    layout.addComponentWestIncx(idField).newline();
    
    layout.addComponentEastIncx(newJLabel("User Login"));
    layout.addComponentWestIncx(loginField).newline();

    layout.addComponentEastIncx(newJLabel("Password"));
    layout.addComponentWestIncx(passwordField).newline();

    layout.gbc.gridx++;
    layout.addComponentWestIncx(saveButton).newline();
    
    layout.gbc.gridwidth=2;
    layout.addComponentIncx(messageLabel);
    
    return layout.getJPanel();
    }
  
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }
    
  public void actionPerformed(ActionEvent event)
    {
    String login = loginField.getText();
    String password = passwordField.getText();

    Integer user_id = Aeutils.convStringToInteger(idField.getText(),null);
    
    new ChangeUserLoginPassword(user_id, login, password).execute();
    }

  @Override
  public void saveWindowPosition(WinposStore wpstore)
    {
    // there is nothing to do
    }


private class ChangeUserLoginPassword extends SwingWorker
  {
  private Integer user_id;
  private String login,password;
  
  ChangeUserLoginPassword ( Integer user_id, String login, String password )
    {
    this.user_id = user_id;
    this.login = login;
    this.password = password;
    }
    
  protected Object doInBackground()
    {
    try
      {
      AeDbase dbase = stat.newWorkDbase();
  
      AedbWebuser db = stat.aedbFactory.aewebUser;
      
      curAeusrId = db.recordSet(dbase, user_id, login, password);

      dbase.close();
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow(classname+"ChangeUserLoginPassword",exc);
      }
    return null;
    }

  protected void done()
    {
    try
      {
      if ( curAeusrId == null )
        {
        messageLabel.setText("Incorrect Username / Password");
        return;
        }
      
      passwordField.setText(null);
      messageLabel.setText("Data Updated id="+curAeusrId);
//      stat.console.setContent(stat.backendPanel);
      }
    catch ( Exception exc )
      {
      stat.log.exceptionShow(classname+"done",exc);
      }
    }
  }



  
  }
