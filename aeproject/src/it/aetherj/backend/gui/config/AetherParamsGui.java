package it.aetherj.backend.gui.config;

import java.awt.event.*;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.gui.utils.SwingGridBagLayout;
import it.aetherj.boot.ComponentProvider;
import it.aetherj.shared.Aeutils;

public class AetherParamsGui implements ComponentProvider
  {
  private static final String classname = "AetherParamsGui";
  
  private final ButtonListener buttonListener = new ButtonListener();

  private final Stat stat;
  private final AetherParams params;
  
  private JPanel         workPanel;
  private JButton        saveButton;
  
  private JTextField     bootstrapField;
  private JTextField     bestAddressCountField;

  public AetherParamsGui(Stat stat, AetherParams params )
    {
    this.stat = stat;
    this.params = params;
    
    saveButton = stat.utils.newJButton("Save", buttonListener);
    bootstrapField = new JTextField(40);
    bestAddressCountField = new JTextField(5);
    
    this.workPanel = newWorkPanel();
    }

  /**
   * Called by the datastore when a load or save is performed
   */
  public void refreshGui ()
    {
    bootstrapField.setText(params.getBootLocation());
    bestAddressCountField.setText(""+params.getBestAddressCount());
    }
    
  private JLabel newJLabel(String eng)
    {
    return stat.labelFactory.newJLabel(classname, eng);
    }
  
  private JPanel newWorkPanel()
    {
    SwingGridBagLayout layout = new SwingGridBagLayout();
    
    layout.addComponentEastIncx(newJLabel("Bootstrap address"));
    layout.addComponentWestIncx(bootstrapField).newline();

    layout.addComponentEastIncx(newJLabel("Best Addresses Count"));
    layout.addComponentWestIncx(bestAddressCountField).newline();

    layout.setGridwidth(2);
    layout.addComponentEastIncx(saveButton);
    
    return layout.getJPanel();
    }
  
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }

private final class ButtonListener implements ActionListener
  {
  @Override
  public void actionPerformed(ActionEvent event)
    {
    Object source = event.getSource();
    
    if ( source == saveButton )
      {
      params.setBootLocation(bootstrapField.getText());
      params.setBestAddressCount(bestAddressCountField.getText());
      
      Aeutils.newThreadStart(new SaveParamsRun(), "SaveParamsRun");
      }
    }
  }
  
  
private final class SaveParamsRun implements Runnable
  {
  public void run()
    {
    try
      {
      params.saveToDbase();
      }
    catch ( Exception exc )
      {
      stat.println("SaveParamsRun", exc);
      }
    }
  }
  
  }