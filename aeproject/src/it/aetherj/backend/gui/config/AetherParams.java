/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.config;

import java.net.URL;
import java.security.*;
import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.protocol.MimFingerprint;
import it.aetherj.shared.Aeutils;
import it.aetherj.shared.v0.*;

/**
 * Store all config here, note that they are stored in a key/value mode in the dbase
 * So, this MUST be initialized after the dbase is ready
 * You should call initialize() once at start and then saveToDbase, if needed
 */
public class AetherParams
  {
  private static final String classname="AetherParams";

  private static final String key_MimBootURL="MimBootLocation";
  private static final String val_MimBootURL="10.0.0.191:37107";
  private static final String key_backendTcpPort = "backendTcpPort";
  private static final String key_gwtWebTcpPort = "gwtWebTcpPort";
  private static final String key_backendPublicKey = "BackendPublicKey";
  private static final String key_backendPrivateKey = "BackendPrivateKey";
  private static final String key_bestAddressCount = "BestAddressCountKey";
  
  private static final int val_bestAddressCountMin = 3;

  private final Stat stat;
  private final PrintlnProvider feedback;
  
  private int backendTcpPort;   // the TCP port the back end is listening to
  public  int gwtWebTcpPort;    // the tcp port of the GWT server
  
  // From Mim Protocol Documentation: Node ID is the fingerprint of the backend's public key
  private MimFingerprint backendNodeId;    

  public AetherParamsKeys publicPrivateKeys;
  
  private String bootLocation;
  private int bestAddressCount;   // howmany best addresses do we wish to work ?
  
  public AetherParams(Stat stat, PrintlnProvider feedback)
    {
    this.stat = stat;
    this.feedback = feedback;
    }
    
  public int getBestAddressCount()
    {
    if ( bestAddressCount < val_bestAddressCountMin )
      bestAddressCount=val_bestAddressCountMin;
    
    if ( bestAddressCount > 20 )
      bestAddressCount=20;
    
    return bestAddressCount;
    }

  public void setBestAddressCount ( String value )
    {
    bestAddressCount = Aeutils.convStringToInteger(value, val_bestAddressCountMin);
    // the following will adjust, if needed
    getBestAddressCount();
    }
    
  public String getBootLocation()
    {
    if ( bootLocation == null )
      bootLocation=val_MimBootURL;
    
    return bootLocation;
    }
  
  public void setBootLocation (String location )
    {
    try
      {
      if ( ! location.startsWith("https://"))
        location="https://"+location;

      URL url = new URL(location);
      
      String address = url.getHost();
      int port=url.getPort();
      
      bootLocation=address+":"+port;
      }
    catch ( Exception exc )
      {
      feedback.println("setBootLocation", exc);
      }
    }
  
  public MimAddress getBackendAddress ()
    {
    return new MimAddress(backendTcpPort);
    }
    
  public MimPublicKey getMimPublicKey ()
    {
    return publicPrivateKeys.getMimPublicKey();
    }
  
  public PrivateKey getPrivateKey ()
    {
    return publicPrivateKeys.getPrivate();
    }
    
  public int getBackendPort ()
    {
    return backendTcpPort;
    }
  
  public void setBackendPort ( int port )
    {
    if ( port < 0 || port > 0xFFFF )
      throw new IllegalArgumentException("Invalid port "+port);
    
    backendTcpPort=port;
    }
  
  public boolean initializeDone(AeDbase dbase)
    {
    backendTcpPort = stat.dbProperty.getProperty(dbase, key_backendTcpPort, 0);
    gwtWebTcpPort = stat.dbProperty.getProperty(dbase, key_gwtWebTcpPort, 0);
    bootLocation = stat.dbProperty.getProperty(dbase, key_MimBootURL, val_MimBootURL);
    bestAddressCount=stat.dbProperty.getProperty(dbase, key_bestAddressCount, val_bestAddressCountMin);
    
    publicPrivateKeys = initializePublicPrivateKeys(dbase);

    backendNodeId = getMimPublicKey().getFingerprint();
    
    feedback.println("NodeId="+getNodeId());
    
    stat.aeParamsGui.refreshGui();

    return true;  
    }
   
  private AetherParamsKeys initializePublicPrivateKeys (AeDbase dbase)
    {
    String private_s = stat.dbProperty.getProperty(dbase, key_backendPrivateKey, "");
    String public_s = stat.dbProperty.getProperty(dbase, key_backendPublicKey, "");
    
    try
      {
      PrivateKey priv_k = CryptoEddsa25519.parsePrivateKey(private_s);
      PublicKey public_k = CryptoEddsa25519.parsePublicKey(public_s);
      return new AetherParamsKeys(feedback, public_k, priv_k);
      }
    catch ( Exception exc )
      {
      feedback.println(classname+".initializePublicPrivateKeys");
      feedback.println("   Creating new Public and private key");
      return new AetherParamsKeys(feedback);
      }
    }
      
  public MimFingerprint getNodeId ()
    {
    return backendNodeId;
    }
      
  public void saveToDbase() throws SQLException
    {
    AeDbase dbase = stat.newWorkDbase();
    
    if ( ! dbase.isConnected() )
      {
      feedback.println(classname+".saveToDbase: ABORT > dbase is NOT connected");
      return;
      }
    
    stat.dbProperty.setProperty(dbase, key_backendTcpPort, backendTcpPort);
    stat.dbProperty.setProperty(dbase, key_gwtWebTcpPort, gwtWebTcpPort);
    stat.dbProperty.setProperty(dbase, key_MimBootURL, bootLocation);
    stat.dbProperty.setProperty(dbase, key_bestAddressCount, bestAddressCount);
    
    String privk = CryptoEddsa25519.toHex(publicPrivateKeys.getPrivate());
    stat.dbProperty.setProperty(dbase, key_backendPrivateKey, privk);
    
    String pubk = CryptoEddsa25519.toHex(publicPrivateKeys.getPublic());
    stat.dbProperty.setProperty(dbase, key_backendPublicKey, pubk);
    
    dbase.close();
    
    feedback.println(classname+".saveToDbase(): DONE");
    }
  
  public String toString()
    {
    StringBuilder risul = new StringBuilder(500);
    
    risul.append("backendTcpPort="+backendTcpPort);
    risul.append(" gwtWebTcpPort="+gwtWebTcpPort);
    
    return risul.toString();
    }
    
  
  
  }
