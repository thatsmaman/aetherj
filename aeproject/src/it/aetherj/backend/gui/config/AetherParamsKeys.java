/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.gui.config;

import java.security.*;

import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.shared.v0.MimPublicKey;

public class AetherParamsKeys
  {
  private static final String classname="AetherParamsKeys";
  
  private final PrintlnProvider feedback;
  
  private PublicKey publicKey;
  private PrivateKey privateKey;
  
  public AetherParamsKeys(PrintlnProvider feedback)
    {
    this.feedback = feedback;
    
    try 
      {
      KeyPair kp =CryptoEddsa25519.newKeyPair();
      publicKey = kp.getPublic();
      privateKey = kp.getPrivate();
      }
    catch ( Exception exc )
      {
      feedback.println(classname+".construct", exc);
      }
    }
  

  public AetherParamsKeys(PrintlnProvider feedback, PublicKey publicKey, PrivateKey privateKey)
    {
    this.feedback = feedback;
    
    this.privateKey = privateKey;
    this.publicKey = publicKey;
    }
  
  public PrivateKey getPrivate ()
    {
    return privateKey;
    }
  
  public PublicKey getPublic ()
    {
    return publicKey;
    }
  
  public MimPublicKey getMimPublicKey ()
    {
    return new MimPublicKey(publicKey);
    }
    
  }
