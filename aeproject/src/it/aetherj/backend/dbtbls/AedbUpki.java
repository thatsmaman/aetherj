/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * Some object in the system needs to have a couple of private and public key, for whatever reason
 * It also happens that users in Aether are idintified by a public key
 * Since it is used to identify somebody, there should be just ONE usrpki for each object....
 * Or, if you want two of them... well, have to references to it
 */
public final class AedbUpki extends AedbOperations
  {
  private static final String classname="AedbUpki";
  
  public static final MimEntity mimEntity = MimEntityList.aekey;
  
  public static final String cn_tbl          = "upki_tbl";
  public static final String cn_id           = "upki_id";
  public static final String cn_fingerprint  = "upki_fingerprint";   // searchable using MimFingerprint
  public static final String cn_type         = "upki_type";
  public static final String cn_upki_public  = "upki_public";        // the public part of a key pair
  public static final String cn_expire       = "upki_expire";
  public static final String cn_user_name    = "upki_user_name";
  public static final String cn_user_info    = "upki_user_info";
  public static final String cn_creation     = "upki_creation";
  public static final String cn_pow          = "upki_pow";
  public static final String cn_signature    = "upki_signature";

  public static final String cn_updla_time   = "upki_updla_time";
  public static final String cn_updla_pow    = "upki_updla_pow";
  public static final String cn_updla_sign   = "upki_updla_sign";
  
  public static final String cn_upki_private  = "upki_private";       // the private part of a pair, if given
  public static final String cn_upki_X509cert = "upki_X509cert";      // the certificate that has the above public key, if any
  public static final String cn_note          = "upki_note";
 
  private static final String upki_search_view = "upki_search_view";

  private static final String query_pkey="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_upki_public+"=?";
  private static final String query_fprint="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_fingerprint+"=?";

  
  public AedbUpki ( Stat stat, PrintlnProvider println )
    {
    super(stat,println,mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getEntityDesc ()
    {
    return "User Pki Handler";
    }


  @Override
  public final String getMainTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  public Integer getPrimaryId (AeDbase dbase, MimPublicKey srch_val ) throws SQLException
    {
    if ( srch_val == null || srch_val.isNull() ) return null;
    
    String query="SELECT "+cn_id+" FROM "+cn_tbl+" WHERE "+cn_upki_public+"=?";
    
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    prepared.setBinary(1, srch_val.getPub_A_bytes());
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(getPrimaryColName());
    
    ars.close();
    
    return risul;
    }

  public Integer getPrimaryId (AeDbase dbase, MimFingerprint srch_val ) throws SQLException
    {
    if ( srch_val == null ) 
      return null;
    
    return getPrimaryIdUsing(dbase,query_fprint,srch_val.getFingerprint());
    }
  
  /**
   * This is used when inserting a partial info to satisfy the foreign key relationship
   */
  public Integer insert(AeDbase dbase, MimFingerprint fprint, MimPublicKey key_s ) throws SQLException
    {
    if ( fprint == null || fprint.isNull() )
      throw new IllegalArgumentException("Cannot insert null FIngerprint");

    if ( key_s == null || key_s.isNull() )
      throw new IllegalArgumentException("Cannot insert null public key");
      
    String query = "INSERT INTO  "+cn_tbl+" ( "+cn_fingerprint+","+cn_upki_public+" ) VALUES ( ?,? ) ";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,fprint);
    prepared.setValue(insIndex++,key_s);
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();
    
    // here I may queue for further full info
    stat.mcwishConsole.add(mimEntity, fprint);

    return primary_id;
    }
    
  private Integer getPrimaryIdUsing (AeDbase dbase, String query, byte[] srch_val ) throws SQLException
    {
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    prepared.setBinary(1, srch_val);
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(getPrimaryColName());
    
    ars.close();
    
    return risul;
    }

  
  /**
   * Return the ID of the row inserted or the ID of the row that is already present
  public Integer recordSet ( AeDbase dbase, MimPublicKey pk ) throws SQLException
    {
    if ( pk == null || pk.isNull() )
      return null;
    
    Integer row_id = getPrimaryIdUsing(dbase, query_pkey, pk.getPub_A_bytes());
    
    if ( row_id != null )
      return row_id;
    
    return insertPublicKey(dbase, pk);
    }
   */

  
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj == null ) 
      return null;
    
    if (! ( obj instanceof MimKey ) )
      {
      println.println(classname+"dbaseSave: unsupported class "+obj.getClass());
      return null;
      }
    
    return recordSet(dbase, (MimKey)obj);
    }
  
  
  
  public Integer recordSet ( AeDbase dbase, MimKey row ) throws SQLException
    {
    if ( row == null )
      return null;
    
    Integer row_id = getPrimaryIdUsing(dbase, query_fprint, row.fingerprint.getFingerprint());
    
    if ( row_id != null )
      dbaseUpdate(dbase,row_id,row);
    else
      row_id=dbaseInsert(dbase,row);
    
    return row_id;
    }
    
  private void dbaseUpdate ( AeDbase dbase, Integer prim_id, MimKey row ) throws SQLException
    {
    // TODO
    }
  
  private Integer dbaseInsert ( AeDbase dbase, MimKey row) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_type+","+
       cn_upki_public+","+
       cn_expire+","+
       cn_user_name+","+
       cn_user_info+","+
       cn_creation+","+
       cn_pow+","+
       cn_signature+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,? )";
     
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setString(insIndex++,row.type);
    prepared.setValue(insIndex++,row.key);
    prepared.setValue(insIndex++,row.expiry);
    prepared.setString(insIndex++,row.name);
    prepared.setString(insIndex++,row.info);
    prepared.setValue(insIndex++,row.creation);
    prepared.setValue(insIndex++,row.proof_of_work);
    prepared.setValue(insIndex++,row.signature);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }

  
  
  @Override
  public final String getListViewName ()
    {
    return upki_search_view;
    } 

/**
 * Need to add unique to public key  
 */
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowVarbinary(cn_fingerprint,MimFingerprint.fingerprint_bytes_len));
    addSchemaRow(new SchemaRowVarchar(cn_type,20));
    addSchemaRow(new SchemaRowVarbinary(cn_upki_public,MimPublicKey.pub_key_bytes_len)).setNotNullable();
    addSchemaRow(new SchemaRowTimestamp(cn_expire));
    addSchemaRow(new SchemaRowVarchar(cn_user_name,80));  
    addSchemaRow(new SchemaRowVarchar(cn_user_info,32000));
    addSchemaRow(new SchemaRowTimestamp(cn_creation));
    addSchemaRow(new SchemaRowVarbinary(cn_signature,MimSignature.signature_len));  
    addSchemaRow(new SchemaRowVarchar(cn_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarchar(cn_upki_private,200));  // I may not have a private key
    addSchemaRow(new SchemaRowVarchar(cn_upki_X509cert,1000));
    addSchemaRow(new SchemaRowVarchar(cn_note,500));
    }

  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+upki_search_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+upki_search_view+" AS "+ 
    "SELECT * FROM upki_tbl ");
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String  query="CREATE UNIQUE INDEX upki_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);

    /*
    query="CREATE UNIQUE INDEX upki_pub_index ON "+cn_tbl+" ( "+cn_upki_public+" ) ";
    dbexec.executeDbaseDone(query);
    */
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
//    dbexec.executeDbaseDone("DROP INDEX upki_pub_index");
    dbexec.executeDbaseDone("DROP INDEX upki_fingerprint_index");
    }  
  }
      
      
  } 

