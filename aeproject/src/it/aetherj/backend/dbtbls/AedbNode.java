/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * 
 * It happens that there is no Node in mim, BUT logically there is one 
 * My aenode has a public key and a private key, but those are stored in config
 * This is for nodes that are OUTSIDE, so, I need to store the aenode public key and the aeaddres
 * It is maybe possible that a aenode has many addresses ? maybe
 * 
 * to see the values in hex from the query window
 * select rawtohex(node_fingerprint) from node_tbl
 */
public final class AedbNode extends AedbOperations
  {
  private static final String classname="AedbNode";
  
  public static final MimEntity mimEntity = MimEntityList.aenode;
  
  public static final String cn_tbl         = "aenode_tbl";
  public static final String cn_id          = "aenode_id";
  public static final String cn_fingerprint = "aenode_fingerprint"; 
  public static final String cn_name        = "aenode_name";
  public static final String cn_pki_public  = "aenode_pki_public";  
  public static final String cn_note        = "aenode_note";
 
  private static final String cn_search_view = "aenode_search_view";
  
  public AedbNode ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getEntityDesc ()
    {
    return "Node Handler";
    }


  @Override
  public final String getMainTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  
  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getListViewName ()
    {
    return cn_search_view;
    } 

  @Override
  public Integer dbaseSave ( AeDbase dbase, Object object) throws SQLException
    {
    if ( object instanceof MimPayload )
      return dbaseSave(dbase,(MimPayload)object);
    
    return null;
    }

  private Integer dbaseSave ( AeDbase dbase, MimPayload row) throws SQLException
    {
    MimPublicKey pk = row.node_public_key;
    
    MimFingerprint fingerprint = pk.getFingerprint();
    
    Integer row_id = getPrimaryId(dbase, fingerprint);
    
    if ( row_id == null )
      row_id = dbaseInsert(dbase, fingerprint, row.node_public_key );
    else
      dbaseUpdate(dbase );
    
    return row_id;
    }

  private void dbaseUpdate ( AeDbase dbase ) throws SQLException
    {
    }
  
  private Integer dbaseInsert ( AeDbase dbase, MimFingerprint fprint, MimPublicKey pkey ) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_pki_public+","+
       cn_note+
       " ) VALUES ( ?,?,? )";
     
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,fprint);
    prepared.setValue(insIndex++,pkey);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();
    
    return primary_id;
    }
  
    
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowVarbinary(cn_fingerprint,MimFingerprint.fingerprint_bytes_len)).setNotNullable();
    addSchemaRow(new SchemaRowVarchar(cn_name,80));  
    addSchemaRow(new SchemaRowVarbinary(cn_pki_public,MimPublicKey.pub_key_bytes_len)).setNotNullable();
    addSchemaRow(new SchemaRowVarchar(cn_note,500));
    }
  
  
  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+cn_search_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+cn_search_view+" AS "+ 
      "SELECT node_tbl.*");
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX node_pub_index ON "+cn_tbl+" ( "+cn_pki_public+" ) ";
    dbexec.executeDbaseDone(query);
    
    query="CREATE UNIQUE INDEX node_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX node_pub_index");
    dbexec.executeDbaseDone("DROP INDEX node_fingerprint_index");
    }  
  
  
  
  }
      
      
  } 

