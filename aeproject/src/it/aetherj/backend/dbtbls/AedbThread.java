/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.MimThread;

/**
 * Some object in the system needs to have a couple of private and public key, for whatever reason
 * So, it refers to this table with a specific ID to refer to it
 * Since it is used to identify somebody, there should be just ONE dbepki for each object....
 * Or, if you want two of them... well, have to references to it
 */
public final class AedbThread extends AedbOperations
  {
  private static final String classname="AedbThread";
  
  public static final MimEntity mimEntity = MimEntityList.aethread;
  
  public static final String cn_tbl  = "aethread_tbl";
  public static final String cn_id          = "aethread_id";
  
  public static final String cn_fingerprint = "aethread_fingerprint";
  public static final String cn_creation    = "aethread_creation";
  public static final String cn_pow         = "aethread_pow";
  public static final String cn_signature   = "aethread_signature";
  
  public static final String cn_board_id    = "aethread_board_id";   // this thread refers to this board
  public static final String cn_name        = "aethread_name";
  public static final String cn_link        = "aethread_link";
  public static final String cn_body        = "aethread_body";
  public static final String cn_owner_id    = "aethread_owner_id";   // this thread is owned by this user
  public static final String cn_note        = "aethread_note";
 
  private static final String aethread_search_view = "aethread_search_view";
  
  private AedbUpki aedbPki;      // needed to manage the pki relations
  private AedbBoard aedbBoard;  // needed to manage the board relations
  
  public AedbThread ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbPki = stat.aedbFactory.aedbUpki;
    aedbBoard = stat.aedbFactory.aedbBoard;
    }

  @Override
  public final String getEntityDesc ()
    {
    return "Aether Thread";
    }


  @Override
  public final String getMainTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getListViewName ()
    {
    return aethread_search_view;
    } 

  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj == null ) 
      return null;
    
    if (! ( obj instanceof MimThread ) )
      {
      println.println(classname+"dbaseSave: unsupported class "+obj.getClass());
      return null;
      }
    
    MimThread row = (MimThread)obj;
    
    Integer prim_id=getPrimaryId(dbase, row.fingerprint);
    
    if ( prim_id == null )
      return dbaseInsert(dbase, row);
    else 
      return dbaseUpdate(dbase, prim_id, row);
    }
  
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimThread row) throws SQLException
    {
    return prim_id;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, MimThread row) throws SQLException
    {
    Integer owner_id = aedbPki.getPrimaryId(dbase, row.owner);
    
    if ( owner_id == null )
      owner_id = aedbPki.insert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      {
      // queue the request for board info
      stat.mcwishConsole.add(MimEntityList.aeboard, row.board);
      throw new IllegalArgumentException("Cannot insert Thread if I do not have the Board ");
      }

    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_board_id+","+
       cn_name+","+
       cn_link+","+
       cn_body+","+
       cn_owner_id+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setInt(insIndex++,board_id);
    prepared.setString(insIndex++,row.name);
    prepared.setString(insIndex++,row.link);
    prepared.setString(insIndex++,row.body);
    prepared.setInt(insIndex++,owner_id);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  public Brs select ( AeDbase dbase, int w_board_id )
    {
    if ( w_board_id == 0 )
       return dbase.select("SELECT * FROM "+aethread_search_view);
    else
      return dbase.select("SELECT * FROM "+aethread_search_view+" WHERE "+cn_board_id+"="+w_board_id);
    }
  
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);

    addSchemaRow(new SchemaRowInteger(cn_board_id)).setNotNullable();
    addSchemaRow(new SchemaRowVarchar(cn_name,200));  
    addSchemaRow(new SchemaRowVarchar(cn_link,500));
    addSchemaRow(new SchemaRowVarchar(cn_body,65535));
    addSchemaRow(new SchemaRowInteger(cn_owner_id)).setNotNullable();
    addSchemaRow(new SchemaRowVarchar(cn_note,500));
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+aethread_search_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+aethread_search_view+" AS "+ 
        " SELECT * FROM "+cn_tbl+
        " LEFT OUTER JOIN "+AedbBoard.cn_tbl+" ON "+cn_board_id+"="+AedbBoard.cn_id
        );
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX thread_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX thread_fingerprint_index");
    }  
  
  }
      
      
  } 

