/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * Some object in the system needs to have a couple of private and public key, for whatever reason
 * So, it refers to this table with a specific ID to refer to it
 * Since it is used to identify somebody, there should be just ONE dbepki for each object....
 * Or, if you want two of them... well, have to references to it
 */
public final class AedbBoard extends AedbOperations
  {
  private static final String classname="AedbBoard";
  
  public static final MimEntity mimEntity = MimEntityList.aeboard;
  
  public static final String cn_tbl          = "aeboard_tbl";
  public static final String cn_id           = "aeboard_id";
  public static final String cn_fingerprint  = "aeboard_fingerprint";
  public static final String cn_name         = "aeboard_name";
  public static final String cn_owner_id     = "aeboard_owner_pki_id";  // refers to a public key, if not present, insert it   
  public static final String cn_desc         = "aeboard_desc";  // description
  public static final String cn_creation     = "aeboard_creation";
  public static final String cn_pow          = "aeboard_pow";
  public static final String cn_signature    = "aeboard_signature";
  public static final String cn_update_time  = "aeboard_update_time";
  public static final String cn_update_pow   = "aeboard_update_pow";
  public static final String cn_update_sign  = "aeboard_update_sign";
  public static final String cn_receive_time = "aeboard_receive_time";
  public static final String cn_note         = "aeboard_note";
   
  private static final String aeboard_search_view = "aeboard_search_view";
  
  private AedbUpki aedbPki;   // needed to manage the pki relations
  
  public AedbBoard ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity); 
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbPki = (AedbUpki)stat.aedbFactory.getAedb(AedbUpki.mimEntity);
    }

  @Override
  public final String getEntityDesc ()
    {
    return "Aether Board";
    }


  @Override
  public final String getMainTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  
  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getListViewName ()
    {
    return aeboard_search_view;
    } 

  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj == null ) 
      return null;
    
    if ( obj instanceof MimBoard )
      return dbaseSave(dbase, (MimBoard)obj);

    if ( obj instanceof MimBoardIndex )
      return dbaseSave(dbase, (MimBoardIndex)obj);

    println.println(classname+"dbaseSave: unsupported class "+obj.getClass());
    return null;
    }

  private Integer dbaseSave ( AeDbase dbase, MimBoardIndex row) throws SQLException
    {
    throw new SQLException("UNSUPPORTED");
    
//    return null;
    }
  
  public Integer dbaseSave ( AeDbase dbase, MimBoard row) throws SQLException
    {
    Integer prim_id=getPrimaryId(dbase, row.fingerprint);
    
    if ( prim_id == null )
      return dbaseInsert(dbase, row);
    else 
      return dbaseUpdate(dbase, prim_id, row);
    }
    
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimBoard board) throws SQLException
    {
    return prim_id;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, MimBoard board) throws SQLException
    {
    Integer pki_id = aedbPki.getPrimaryId(dbase, board.owner);
    
    if ( pki_id == null )
      pki_id = aedbPki.insert(dbase, board.owner, board.owner_publickey);
    
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_name+","+
       cn_owner_id+","+
       cn_desc+","+
       cn_update_time+","+
       cn_update_pow+","+
       cn_update_sign+","+
       cn_receive_time+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,board.fingerprint);
    prepared.setValue(insIndex++,board.creation);
    prepared.setString(insIndex++,board.name);
    prepared.setInt(insIndex++,pki_id);
    prepared.setString(insIndex++,board.description);
    prepared.setValue(insIndex++,board.last_update);
    prepared.setValue(insIndex++,board.update_proof_of_work);
    prepared.setValue(insIndex++,board.update_signature);
    prepared.setTimestamp(insIndex++);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  public Brs select ( AeDbase dbase )
    {
    String query = "SELECT * FROM "+aeboard_search_view;
    
    return dbase.select(query);
    }
    
    
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);
    
    addSchemaRow(new SchemaRowVarchar(cn_name,255));  
    addSchemaRow(new SchemaRowInteger(cn_owner_id)).setNotNullable();  // need to add a foreign key
    addSchemaRow(new SchemaRowVarchar(cn_desc,65535));
    addSchemaRow(new SchemaRowTimestamp(cn_update_time));
    addSchemaRow(new SchemaRowVarchar(cn_update_pow,MimPowValue.pow_ascii_len));
    addSchemaRow(new SchemaRowVarbinary(cn_update_sign,MimSignature.signature_len));
    addSchemaRow(new SchemaRowTimestamp(cn_receive_time));
    addSchemaRow(new SchemaRowVarchar(cn_note,500));
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+aeboard_search_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+aeboard_search_view+" AS "+ 
        " SELECT * FROM "+cn_tbl+
        " LEFT OUTER JOIN "+AedbUpki.cn_tbl+" ON "+cn_owner_id+"="+AedbUpki.cn_id
        );
    }
  
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX board_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX board_fingerprint_index");
    }  

  }
      
      
  } 

