/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.HashMapKeyProvider;

public abstract class AedbOperations implements HashMapKeyProvider
  {
  protected final Stat stat;

  public final MimEntity dbaseEntity;

  protected final PrintlnProvider println;

  /**
   * The Entity uniquely identify what this operation is about
   */
  public AedbOperations ( Stat stat, PrintlnProvider println, MimEntity mimEntity )
    {
    this.stat = stat;
    this.println = println;
    this.dbaseEntity = mimEntity;
    }

  @Override
  public String getHashMapKey()
    {
    return dbaseEntity.getValue();
    }
  
  /**
   * Subclasses should give me a description of what they are doing in a human readable form.
   * If not overridden then return some meaningful base value
   */
  protected String getEntityDesc ()
    {
    return "entity: "+dbaseEntity;
    }

  /**
   * Search in the table for the values bound to fingerprinting
   * @param dbase
   * @param srch_val
   * @return
   * @throws SQLException
   */
  protected Integer getPrimaryId (AeDbase dbase, MimFingerprint srch_val ) throws SQLException
    {
    if ( srch_val == null )
      return null;
    
    String colname=getFingerprintColName();
    
    if ( colname==null )
      return null;
    
    return getPrimaryId(dbase, colname, srch_val.toString());
    }
  
  protected Integer getPrimaryId (AeDbase dbase, String srch_col, String srch_val ) throws SQLException
    {
    StringBuilder query = new StringBuilder(500);
    query.append("SELECT ");
    query.append(getPrimaryColName());
    query.append(" FROM ");
    query.append(getMainTableName());
    query.append(" WHERE ");
    query.append(srch_col+"=?");
    
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    
    prepared.setString(1, srch_val);
    
    Brs ars = prepared.executeQuery();
    
    Integer risul = null;
    
    if ( ars.next() ) 
      risul=ars.getInteger(getPrimaryColName());
    
    ars.close();
    
    return risul;
    }
  
  public int getRowCount (AeDbase dbase) throws SQLException
    {
    String query = "SELECT COUNT(*) FROM "+getMainTableName();
  
    Dprepared prepared=dbase.getPreparedStatement(query.toString());
    Brs ars = prepared.executeQuery();

    Integer risul = 1;
    
    if ( ars.next() ) 
      risul=ars.getInteger(1);
    
    ars.close();
    
    return risul.intValue();
    }
  
  /**
   * Subclasses must tell to the rest of the system what is the main "table".
   * One of the main use of the main table is to perform delete, an assumption is that it is really a table..
   * @return a String that will be used to perform delete on records, can be null meaning no delete.
   */
  protected abstract String getMainTableName ();

  /**
   * Subclasses must tell me what is the primary column name, it will be used to mark such column as primary
   * when listes or details are returned.
   * @return a String representing the column name or an exception if this is not available.
   */
  protected abstract String getPrimaryColName ();
  
  /**
   * Aether has quite often, possibly always, a fingerprint on a post, a unique key
   * @return the column name in the table where this value is stored
   */
  protected abstract String getFingerprintColName ();
  
  /**
   * Subclasses MUST tell to the rest of the system what is the name of the view that will be used for listing.
   * @return a string with a view that should be used to do the listing.
   */
  protected abstract String getListViewName ();
  
  /**
   * Subclasses that wish to initialize things once all classes are setup should do something on this one.
   * It will be called after ALL handlers have been setup but before dbase begins operate.
   * What you typically do is to retrieve references to objects that you need to work, Eg: storico 
   */
  public void initialize()
    {
    }

  /**
   * release any allocated resources before being let go by the rest of program
   */
  public void dispose ()
    {
    }
  
  /**
   * Attempt to save the given object, returns the saved or updated primary id
   */
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj == null )
      throw new IllegalArgumentException("dbaseSave obj is null");
    
    throw new IllegalArgumentException("dbaseSave is NOT implemented for "+obj.getClass());
    }
  
  
  
  }
