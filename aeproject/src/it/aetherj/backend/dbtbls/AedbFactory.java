/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.util.HashMap;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.protocol.MimEntity;
import it.aetherj.shared.v0.MimGetEntity;

public class AedbFactory
  {
  private static final String classname = "AedbFactory.";

  private final Stat stat;
  private final PrintlnProvider print;
  private final HashMap<String, AedbOperations> pgrpMap = new HashMap<>();

  public final AedbWebuser aewebUser;
  public final AedbAddress aedbAddress;
  public final AedbNode aedbNode;
  public final AedbUpki aedbUpki;
  public final AedbBoard aedbBoard;
  public final AedbThread aedbThread;
  public final AedbPost aedbPost;
  public final AedbTstamp aedbTstamp;
  
  /**
   * @param println
   */
  public AedbFactory(Stat stat, PrintlnProvider println)
    {
    this.stat = stat;
    this.print = println;

    // this holds the login code, so, it is a bit more hard wired
    aewebUser   = new AedbWebuser(stat, println);
    aedbAddress = new AedbAddress(stat, println);
    aedbNode    = new AedbNode(stat, println);
    aedbUpki    = new AedbUpki(stat, println);
    aedbBoard   = new AedbBoard(stat, println);
    aedbThread  = new AedbThread(stat, println);
    aedbPost    = new AedbPost(stat, println);
    aedbTstamp  = new AedbTstamp(stat, println);
    
    // from here on I just attach all known rpcdb tables to the map, so later the check on creation can be done
    addRpcdbTable(aedbNode);
    addRpcdbTable(aedbAddress);
    addRpcdbTable(aedbUpki);
    addRpcdbTable(aedbThread);
    addRpcdbTable(aedbBoard);
    addRpcdbTable(aedbPost);
    addRpcdbTable(aewebUser);
    addRpcdbTable(aedbTstamp);
    addRpcdbTable(new AedbVote(stat, println));
    addRpcdbTable(new AedbMimuser(stat, println));
    }

  public Integer isGoodUsernamePassword (AeDbase dbase, String username, String password )
    {
    return aewebUser.isGoodUsernamePassword(dbase, username, password);
    }
    
  private void addRpcdbTable(AedbOperations atable)
    {
    // NOTE that I add what should be a unique map key
    pgrpMap.put(atable.getHashMapKey(), atable);
    }

  public AedbOperations getAedb(MimGetEntity obj)
    {
    return getAedb(obj.getMimEntity());
    }

  public AedbOperations getAedb(MimEntity obj)
    {
    return pgrpMap.get(obj.getValue());
    }

  public void initialize()
    {
    for ( AedbOperations handler : pgrpMap.values()  )
      handler.initialize();
  
    }

  /**
   * an aid to garbage collect, call this before releasing. It is an aid, in
   * theory it should work anyway :-)
   */
  private void dispose()
    {
    for ( AedbOperations oper : pgrpMap.values())
      oper.dispose();

    pgrpMap.clear();
    }

  }
