/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.MimPost;

/**
 * Some object in the system needs to have a couple of private and public key, for whatever reason
 * So, it refers to this table with a specific ID to refer to it
 * Since it is used to identify somebody, there should be just ONE dbepki for each object....
 * Or, if you want two of them... well, have to references to it
 */
public final class AedbPost extends AedbOperations
  {
  private static final String classname="AedbPost";
  
  public static final MimEntity mimEntity = MimEntityList.aepost;
  
  public static final String cn_tbl  = "aepost_tbl";
  public static final String cn_id          = "aepost_id";

  public static final String cn_fingerprint = "aepost_fingerprint";
  public static final String cn_creation    = "aepost_creation";
  public static final String cn_pow         = "aepost_pow";
  public static final String cn_signature   = "aepost_signature";
  
  public static final String cn_board_id    = "aepost_board_id";   // this thread refers to this board
  public static final String cn_thread_id   = "aepost_thread_id";
  public static final String cn_parent_id   = "aepost_parent_id";  // and refers to this parent
  public static final String cn_body        = "aepost_body";
  public static final String cn_owner_id    = "aepost_owner_id";   // this thread is owned by this user
  public static final String cn_note        = "aepost_note";
 
  private static final String aepost_search_view = "aepost_search_view";
  
  private AedbUpki aedbPki;      // needed to manage the pki relations
  private AedbBoard aedbBoard;  // needed to manage the board relations
  private AedbThread aedbThread;  // 
  
  public AedbPost ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbPki = (AedbUpki)stat.aedbFactory.getAedb(AedbUpki.mimEntity);
    aedbBoard = (AedbBoard)stat.aedbFactory.getAedb(AedbBoard.mimEntity);
    aedbThread = (AedbThread)stat.aedbFactory.getAedb(AedbThread.mimEntity);
    }

  @Override
  public final String getEntityDesc ()
    {
    return "Aether Post";
    }


  @Override
  public final String getMainTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getListViewName ()
    {
    return aepost_search_view;
    } 
  
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj == null ) 
      return null;
    
    if (! ( obj instanceof MimPost ) )
      {
      println.println(classname+"dbaseSave: unsupported class "+obj.getClass());
      return null;
      }
    
    MimPost row = (MimPost)obj;
    
    Integer prim_id=getPrimaryId(dbase, row.fingerprint);
    
    if ( prim_id == null )
      return dbaseInsert(dbase, row);
    else 
      return dbaseUpdate(dbase, prim_id, row);
    }
  
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimPost row) throws SQLException
    {
    String query="UPDATE "+cn_tbl+" SET "+
    cn_parent_id+"=?"+
    " WHERE "+cn_id+"=?";
    
    Integer id = getPrimaryId(dbase, row.parent);
    
    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++,id);
    prepared.setInt(insIndex++,prim_id);
    
    prepared.executeUpdate();
    prepared.close();
    
    return prim_id;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, MimPost row) throws SQLException
    {
    Integer owner_id = aedbPki.getPrimaryId(dbase, row.owner_publickey);
    
    if ( owner_id == null )
      owner_id = aedbPki.insert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      {
      stat.mcwishConsole.add(MimEntityList.aeboard, row.board);
      throw new IllegalArgumentException("Cannot insert Post if I do not have the Board ");
      }
    
    Integer thread_id = aedbThread.getPrimaryId(dbase, row.thread);
    
    if ( thread_id == null )
      {
      stat.mcwishConsole.add(MimEntityList.aethread, row.thread);
      throw new IllegalArgumentException("Cannot insert Post if I do not have the thread_id ");
      }
    
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_board_id+","+
       cn_thread_id+","+
       cn_parent_id+","+
       cn_body+","+
       cn_owner_id+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setInt(insIndex++,board_id);
    prepared.setInt(insIndex++,thread_id);
    prepared.setInt(insIndex++,null);
    prepared.setString(insIndex++,row.body);
    prepared.setInt(insIndex++,owner_id);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  public Brs select ( AeDbase dbase, int thread_id ) throws SQLException
    {
    String query = "SELECT * FROM "+aepost_search_view+" WHERE "+cn_thread_id+"=?";

    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++,thread_id);
    
    return prepared.executeQuery();
    }
    
  
  
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);
    
    addSchemaRow(new SchemaRowInteger(cn_board_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_thread_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_parent_id));
    addSchemaRow(new SchemaRowVarchar(cn_body,65535));
    addSchemaRow(new SchemaRowInteger(cn_owner_id)).setNotNullable();
    addSchemaRow(new SchemaRowVarchar(cn_note,500));
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+aepost_search_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    String query="CREATE VIEW "+aepost_search_view+" AS "+ 
        " SELECT aepost_tbl.* FROM aepost_tbl "+
        " LEFT OUTER JOIN aethread_tbl ON aepost_thread_id=aethread_id "+
        " LEFT OUTER JOIN aepost_tbl ON aepost_parent_id=aepost_id "+
        " LEFT OUTER JOIN mimuser_tbl ON aepost_owner_id=mimuser_id ";
    
    db.executeDbaseDone(query);
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX post_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX post_fingerprint_index");
    }  
  
  
  }
      
      
  } 

