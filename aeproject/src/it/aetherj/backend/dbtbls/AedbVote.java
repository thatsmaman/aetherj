/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.*;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.MimVote;

/**
 */
public final class AedbVote extends AedbOperations
  {
  private static final String classname="AedbVote";
  
  public static final MimEntity mimEntity = MimEntityList.aevote;
  
  public static final String cn_tbl         = "aevote_tbl";
  public static final String cn_id          = "aevote_id";

  public static final String cn_fingerprint = "aevote_fingerprint";
  public static final String cn_creation    = "aevote_creation";
  public static final String cn_pow         = "aevote_pow";
  public static final String cn_signature   = "aevote_signature";
  
  public static final String cn_board_id    = "aevote_board_id";   // this thread refers to this board
  public static final String cn_thread_id   = "aevote_thread_id";  // the thread of the above board
  public static final String cn_post_id     = "aevote_post_id";    // and refers to this parent
  public static final String cn_owner_id    = "aevote_owner_id";   // this thread is owned by this user
  public static final String cn_last_update = "aevote_last_update";
  public static final String cn_note        = "aevote_note";
 
  private static final String aevote_search_view = "aevote_search_view";
  
  private AedbUpki   aedbPki;     // needed to manage the pki relations
  private AedbBoard  aedbBoard;   // needed to manage the board relations
  private AedbThread aedbThread;  // 
  private AedbPost   aedbPost;    //
  
  public AedbVote ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    aedbPki = stat.aedbFactory.aedbUpki;
    aedbBoard = stat.aedbFactory.aedbBoard;
    aedbThread = stat.aedbFactory.aedbThread;
    aedbPost = stat.aedbFactory.aedbPost;
    }

  @Override
  public final String getEntityDesc ()
    {
    return "Aether Post";
    }


  @Override
  public final String getMainTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }
  @Override
  public final String getFingerprintColName ()
    {
    return cn_fingerprint;
    }

  @Override
  public final String getListViewName ()
    {
    return aevote_search_view;
    } 
  
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj == null ) 
      return null;
    
    if (! ( obj instanceof MimVote ) )
      {
      println.println(classname+"dbaseSave: unsupported class "+obj.getClass());
      return null;
      }
    
    MimVote row = (MimVote)obj;
    
    Integer prim_id=getPrimaryId(dbase, row.fingerprint);
    
    if ( prim_id == null )
      return dbaseInsert(dbase, row);
    else 
      return dbaseUpdate(dbase, prim_id, row);
    }
  
  public Integer dbaseUpdate ( AeDbase dbase, Integer prim_id, MimVote row) throws SQLException
    {
    /*
    String query="UPDATE "+cn_tbl+" SET "+
    cn_parent_id+"=?"+
    " WHERE "+cn_id+"=?";
    
    Integer id = getPrimaryId(dbase, row.parent);
    
    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++,id);
    prepared.setInt(insIndex++,prim_id);
    
    prepared.executeUpdate();
    prepared.close();
    */
    
    return prim_id;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, MimVote row) throws SQLException
    {
    Integer owner_id = aedbPki.getPrimaryId(dbase,row.owner);
    
    if ( owner_id == null )
      owner_id = aedbPki.insert(dbase, row.owner, row.owner_publickey);
    
    Integer board_id = aedbBoard.getPrimaryId(dbase, row.board); 
    
    if ( board_id == null )
      {
      stat.mcwishConsole.add(MimEntityList.aeboard, row.board);
      throw new IllegalArgumentException("Cannot insert Vote if I do not have the Board ");
      }
    
    Integer thread_id = aedbThread.getPrimaryId(dbase, row.thread);
    
    if ( thread_id == null )
      {
      stat.mcwishConsole.add(MimEntityList.aethread, row.thread);
      throw new IllegalArgumentException("Cannot insert Vote if I do not have the thread_id ");
      }
    
    Integer post_id = aedbPost.getPrimaryId(dbase, row.target);
    
    if ( post_id == null )
      {
      stat.mcwishConsole.add(MimEntityList.aepost, row.target);
      throw new IllegalArgumentException("Cannot insert Vote if I do not have the post_id ");
      }
    
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_fingerprint+","+
       cn_creation+","+
       cn_board_id+","+
       cn_thread_id+","+
       cn_post_id+","+
       cn_owner_id+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,? )";
    
    MimDprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setValue(insIndex++,row.fingerprint);
    prepared.setValue(insIndex++,row.creation);
    prepared.setInt(insIndex++,board_id);
    prepared.setInt(insIndex++,thread_id);
    prepared.setInt(insIndex++,post_id);
    prepared.setInt(insIndex++,owner_id);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
  
  public Brs select ( AeDbase dbase, int thread_id ) throws SQLException
    {
    String query = "SELECT * FROM "+aevote_search_view+" WHERE "+cn_thread_id+"=?";

    Dprepared prepared = dbase.getPreparedStatement(query);
    int insIndex=1;
    prepared.setInt(insIndex++,thread_id);
    
    return prepared.executeQuery();
    }
    
  
  
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addProvableHeader(cn_id, cn_fingerprint, cn_creation, cn_pow, cn_signature);

    addSchemaRow(new SchemaRowInteger(cn_board_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_thread_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_post_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_owner_id)).setNotNullable();
    addSchemaRow(new SchemaRowInteger(cn_last_update));
    addSchemaRow(new SchemaRowVarchar(cn_note,500));
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+aevote_search_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    String query="CREATE VIEW "+aevote_search_view+" AS "+ 
        " SELECT aevote_tbl.* FROM aevote_tbl "+
        " LEFT OUTER JOIN aethread_tbl ON aevote_thread_id=aethread_id "+
        " LEFT OUTER JOIN aevote_tbl ON aevote_parent_id=aevote_id "+
        " LEFT OUTER JOIN mimuser_tbl ON aevote_owner_id=mimuser_id ";
    
    db.executeDbaseDone(query);
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX vote_fingerprint_index ON "+cn_tbl+" ( "+cn_fingerprint+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX vote_fingerprint_index");
    }  

  }
      
      
  } 

