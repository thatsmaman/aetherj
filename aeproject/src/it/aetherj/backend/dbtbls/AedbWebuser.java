/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.security.MessageDigest;
import java.sql.SQLException;

import it.aetherj.backend.*;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.MimEntityList;

/**
 * This is a web user, it should have an associated Aether user, but who knows, so, I am having a separate table
 */
public final class AedbWebuser extends AedbOperations
  {
  private static final String classname="AedbWebuser";
  
  public static final String cn_tbl     = "webuser_tbl";
  public static final String cn_id      = "webuser_id";
  public static final String cn_login   = "webuser_login";
  public static final String cn_hashpass = "webuser_hashpass";   
  public static final String cn_note    = "webuser_note";
 
  private static final String webuser_search_view = "webuser_search_view";
  
  public AedbWebuser ( Stat stat, PrintlnProvider println )
    {
    super(stat, println, MimEntityList.aenode);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getEntityDesc ()
    {
    return "Webuser Handler";
    }

  @Override
  public final String getMainTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getFingerprintColName ()
    {
    return null;
    }
  
  @Override
  public final String getListViewName ()
    {
    return webuser_search_view;
    } 

  /**
   * Check that the user/pw are actually ok
   * @return the wana_id associated with this user/pw
   */
  public Integer isGoodUsernamePassword (AeDbase dbase, String username, String password )
    {
    String query = "SELECT * FROM "+cn_tbl+" WHERE "+cn_login+"=?";

    byte [] aeusr_hashpass = null;
    Integer aeusr_id = null;

    try
      {
      Dprepared prepared = dbase.getPreparedStatement(query);
      
      int insIndex = 1;
      prepared.setString(insIndex++,username);
  
      Brs ars = prepared.executeQuery();
      if ( ars.next() ) 
        {
        aeusr_id = ars.getInteger(cn_id);
        aeusr_hashpass = ars.getBytes(cn_hashpass);
        }
      ars.close();
      prepared.close();
      }
    catch ( Exception exc )
      {
      println.println(classname+"isGoodUsernamePassword",exc);
      return null;
      }

    // basically user not found
    if ( aeusr_id == null ) return null;
    
    // there is a special case when the user id is 1 and the password is blank
    if ( aeusr_id.intValue() == 1 && aeusr_hashpass == null ) return aeusr_id;

    if ( ! isGoodHash(password, aeusr_hashpass )) 
      {
      println.println(classname+"isGoodUsernamePassword: Bad password for user="+username);
      return null;
      }
      
    return aeusr_id;
    }
   
  /**
   * This MUST be the same algorithm used in the WEB
   * @param clearPass
   * @return
   */
  public byte[] getDigestedPass ( String clearPass )
    {
    try
      {
      byte [] bytePass = clearPass.getBytes(Const.utf16leCharset);
      MessageDigest hasher  = MessageDigest.getInstance("SHA1");
      return hasher.digest(bytePass);
      }
    catch (Exception exc)
      {
      println.println(classname+"getDigestedPass",exc);
      return new byte[0];
      }
    }

  private boolean isGoodHash ( String clearPass, byte [] wantByteHash )
    {
    if ( wantByteHash == null ) 
      {
      println.println(classname+"isGoodHash: NULL wantByteHash");
      return false;
      }
    
    byte [] haveByteHash = getDigestedPass(clearPass);
    
    if ( haveByteHash.length != wantByteHash.length )      
      {
      println.println(classname+"isGoodHash: haveByteHash.length="+haveByteHash.length+" != wantByteHash.length="+ wantByteHash.length );
      return false;
      }

    for (int index=0; index<wantByteHash.length; index++)
      if ( haveByteHash[index] != wantByteHash[index] ) return false;
  
    return true;
    }
    
  /**
   * There has to be a login and a valid password
   * @param dbase
   * @param login
   * @param password
   * @return
   * @throws SQLException
   */
  private Integer insertUser(AeDbase dbase, String login, String password ) throws SQLException
    {
    if ( password == null || password.length() < 8 )
      throw new IllegalArgumentException("password is null or shorter than 8 chars");
    
    String query = "INSERT INTO  "+cn_tbl+" ( webuser_login,webuser_hashpass ) VALUES ( ?,? ) ";
    
    byte []hash = getDigestedPass(password);
    
    Dprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setString(insIndex++,login);
    prepared.setBinary(insIndex++,hash);

    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();

    return primary_id;
    }
    
  private Integer updateUser(AeDbase dbase, Integer user_id, String login, String password ) throws SQLException
    {
    byte []hash=null;
    
    if ( password != null && password.length() >= 8 )
      hash = getDigestedPass(password);

    StringBuilder query = new StringBuilder(500);
    query.append("UPDATE "+getMainTableName()+" SET ");
    query.append(cn_login+"=? ");
    query.append(hash == null ? "" : ", "+cn_hashpass+"=? ");
    query.append("WHERE "+cn_id+"=?");
    
    Dprepared prepared = dbase.getPreparedStatement(query.toString());
    int insIndex=1;
    prepared.setString(insIndex++,login);
    if ( hash != null ) prepared.setBinary(insIndex++,hash);
    prepared.setInt(insIndex++, user_id);

    int a = prepared.executeUpdate();
    
    prepared.close();

    return a > 0 ? user_id : null;
    }

  /**
   * Return the ID of the row inserted or the ID of the row that is already present
   * @throws SQLException 
   */
  public Integer recordSet ( AeDbase dbase, Integer userId, String login, String password ) throws SQLException
    {
    if ( userId == null )
      return insertUser(dbase, login, password);

    Brs brs = dbase.selectTableUsing(getMainTableName(), getPrimaryColName(), userId);
    boolean have_row = brs.next();
    brs.close();
    
    if ( have_row )
      return updateUser(dbase, userId, login, password);
    else
      return insertUser(dbase, login, password);
    }
    
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowVarchar(cn_login,30));  
    addSchemaRow(new SchemaRowVarbinary(cn_hashpass,100));
    addSchemaRow(new SchemaRowVarchar(cn_note,500));
    }

  @Override
  public boolean createTable (ExecuteProvider db ) 
    {
    super.createTable(db);
      
    return db.executeDbaseDone("INSERT INTO "+getMainTableName()+" (webuser_login) VALUES ('admin')");
    }

  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    // login names must be unique
    String query="CREATE UNIQUE INDEX webuser_login_index ON "+cn_tbl+" ( "+cn_login+" ) ";
    dbexec.executeDbaseDone(query);
    }
  
  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX webuser_login_index");
    }  


  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+webuser_search_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+webuser_search_view+" AS "+ 
    "SELECT webuser_tbl.* FROM webuser_tbl");
    }
  }
      
      
  } 

