/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.dbtbls;

import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbase.dbschema.*;
import it.aetherj.backend.mimclient.McwAddress;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.MimAddress;

public final class AedbAddress extends AedbOperations
  {
  private static final String classname="AedbAddress.";
  
  public static final MimEntity mimEntity = MimEntityList.aeaddres;

  public static final String cn_tbl          = "adrs_tbl";
  public static final String cn_id           = "adrs_id";
  public static final String cn_location     = "adrs_location";
  public static final String cn_sublocation  = "adrs_sublocation";
  public static final String cn_port         = "adrs_port";
  public static final String cn_iptype       = "adrs_iptype";
  public static final String cn_type         = "adrs_type";
  public static final String cn_LastSuccessfulPing = "adrs_LastSuccessfulPing";
  public static final String cn_LastSuccessfulSync = "adrs_LastSuccessfulSync";
  public static final String cn_ProtoVMajor  = "adrs_ProtoVMajor";
  public static final String cn_ProtoVMinor  = "adrs_ProtoVMinor";
  public static final String cn_ClientVMajor = "adrs_ClientVMajor";
  public static final String cn_ClientVMinor = "adrs_ClientVMinor";
  public static final String cn_ClientName   = "adrs_ClientName";
  public static final String cn_note = "adrs_note";
 
  private static final String adrs_search_view = "adrs_search_view";
  
  public AedbAddress ( Stat stat, PrintlnProvider println )
    {
    super(stat,println,mimEntity);
    
    stat.dbSchemaManager.addProvider(new ProviderInstance());
    }

  @Override
  public void initialize()
    {
    }

  @Override
  public final String getEntityDesc ()
    {
    return "Address Handler";
    }


  @Override
  public final String getMainTableName ()
    {
    return cn_tbl;
    }
    
  @Override
  public final String getPrimaryColName ()
    {
    return cn_id;
    }

  @Override
  public final String getFingerprintColName ()
    {
    return null;
    }
  
  @Override
  public final String getListViewName ()
    {
    return adrs_search_view;
    } 
  
  
  public Integer getPrimaryId (AeDbase dbase, McwAddress row ) throws SQLException
    {
    return getPrimaryId(dbase, row.getLocation(), row.getSublocation(), row.getPort());
    }

  /**
   * Someone may wish to find the primary_id for further use
   * @return the primary_id if there is one
   */
  public Integer getPrimaryId (AeDbase dbase, String location, String sublocation, int port ) throws SQLException
    {
    String query = "SELECT "+cn_id+" FROM "+cn_tbl+
        " WHERE "+cn_location+"=? AND "+cn_port+"=? AND "+cn_sublocation+"=? ";
     
     Dprepared prepared=dbase.getPreparedStatement(query.toString());
     
     int insIndex=1;
     prepared.setString(insIndex++, location);
     prepared.setInt(insIndex++, port);
     prepared.setString(insIndex++, sublocation);
     
     Brs ars = prepared.executeQuery();
     
     Integer risul = null;
     
     if ( ars.next() ) 
       risul=ars.getInteger(cn_id);
     
     ars.close();
     
     return risul;
    }
  
  @Override
  public Integer dbaseSave ( AeDbase dbase, Object obj) throws SQLException
    {
    if ( obj == null ) 
      return null;
    
    if ( obj instanceof MimAddress )
      {
      MimAddress row = (MimAddress)obj;
      Integer have_id=getPrimaryId(dbase, row.location, row.sublocation, row.port);
      McwAddress mcwAddress = new McwAddress(have_id, row);
      Integer primary_id=dbaseSave(dbase, mcwAddress);
      
      if ( have_id==null)
        stat.mcwAddressModel.addAddress(mcwAddress);
      
      return primary_id;
      }
    else
      {
      println.println(classname+"dbaseSave: unsupported class "+obj.getClass());
      return null;
      }
    }
  
  public Integer dbaseSave ( AeDbase dbase, McwAddress row) throws SQLException
    {
    if ( row == null ) 
      return null;

    if ( row.primary_id == null )
      return dbaseInsert(dbase, row);
    else 
      return dbaseUpdate(dbase, row);
    }
  
  private Integer dbaseUpdate ( AeDbase dbase, McwAddress row) throws SQLException
    {
    return row.primary_id;
    }
  
  public Integer dbaseInsert ( AeDbase dbase, McwAddress row) throws SQLException
    {
    String query = "INSERT INTO "+cn_tbl+" ( "+
       cn_location+","+
       cn_sublocation+","+
       cn_port+","+
       cn_iptype+","+
       cn_type+","+
       cn_ClientName+","+
       cn_note+
       " ) VALUES ( ?,?,?,?,?,?,? )";
    
    Dprepared prepared = dbase.getPreparedStatementReturnKeys(query);
    int insIndex=1;
    prepared.setString(insIndex++,row.getLocation());
    prepared.setString(insIndex++,row.getSublocation());
    prepared.setInt(insIndex++,row.getPort());
    prepared.setInt(insIndex++,row.ip_type);
    prepared.setInt(insIndex++,row.adrs_type);
    prepared.setString(insIndex++,row.client_name);
    prepared.setString(insIndex++,null);
    
    Brs ars = prepared.executeUpdateReturnKeys();

    Integer primary_id=null;
    
    if ( ars.next() ) 
      primary_id = ars.getInteger(1);
    
    ars.close();  
    prepared.close();
    
    row.primary_id=primary_id;

    return primary_id;
    }
  
  public Brs select ( AeDbase dbase )
    {
    String query = "SELECT * FROM "+adrs_search_view;
    
    return dbase.select(query);
    }
  
private final class ProviderInstance extends SchemaProvider
  {
  ProviderInstance()
    {
    addSchemaRow(new SchemaRowInteger(cn_id)).setPrimary();          
    addSchemaRow(new SchemaRowVarchar(cn_location,300)).setNotNullable();  
    addSchemaRow(new SchemaRowVarchar(cn_sublocation,300));  
    addSchemaRow(new SchemaRowInteger(cn_port)).setNotNullable();  
    addSchemaRow(new SchemaRowInteger(cn_iptype));  
    addSchemaRow(new SchemaRowInteger(cn_type));
    
    addSchemaRow(new SchemaRowTimestamp(cn_LastSuccessfulPing));  
    addSchemaRow(new SchemaRowTimestamp(cn_LastSuccessfulSync));
    addSchemaRow(new SchemaRowVarchar(cn_ClientName,255));
    addSchemaRow(new SchemaRowVarchar(cn_note,300));  
    }


  @Override
  public String getTableName ()
    {
    return cn_tbl;
    }

  @Override
  public void dropView ( ExecuteProvider db )
    {
    db.executeDbaseDone("DROP VIEW "+adrs_search_view);
    }

  @Override
  public void createView ( ExecuteProvider db )
    {
    db.executeDbaseDone("CREATE VIEW "+adrs_search_view+" AS "+ 
      "SELECT adrs_tbl.* "+
      " FROM adrs_tbl ");
    }
  
  
  @Override
  public void createIndex ( ExecuteProvider dbexec ) 
    {
    String query="CREATE UNIQUE INDEX adrs_loc_port_index ON "+cn_tbl+" ( "+
      cn_location+","+cn_port+","+cn_sublocation+" ) ";
    
    dbexec.executeDbaseDone(query);
    }

  @Override
  public void dropIndex   ( ExecuteProvider dbexec ) 
    {
    dbexec.executeDbaseDone("DROP INDEX adrs_loc_port_index");
    }  

  }
      
      
  } 

