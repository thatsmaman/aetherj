package it.aetherj.backend.mimclient;

import java.util.ArrayList;

import it.aetherj.protocol.*;
import it.aetherj.shared.v0.MimFilterFingerprint;

public class McWishFingerprint
  {
  private final MimEntity entity;
  
  private final ArrayList<MimFingerprint>fingerList = new ArrayList<MimFingerprint>(20);
  
  public McWishFingerprint ( MimEntity entity )
    {
    this(entity,null);
    }
  
  public McWishFingerprint ( MimEntity entity, MimFingerprint fingerprint )
    {
    if ( entity == null )
      throw new IllegalArgumentException("entity is null");
    
    this.entity = entity;
    
    add(fingerprint);
    }
  
  public MimFilterFingerprint getMimFilter()
    {
    MimFilterFingerprint risul = new MimFilterFingerprint(fingerList);
    
    return risul;
    }
  
  public MimEntity getEntity()
    {
    return entity;
    }
  
  public boolean hasEntity ( MimEntity w_entity )
    {
    if ( w_entity == null )
      return false;
    
    return entity.equals(w_entity);
    }

  public boolean isEmpty ()
    {
    return fingerList.isEmpty();
    }
  
  /**
   * I have to make sure that the new fprint is not in the list
   * @param fingerprint
   */
  public void add ( MimFingerprint fingerprint )
    {
    if ( fingerprint == null )
      return;
    
    if ( fingerList.contains(fingerprint))
      return;
    
    fingerList.add(fingerprint);
    }  
  
  }
