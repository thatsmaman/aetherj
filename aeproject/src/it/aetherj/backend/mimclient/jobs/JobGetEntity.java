/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient.jobs;

import java.net.*;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.*;
import it.aetherj.shared.v0.MimPayload;

/**
 * This should get aenode info and store them in the proper place
 */
public class JobGetEntity extends McwRunner
  {
  private static final String classname="JobGetEntity";
  
  public JobGetEntity(Stat stat, McwAddress addr, McwParams params ) 
    {
    super(stat, addr, params);
    }
  
  @Override
  public int runJob ()
    {
    try
      {
      // This is trapped since  subclass needs the result value in a reliable way
      println("runJob GET "+getUrl());
      
      McwURLConnection conn = new McwURLConnection(stat,getUrl());
            
      int res_code = conn.getResponseCode();
      
      println(McwJobStats.getResMessage(res_code));
      
      if ( res_code != McwJobStats.res_OK || mcwParams.hasNoResponse )
        return res_code;
      
      MimPayload payload = conn.getResponse(getPrintln(), MimPayload.class);

      int imported=mimParse(payload);

      printlnConsole(classname+" imported="+imported);
      
      return res_code;
      }
    catch ( SocketTimeoutException exc )
      {
      return McwJobStats.res_Timeout;
      }
    catch ( ConnectException exc )
      {
      return McwJobStats.res_ConnRefused;
      }
    catch ( NoRouteToHostException exc )
      {
      return McwJobStats.res_NoRoute;
      }
    catch ( MismatchedInputException exc )
      {
      setJobException(exc);
      println("AAAA",exc);
      return McwJobStats.res_Mismatch;
      }
    catch ( Exception exc )
      {
      println(classname+".runJob()",exc);
      setJobException(exc);
      return McwJobStats.res_Exception;
      }
    }


  }
