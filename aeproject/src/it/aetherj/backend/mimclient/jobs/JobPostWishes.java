package it.aetherj.backend.mimclient.jobs;

import it.aetherj.backend.Stat;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.*;
import it.aetherj.shared.v0.*;

/**
 * Post the list of wishes for a specific content
 * If there is nothing to do, just say it
 */
public class JobPostWishes extends JobPostEntity
  {
  private static final String classname="JobPostWishes";
  
  public JobPostWishes ( Stat stat, McwAddress address, McwParams params )
    {
    super(stat,address, params);
    }
  
  @Override
  public int runJob ()
    {
    try
      {
      println("runJob GET "+getUrl());
      
      McWishFingerprint  wish = stat.mcwishConsole.pullFingerprints(mcwParams.entity);
      
      if ( wish == null || wish.isEmpty() )
        {
        println("  wish list for "+mcwParams.entity+" is empty");
        return McwJobStats.res_NoToDo;
        }

      MimFilterArray filters = new MimFilterArray();
      filters.add(wish.getMimFilter());

      MimPayload payload = newMimPayload(filters);
            
      McwURLConnection conn = new McwURLConnection(stat,getUrl(),McwURLConnection.method_POST);

      int res_code=conn.writeObject(getPrintln(), payload);

      println(McwJobStats.getResMessage(res_code));
      
      if ( res_code != McwJobStats.res_OK)
        return res_code;
      
      MimPayload rx_payload = conn.getResponse(getPrintln(), MimPayload.class);
      
      boolean correct = isPayloadSignatureCorrect(rx_payload);
      
      println("payload signature correct="+correct);
        
      int imported=mimParse(rx_payload);
      
      printlnConsole(classname+"."+mcwParams.entity+" imported="+imported);

      return res_code;
      }
    catch ( Exception exc )
      {
      println("runJob",exc);
      return McwJobStats.res_Exception;
      }
    }  

  
  
  }
