package it.aetherj.backend.mimclient.jobs;

import it.aetherj.backend.Stat;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.protocol.MimEntityList;

public class JobGetStatus extends JobGetEntity
  {

  public JobGetStatus(Stat stat, McwAddress addr )
    {
// APparently ping/status has worse result than /v1/status    
//    super(stat, addr, new McwParams(MimEntityList.pingstatus));
    
    super(stat, addr, new McwParams(MimEntityList.aestatus));
    
    // Careful, this is a specialized class....
    mcwParams.hasNoResponse=true;
    }

  @Override
  public int runJob ()
    {
    job_start_time=System.currentTimeMillis();

    int res_code=super.runJob();

    address.setPingStats(new McwJobStats(res_code, job_start_time));
    
    return res_code;
    }
  
  }
