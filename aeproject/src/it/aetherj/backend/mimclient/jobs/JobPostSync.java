package it.aetherj.backend.mimclient.jobs;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbtbls.AedbTstamp;
import it.aetherj.backend.mimclient.*;
import it.aetherj.backend.mimclient.ssl.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.v0.*;

/**
 * This should wrap up the logic in Sync state, so, first a post on node (to get the other node id)
 * then for all endpoints get the "index" (if I ever manage to get the way to do it)
 * and then a post to get the "missing content"
 */
public class JobPostSync extends McwRunner
  {
  private static final String classname="JobPostSync";
  
  private static final MimEntity []entityList = { 
      MimEntityList.aekey,  
      MimEntityList.aeboard, 
      MimEntityList.aethread, 
      MimEntityList.aepost, 
      MimEntityList.aevote };
  
  public JobPostSync(Stat stat, McwAddress addr, McwParams params)
    {
    super(stat, addr, params);
    }

  /**
   * The actual node for an address may have changed since last time, so, I cannot store it into the dbase...
   * @throws SQLException 
   */
  private Integer getNodePrimaryId () throws IOException, SQLException
    {
    // I need to work with the node entity now
    mcwParams.entity=MimEntityList.aenode;
    
    MimPayload payload = newMimPayload(null);
    
    resetJobStartTime();
    
    McwURLConnection conn = new McwURLConnection(stat,getUrl(),McwURLConnection.method_POST);

    int res_code=conn.writeObject(getPrintln(), payload);

    if ( res_code != McwJobStats.res_OK )
      {
      println(McwJobStats.getResMessage(res_code));
      address.setPOSTStats(new McwJobStats(res_code, job_start_time));
      conn.close();
      return null;
      }
    
    payload = conn.getResponse(getPrintln(), MimPayload.class);
    
    // TODO need to add payload verification here
        
    return stat.aedbFactory.aedbNode.dbaseSave(mimcDbase, payload);
    }
  

//return address.getUrl("/v0/c0/index/cache_2b3b27645da9ebf99832fcfc22c80cb00d25069c6ff8b0a17722156bc7e860f8/0.json");

  private int importIndex( MimResultsCache index ) throws IOException
    {
    URL pre = super.getUrl();
    String file = pre.getFile();
    String post="/"+index.response_url+"/0.json";
    
    URL connTo=new URL(pre.getProtocol(),pre.getHost(),pre.getPort(),file+post);
    
    McwURLConnection conn = new McwURLConnection(stat,connTo);
    
    int res_code = conn.getResponseCode();
    
    println(McwJobStats.getResMessage(res_code));

    if ( res_code != McwJobStats.res_OK )
      {
      address.setGETStats(new McwJobStats(res_code, job_start_time));
      conn.close();
      return res_code;
      }
    
    MimPayload payload = conn.getResponse(getPrintln(), MimPayload.class);
        
    // TODO need to add payload verification here
    
    println("imported "+mimParse(payload));
    
    return res_code;
    }
  
  
  
  private int syncEntity(Integer node_id, MimEntity entity ) throws SQLException, IOException
    {
    // from now on, work with this entity
    mcwParams.entity = entity;
    
    AedbTstamp aedbTstamp = stat.aedbFactory.aedbTstamp;
    
    MimTimestamp from = aedbTstamp.getLatestSyncTime(mimcDbase, node_id, entity);
    
    MimFilterArray filters = new MimFilterArray();
    filters.add(new MimFilterTimestamp(from, new MimTimestamp()));
    
    MimPayload payload = newMimPayload(filters);
    
    resetJobStartTime();
    
    McwURLConnection conn = new McwURLConnection(stat,getUrl(),McwURLConnection.method_POST);

    int res_code=conn.writeObject(getPrintln(), payload);

    if ( res_code != McwJobStats.res_OK )
      {
      println(McwJobStats.getResMessage(res_code));
      address.setPOSTStats(new McwJobStats(res_code, job_start_time));
      conn.close();
      return 0;
      }
    
    payload = conn.getResponse(getPrintln(), MimPayload.class);

    // update on import time is in the mimParse
    return mimParse(payload);
    }

  @Override
  public int runJob ()
    {
    try
      {
      Integer node_id=getNodePrimaryId();
      
      if ( node_id == null )
        return McwJobStats.res_NOContent;

      if ( stat.haveShutdownReq() )
        return McwJobStats.res_Shutdown;
      
      int imported=0;
      
      for ( MimEntity entity : entityList )
        {
        imported += syncEntity ( node_id, entity);

        if ( stat.haveShutdownReq() )
          break;
        }
      
      println("Imported="+imported);
      
      return McwJobStats.res_OK;
      }
    catch ( Exception exc )
      {
      println("runJob",exc);
      return McwJobStats.res_Exception;
      }
    }  
  }
