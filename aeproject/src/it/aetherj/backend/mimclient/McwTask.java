/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.concurrent.PriorityBlockingQueue;

import javax.swing.*;

import it.aetherj.backend.Stat;
import it.aetherj.boot.*;
import it.aetherj.shared.Aeutils;

/**
 * This creates a queue of jobs to run
 * You can have more than one Task in the System
 */
public class McwTask implements ComponentProvider
  {
  private static final String classname = "McwTask";

  private final PriorityBlockingQueue<McwRunner>workersList = new PriorityBlockingQueue<>(100);
  
  private final Stat stat;
  private final LimitedTextArea logArea;
  private final JTextField tickerField;
  private final JPanel workPanel;
  private final String taskName;

  private int tickCounter;
  private Thread runnerThread;
  
  public McwTask(Stat stat, String taskName)
    {
    this.stat = stat;
    this.taskName=taskName;
    
    logArea = new LimitedTextArea(2000);
    
    tickerField=new JTextField(5);
    tickerField.setEditable(false);
    
    workPanel = newWorkPanel();
    }
  
  public String getTaskName()
    {
    return taskName;
    }
  
  private JPanel newWorkPanel()
    {
    JPanel risul = new JPanel(new BorderLayout());
    
    risul.add(newNorthPanel(),BorderLayout.NORTH);
    risul.add(logArea.getComponentToDisplay(),BorderLayout.CENTER);
    
    return risul;
    }

  @Override
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }

  public void startThread ()
    {
    if ( runnerThread != null )
      {
      println("Thread already active");
      return;
      }
    
    runnerThread =Aeutils.newThreadStart(new SchedulerRunnable(), "Scheduler Runnable");
    }
  
  private void println(String message)
    {
    logArea.println(message);
    }
  
  private void println(String message, Throwable exc)
    {
    logArea.println(message, exc);
    }
  
  public void queue ( McwRunner job ) 
    {
    if ( job == null )
      return;

    try
      {
      workersList.put(job);
      }
    catch ( Exception exc )
      {
      println("queue",exc);
      }
    }
    
  protected void incTick ()
    {
    tickerField.setText(Integer.toString(tickCounter++));
    }
  
  private JPanel newNorthPanel()
    {
    JPanel risul = new JPanel();
    
    risul.add(tickerField);
    
    return risul;
    }
  
  private void runOneJob ()
    {
    try
      {
      // this will block here until there is something to take
      McwRunner worker = workersList.take();
      
      Date now = new Date();
      Date runAt = worker.getRunAt();
      
      if ( runAt.after(now))
        {
        // it may happen that the earliest to run is actually scheduled in the future
        // so, add it back to the queue and sleep some time
        workersList.add(worker);
        incTick();
        Aeutils.sleepSec(2);
        return;
        }
      
      // call the run method of the worker
      worker.run(logArea);
      
      incTick();
      }
    catch ( Exception exc )
      {
      println("runOneJob",exc);
      }
    }

private class SchedulerRunnable implements Runnable
  {
  public void run()
    {
    while ( ! stat.haveShutdownReq() )
      runOneJob();
    }
  }

  
  
  }
