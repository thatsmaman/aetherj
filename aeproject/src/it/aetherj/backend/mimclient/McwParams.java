/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import it.aetherj.protocol.MimEntity;

public class McwParams
  {
  public MimEntity entity;
  public int onExitWait_s;       // seconds to wait on exit
  public boolean hasNoResponse;  // if true the job has no response

  /**
   * It is legal to pass null entity if the class using the params impose its own entity
   * @param entity
   */
  public McwParams( MimEntity entity)
    {
    this.entity=entity;
    }
  
  public String getMethod()
    {
    if ( entity != null )
      return entity.getMethod();
    else
      return "null_entity";
    }
  }
