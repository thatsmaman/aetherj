package it.aetherj.backend.mimclient;

import java.util.ArrayList;

import it.aetherj.backend.Stat;
import it.aetherj.protocol.*;

/**
 * I need a way to queue some content that is desired and request it later
 * Mim Client Wish
 * Hold all things that we wish from a client, can be any client, since in theory mim is decentralized...
 * Maybe not...
 * The idea is that access to adding and removing is guarded
 */
public class McWishFactory
  {
  private final Stat stat;
  
  private final ArrayList<McWishFingerprint> wishList = new ArrayList<>(500);
  
  public McWishFactory(Stat stat)
    {
    this.stat = stat;
    }

  public synchronized McWishFingerprint pullFingerprints ( MimEntity entity )
    {
    int found_i = -1;
    
    int listlen=wishList.size();
    
    for (int index=0; index<listlen; index++)
      {
      McWishFingerprint wish = wishList.get(index);
      if ( wish.hasEntity(entity))
        {
        found_i=index;
        break;
        }
      }
    
    if ( found_i < 0 )
      return null;
    
    return wishList.remove(found_i);
    }
  
  public synchronized void add ( MimEntity entity, MimFingerprint fingerprint )
    {
    McWishFingerprint store = getAlways(entity);
    
    store.add(fingerprint);
    }

  private McWishFingerprint getAlways ( MimEntity entity )
    {
    for ( McWishFingerprint have : wishList )
      {
      if ( entity.equals(have.getEntity()) )
        return have;
      }
    
    McWishFingerprint risul = new McWishFingerprint(entity);
    wishList.add(risul);
    
    return risul;
    }
  
  }
