/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.table.*;

import it.aetherj.backend.Stat;
import it.aetherj.backend.mimclient.jobs.*;
import it.aetherj.boot.ComponentProvider;
import it.aetherj.protocol.*;

public class McwAddressTable implements ComponentProvider
  {
  private static final String classname="McwAddressTable";
  
  private final Stat stat;
  private final McwConsole parent;
  
  private final JScrollPane workPanel;
  private final McwAddressTableModel model;  

  private JTable jtable;
  
  public McwAddressTable(Stat stat, McwConsole parent )
    {
    this.stat = stat;
    this.parent = parent;
    this.model = stat.mcwAddressModel;
    
    workPanel = newWorkPanel();
    }
    
  private JScrollPane newWorkPanel()
    {
    jtable = new JTable(model);
    
    TableColumn column = jtable.getColumnModel().getColumn(McwAddressTableModel.col_id);
    column.setMaxWidth(100);
    column.setPreferredWidth(50);
  
    column = jtable.getColumnModel().getColumn(McwAddressTableModel.col_end_time);
    column.setCellRenderer(new AddressDateRendered());
    
    jtable.setAutoCreateRowSorter(true);
    
    jtable.addMouseListener(new McwMouseAdapter());
    
    JScrollPane risul = new JScrollPane(jtable);

    return risul;
    }
    
  public JComponent getComponentToDisplay()
    {
    return workPanel;
    }

  public McwAddress getSelected ()
    {
    int row=jtable.getSelectedRow();
    
    if ( row < 1 )
      return null;
    
    row = jtable.convertRowIndexToModel(row);
    
    return model.getAddress(row);
    }
  
  /*
  private boolean setSelectedPrimaryId ( Integer w_id )
    {
    if ( w_id == null )
      return false;
    
    int model_row = model.getRowIndexForPrimaryId(w_id);
    if ( model_row < 0 )
      return false;
    
    int table_row = jtable.convertRowIndexToView(model_row);
    
    jtable.setRowSelectionInterval(table_row, table_row);
    return true;
    }
    */
  public String toString()
    {
    return model.toString();
    }

  private MimEntity getSelectedEntity ( )
    {
      switch ( parent.getRunEntity() )
      {
      case McwConsole.re_Status: return MimEntityList.aestatus;

      case McwConsole.re_Node: return MimEntityList.aenode;

      case McwConsole.re_Address: return MimEntityList.aeaddres;
      
      case McwConsole.re_Aekeys: return MimEntityList.aekey;
      
      case McwConsole.re_Board: return MimEntityList.aeboard;
      
      case McwConsole.re_Thread: return MimEntityList.aethread;

      case McwConsole.re_Post: return MimEntityList.aepost;

      case McwConsole.re_Vote:   return MimEntityList.aevote;
        
      default: return null;
      }
    }
    
  private void runOnSelected (McwAddress address)
    {
    if ( address == null )
      return;
    
    McwParams params = new McwParams(getSelectedEntity());
    params.onExitWait_s=5;
    
    String w_run = parent.getRunState(); 

    McwRunner job=null;
    
    switch ( w_run )
      {
      case McwConsole.rs_GET: 
        job = new JobGetEntity(stat, address, params);
        break;

      case McwConsole.rs_POST: 
        job = new JobPostEntity(stat, address, params);
        break;
    
      case McwConsole.rs_RX_Cache: 
        job = new JobGetCache(stat, address, params);
        break;

      case McwConsole.rs_POST_Sync: 
        job = new JobPostSync(stat, address, params);
        break;

      case McwConsole.rs_RX_Wish: 
        job = new JobPostWishes(stat, address, params);
        break;

      default:
        parent.println("runOnSelected: unsupported RUN state="+w_run);
        break;
      }
    
    parent.queue(job);  
    }

  
private class McwMouseAdapter extends MouseAdapter
  {
  @Override
  public void mouseClicked(java.awt.event.MouseEvent evt) 
    {
    if ( ! parent.isRunModeOnClick() )
      return;
    
    int row = jtable.rowAtPoint(evt.getPoint());
    
    if ( row < 0 )
      return;
    
    row = jtable.convertRowIndexToModel(row);
    
    McwAddress addr = model.getAddress(row);
    
    runOnSelected(addr);
    }
  }

private static final class AddressDateRendered extends DefaultTableCellRenderer 
  {
  private static final long serialVersionUID = 1L;

  private final SimpleDateFormat f = new SimpleDateFormat("hh:mm:ss");

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) 
    {
    if( value instanceof Date) 
      value = f.format(value);
    
    return super.getTableCellRendererComponent(table, value, isSelected,hasFocus, row, column);
    }
  }




 }
