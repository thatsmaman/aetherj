package it.aetherj.backend.mimclient;

import java.net.URL;
import java.util.Date;

import it.aetherj.backend.dbtbls.AedbAddress;
import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.shared.v0.*;

/**
 * NOTE: You cannot guarantee that a given Node is bound to an address
 * Surely you cannot store it into a dbase
 * The reason being that an address might end up to a different node
 */
public final class McwAddress
  {
  public  Integer primary_id;
  
  private String  location;
  private String  sublocation;
  private int     port;
  public  int     ip_type;
  public  int     adrs_type;
  public  String  client_name=MimClient.default_name; 
  
  private Date run_next_time;  // when am I supposed to work again with this aeaddres
  
  private McwJobStats ping_stats;
  private McwJobStats get_stats;
  private McwJobStats post_stats;
  
  // this will hold the most recent statistics, do NOT let it be null
  private McwJobStats latest_stats=new McwJobStats();  
    
  private String msg;
  
  /**
   * Something like host:port/subpart do NOT include the first https:// or http://
   * @param locationURL
   */
  public McwAddress (String locationURL )
    {
    try
      {
      if ( ! locationURL.startsWith("https://"))
        locationURL="https://"+locationURL;

      URL url = new URL(locationURL);
      location = url.getHost();
      port=url.getPort();
      sublocation=url.getFile();
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      }
    
    verifyAddress();
    }
    
  public McwAddress ( Integer primary_id, MimAddress mima )
    {
    this.primary_id = primary_id;
    this.location = mima.location;
    this.sublocation=mima.sublocation;
    this.port = mima.port;
    this.ip_type = mima.location_type;
    this.adrs_type = mima.type;
    this.client_name=mima.client.name;

    verifyAddress();
    }
  
  public McwAddress ( Brs rs )
    {
    primary_id=rs.getInteger(AedbAddress.cn_id);
    location=rs.getString(AedbAddress.cn_location);        // normally an IP aeaddres, only this
    sublocation=rs.getString(AedbAddress.cn_sublocation);
    port=rs.getInteger(AedbAddress.cn_port);
    ip_type=rs.getInteger(AedbAddress.cn_iptype, MimAddress.location_type_IIPv4);
    adrs_type=rs.getInteger(AedbAddress.cn_type, MimAddress.type_DYNAMIC);
    client_name=rs.getString(AedbAddress.cn_ClientName);
    
    verifyAddress();
    }
  
  private void verifyAddress ()
    {
    if ( location == null || location.length() < 3 )
      location="null_or_empty";
    
    if ( sublocation == null  )
      sublocation="";
    
    if ( port == 0 )
      port=80;
    }
  
  public String getLocation ()
    {
    return location;
    }
    
  public String getSublocation()
    {
    return sublocation;
    }
  
  public int getPort()
    {
    return port;
    }
    
  /**
   * Returns latest run stats
   * @return a not null object, may be null in content
   */
  public McwJobStats getRunStats()
    {
    return latest_stats;
    }
  
  /**
   * @param w_id MUST be not null, otherwise NPE
   * @return
   */
  public boolean hasId ( Integer w_id )
    {
    if ( primary_id == null )
      return false;

    return w_id.equals(primary_id);
    }
    
  public void setPingStats (McwJobStats stats)
    {
    if ( stats == null ) return;
      
    ping_stats=stats;
    latest_stats=stats;
    }
    
  public void setGETStats (McwJobStats stats)
    {
    if ( stats == null ) return;

    get_stats=stats;
    latest_stats=stats;
    }

  public void setPOSTStats  (McwJobStats stats)
    {
    if ( stats == null ) return;

    post_stats=stats;
    latest_stats=stats;
    }
        
  public String getMsg ()
    {
    return msg;
    }
  
  public URL getUrl ()
    {
    try
      {
      return new URL("https",location,port,sublocation);
      }
    catch ( Exception exc )
      {
      return null;
      }
    }

  public String toString()
    {
    StringBuilder risul = new StringBuilder(200);
    
    risul.append("[");
    risul.append(primary_id);
    risul.append("] ");
    risul.append(location);
    risul.append(":");
    risul.append(port);
    
    return risul.toString();
    }

  /*
  public boolean equals(Object other)
    {
    if ( other instanceof McwAddress )
      {
      McwAddress oa = (McwAddress)other;

      if ( primary_id == null || oa.primary_id == null )
        return false;

      if ( latest_stats.work_start_ms == 0 || oa.latest_stats.work_start_ms == 0 )
        return false;

      return primary_id.equals(oa.primary_id) && latest_stats.work_start_ms == oa.latest_stats.work_start_ms; 
      }
    else
      {
      return false;
      }
    }
  
  @Override
  public int compareTo(McwAddress other)
    {
    if ( latest_stats.work_start_ms == 0 )
      return -1;
    
    if ( other.latest_stats.work_end_ms == 0 )
      return Integer.MAX_VALUE;
    
    int delta_ms = (int)(latest_stats.work_start_ms - other.latest_stats.work_end_ms);
    if ( delta_ms != 0 )
      return delta_ms;
    
    if ( primary_id == null )
      return -1;
    
    if ( other.primary_id == null )
      return Integer.MAX_VALUE;
    
    return primary_id-other.primary_id;
    }
*/
      
  }
