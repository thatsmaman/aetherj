package it.aetherj.backend.mimclient;

import java.util.*;

import it.aetherj.backend.Stat;

/**
 * I need a way to manage a few worker thread in the system
 */
public class McwScheduler implements Iterable<McwTask>
  {
  private static final String classname = "McwScheduler";
  private static final int task_max=3;

  private final Stat stat;
  private final McwConsole parent;

  private final ArrayList<McwTask>taskList = new ArrayList<>(20);
  
  private int queueIndex;   // where I am inserting at
  
  public McwScheduler(Stat stat, McwConsole parent)
    {
    this.stat = stat;
    this.parent=parent;
    
    for (int index=0; index<task_max; index++)
      taskList.add(new McwTask(stat,"McwTask "+index));
    }
  
  @Override
  public Iterator<McwTask> iterator()
    {
    return taskList.iterator();    
    }
  
  /**
   * Created tasks must be started when it is the right time
   */
  public void start()
    {
    for (McwTask task : taskList)
      task.startThread();
    }

  /**
   * You can queue runners here, they will be split evenly in the various tasks
   * I may need a way to queue runners to the least empty queue, later...
   * @param job
   */
  public void queue ( McwRunner job )
    {
    if ( job == null )
      return;
    
    if ( queueIndex >= taskList.size() )
      queueIndex=0;
    
    McwTask task=taskList.get(queueIndex++);
    
    task.queue(job);
    }
  
  }