/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient.ssl;

import java.io.*;
import java.net.*;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.aetherj.backend.Stat;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.shared.Aedbg;

public class McwURLConnection
  {
  private static final String classname="McwURLConnection";
  
  public static final String method_POST="POST";
  public static final String method_GET="GET";
 
  private final Stat stat;
  private final HttpsURLConnection conn;
  
  public McwURLConnection(Stat stat, URL toURL ) throws IOException
    {
    this(stat,toURL,method_GET);
    }
  
  public McwURLConnection(Stat stat, URL toURL, String reqMethod) throws IOException
    {
    this.stat = stat;
    
    conn = (HttpsURLConnection)toURL.openConnection();
    
    conn.setSSLSocketFactory(stat.mimSSLFactory.sslSocketFactory);
    conn.setHostnameVerifier(stat.mimSSLFactory.hostnameVerifier);
    
    conn.setRequestProperty("Content-Type", "application/json");
    conn.setRequestProperty("Accept", "application/json");
    
    if ( reqMethod == null )
      setRequestMethod(method_GET);
    else
      setRequestMethod(reqMethod);
    
    setConnectTimeout(1000);
    }
  
  public void close()
    {
    conn.disconnect();
    }
  
  public void setDoOutput()
    {
    conn.setDoOutput(true);
    }
  
  public void setRequestMethod(String method) throws ProtocolException
    {
    conn.setRequestMethod(method);
    }
  
  public void setRequestProperty(String key, String value)
    {
    conn.setRequestProperty(key, value);
    }

  public void setConnectTimeout ( int timeout_ms )
    {
    conn.setConnectTimeout(timeout_ms);
    }
  
  public int getResponseCode() throws IOException
    {
    return conn.getResponseCode();
    }
  
  public InputStreamReader getInputStreamReader() throws IOException
    {
    return new InputStreamReader(conn.getInputStream());
    }
  
  public OutputStream getOutputStream() throws IOException
    {
    return conn.getOutputStream();
    }
  
  
  public int writeObject ( PrintlnProvider  println, Object obj) throws IOException
    {
    ObjectMapper mapper = new ObjectMapper();

    setDoOutput();  // first this
    
    OutputStream os = getOutputStream();
  
    if ( stat.dbg.isDbg(Aedbg.HTTP_tx))
      {
      String tosend=mapper.writeValueAsString(obj);
      
      println.println(tosend);
      
      byte[] input = tosend.getBytes("utf-8");
      os.write(input, 0, input.length);
      }
    else
      {
      mapper.writeValue(os, obj);
      }
  
    return getResponseCode();
    }
  
  
  private String getResponse () throws IOException
    {
    InputStreamReader isr = new InputStreamReader(conn.getInputStream());
    BufferedReader br =  new BufferedReader(isr);
    StringBuilder risul = new StringBuilder(65000);
    
    String input;
       
    while ((input = br.readLine()) != null)
       risul.append(input);
    
    br.close();
  
    return risul.toString();
    }

  /**
   * This is WAY slower but it shows what is happening
   * @param println
   * @param valueType
   * @return
   * @throws IOException
   */
  private <T> T getResponseLogged (PrintlnProvider println, Class<T> valueType) throws IOException
    {
    InputStreamReader isr = new InputStreamReader(conn.getInputStream());
    BufferedReader br =  new BufferedReader(isr);
    StringBuilder risul = new StringBuilder(65000);
    
    String input;
       
    while ((input = br.readLine()) != null)
      {
      risul.append(input);
      println.println(input);
      }
    
    br.close();
  
    String received=risul.toString();
    
    if ( received.length() > 0 )
      {
      println.println(received);
      ObjectMapper mapper = new ObjectMapper();
      return mapper.readValue(risul.toString(), valueType);
      }
    else
      {
      println.println("rx is empty");
      return null;
      }
    }
  
  /**
   * Map directly into an object, way faster
   * NOTE that JsonParser.Feature.AUTO_CLOSE_SOURCE is normally TRUE
   * @param println
   * @param valueType
   * @return
   * @throws IOException
   */
  public <T> T getResponse (PrintlnProvider println, Class<T> valueType) throws IOException
    {
    if ( stat.dbg.isDbg(Aedbg.HTTP_rx))
      return getResponseLogged(println,valueType);
    
    ObjectMapper mapper = new ObjectMapper();
    InputStreamReader isr = new InputStreamReader(conn.getInputStream());
    BufferedReader br =  new BufferedReader(isr);
    return mapper.readValue(br, valueType);
    }
  
  
  }
