/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient.ssl;

import javax.net.ssl.*;

import it.aetherj.backend.Stat;

public class McwSSLFactory
  {
  private static final String classname="McwSSLFactory";
  
  private final Stat stat;
  
  // the following are needed when creating a HTTPS socket
  public final SSLSocketFactory sslSocketFactory;
  public final McwHostnameVerifier hostnameVerifier;
  
  private static final McwX509TrustManager[] ALL_TRUSTING = { new McwX509TrustManager() };
  
  public McwSSLFactory(Stat stat)
    {
    this.stat = stat;
    
    sslSocketFactory = newFactory();
    hostnameVerifier = new McwHostnameVerifier();
    }

  private SSLSocketFactory newFactory()
    {
    try
      {
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, ALL_TRUSTING, new java.security.SecureRandom());
      return sc.getSocketFactory();
      }
    catch ( Exception exc )
      {
      stat.log.println(classname+".newFactory",exc);
      return null;
      }
    }
  }
