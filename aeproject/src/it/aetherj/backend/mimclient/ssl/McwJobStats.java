package it.aetherj.backend.mimclient.ssl;

import java.util.Date;

public class McwJobStats
  {
  public static final int res_NULL=0;
  public static final int res_OK=200;
  public static final int res_NOContent=204;  // see respwriter.go
  public static final int res_BAD=400;
  public static final int res_NOTFound=404;
  public static final int res_NOTAllowed=405;
  public static final int res_TOOBusy=429;
  public static final int res_Exception=430;
  public static final int res_Timeout=431;
  public static final int res_ConnRefused=432;
  public static final int res_NoRoute=433;
  public static final int res_NoToDo=434;                // nothing to do
  public static final int res_Mismatch=435;              // Jakson telling me nothing to read
  public static final int res_Shutdown=436;              // System is being shutdown

  public final long work_start_ms;
  public final long work_end_ms;
  public final int res_code;
  public final int worktime_ms;
  

  
  
  /**
   * Construct a "null" object
   */
  public McwJobStats()
    {
    work_start_ms=0;
    work_end_ms=0;
    res_code=0;
    worktime_ms=0;
    }
  
  public McwJobStats(int res_code, long start_time_ms )
    {
    work_start_ms=start_time_ms;
    this.res_code=res_code;
    this.work_end_ms=System.currentTimeMillis();
    
    worktime_ms=(int)(work_end_ms-work_start_ms);
    }

  public boolean isAddressAlive ()
    {
    return work_start_ms > 0 && res_code == res_OK;
    }
  
  public Date getEndTime ()
    {
    if ( work_start_ms == 0 )
      return null;
    
    return new Date(work_end_ms);
    }

  public Integer getHttpResCode ()
    {
    if ( work_start_ms == 0 )
      return null;
    
    return Integer.valueOf(res_code);
    }

  public Integer getWorktime ()
    {
    if ( work_start_ms == 0 )
      return null;
    
    return Integer.valueOf(worktime_ms);
    }

  public static String getResMessage(int rescode)
    {
    String msg;
    
    switch ( rescode)
      {
      case res_OK:         msg="OK";          break;
      case res_BAD:        msg="BAD Request"; break;
      case res_NOContent:  msg="NO Content";  break;
      case res_NOTFound:   msg="NOT Found";   break;
      case res_NOTAllowed: msg="NOT Allowed"; break;
      case res_TOOBusy:    msg="TOO Busy";    break;
      case res_Exception:  msg="Exception";   break;
      default: msg="Unknown code "+rescode;
      }
    
    return "rescode "+rescode+" "+msg;
    }

  
  
  public String toString()
    {
    return res_code+" "+worktime_ms;
    }
  }
