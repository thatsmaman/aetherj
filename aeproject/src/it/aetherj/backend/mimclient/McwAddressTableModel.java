/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.net.URL;
import java.sql.SQLException;
import java.util.*;

import javax.swing.table.AbstractTableModel;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.*;
import it.aetherj.backend.mimclient.jobs.*;
import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.boot.PrintlnProvider;
import it.aetherj.boot.dbase.Brs;
import it.aetherj.protocol.*;
import it.aetherj.shared.Aeutils;

/**
 * Hold the list of all Nodes that can be contacted by the server
 * There is a periodic task to load - save the model to dbase
 * Objects are saved here ONLY, if you want to insert a new aenode, you add it here, the keeperThread will sync with dbase
 * One of the key issue is that it is NOT possible to iterate over the list AND at the same time change it
 * So, there must be ONLY ONE thread that adds/remove addresses. 
 * Since only this thread knows when the list is not being updated, it is tthis thread that starts the status ping !!
 * Cannot have a separate thread for status request...
 */
public class McwAddressTableModel extends AbstractTableModel
  {
  private static final String classname="McwAddressTableModel";
  private static final long serialVersionUID = 1L;
  
  private static final int addressStatusIndexNULL=-1;  // this is a way to say there is no valid index
  
  private final FireTableDataChanged fireTableDataChanged = new FireTableDataChanged();
  private final SortByWorktime sortByWorktime = new SortByWorktime();

  private final Stat stat;
  
  public  static final int col_id=0;
  private static final int col_url=1;
  private static final int col_msg=2;
  public  static final int col_end_time=3;
  private static final int col_res_code=4;
  private static final int col_ping_len=5;
  private static final int col_count=6;
  
  private final List<McwAddress> addressList;

  private final AeDbase dbase;   // the dbase to work with, connection will be done at start
  
  private PrintlnProvider logger;
  private Thread keeperThread;
  
  private AedbAddress aedbAddress;
  private AedbUpki aedbUpki;
  
  private int addressStatusIndex=addressStatusIndexNULL;  
  private Date nextSyncBestAddressesTime=new Date();  // when to sync the best addresses
  
  public McwAddressTableModel(Stat stat)
    {
    this.stat=stat;
    this.dbase=stat.newDbase();
    
    addressList = new ArrayList<McwAddress>();
    }
  
  private int getRowIndexForPrimaryId ( Integer w_id )
    {
    int len=getCount();
    
    for (int index=0; index<len; index++)
      {
      McwAddress row=getAddress(index);
      if ( row.hasId(w_id))
        return index;
      }

    return -1;
    }
    
  
  

  /**
   * The thread is in charge of keeping the persistent storage in sync with instance objects
   * @param logger
   */
  public void start(PrintlnProvider logger)
    {
    this.logger=logger;
    
    if ( keeperThread == null )
      {
      keeperThread=Aeutils.newThreadStart(new AddressesKeeper(), classname+".AddressesKeeper");
      println("Synchronizer Thread started");
      }
    else
      {
      println("Synchronizer thread ALREADY started");
      }
    }
    
  private void println(String message)
    {
    if ( logger != null )
      logger.println(message);
    else
      stat.println(message);
    }
  
  private void println(String message,Throwable exc)
    {
    if ( logger != null )
      logger.println(message,exc);
    else
      stat.println(message,exc);
    }

  @Override
  public String toString ()
    {
    return classname+" count="+addressList.size();
    }
  
  private void dbaseLoad () throws SQLException
    {
    // this binds the dbase manager objects
    aedbAddress = stat.aedbFactory.aedbAddress;  
    aedbUpki = stat.aedbFactory.aedbUpki;
    
    // make sure that boot address is saved into dbase
    McwAddress boot=new McwAddress(stat.aeParams.getBootLocation());
    // insert it into dbase only if it is not already there
    Integer primary_id=aedbAddress.getPrimaryId(dbase, boot);
    if ( primary_id==null) aedbAddress.dbaseInsert(dbase, boot);
    
    // now pick all of them up
    Brs ars = aedbAddress.select(dbase);
    
    while ( ars.next() )
      addressList.add( new McwAddress(ars));

    ars.close();

    // now, even if there is just one, the loading is complete
    Aeutils.SwingInvokeLater(fireTableDataChanged);
    }
  

  public synchronized int getCount ()
    {
    return addressList.size();
    }

  public synchronized McwAddress getAddress (int index)
    {
    try
      {
      return addressList.get(index);
      }
    catch ( Exception exc )
      {
      return null;
      }
    }
    
  public synchronized void addAddress(McwAddress mcwNode)
    {
    if ( mcwNode == null ) return;
    
    int rowindex = addressList.size();

    addressList.add(mcwNode);
    
    Aeutils.SwingInvokeLater(new FireRowInserted(rowindex));
    }
    
  private synchronized void removemcwNode(McwAddress mcwNode)
    {
    if ( mcwNode == null ) return;
    
    int rowindex = addressList.size();

    addressList.remove(mcwNode);
    
    fireTableRowsDeleted(rowindex,rowindex);
    }
    
  @Override
  public String getColumnName(int column) 
    {
    switch ( column )
      {
      case col_id:  
        return "Id";
        
      case col_url:  
        return "URL";
        
      case col_msg:  
        return "Message";
        
      case col_end_time:  
        return "Run Time";

      case col_res_code:  
        return "Res Code";

      case col_ping_len:  
         return "Ping ms";

      default:
        return "NULL col="+column;
      }
    }
    
  @Override
  public Class<?> getColumnClass(int col)
    {
    switch ( col )
      {
      case col_id:
      case col_res_code:
      case col_ping_len:
        return Integer.class;
        
      case col_url:  
        return URL.class;
      
      case col_msg:  
        return String.class;
      
      case col_end_time:  
        return Date.class;

      default:
        return Object.class;
      }
    }

  @Override
  public int getRowCount()
    {
    return addressList.size();
    }

  @Override
  public int getColumnCount()
    {
    return col_count;
    }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
    {
    McwAddress row = getAddress(rowIndex);

    if ( row == null ) return "null row="+rowIndex;
    
    McwJobStats stats = row.getRunStats();
    
    switch ( columnIndex )
      {
      case col_id: 
        return row.primary_id;

      case col_url: 
        return row.getUrl();

      case col_msg: 
        return row.getMsg();
        
      case col_end_time: 
        return stats.getEndTime();

      case col_res_code: 
        return stats.getHttpResCode();

      case col_ping_len: 
        return stats.getWorktime();

      default:
        return "NULL col="+columnIndex;
      }
    }

  
  private void dbaseSyncRun ()
    {
    // should go in all of them and commit, if changed data true
      
//      Aeutils.SwingInvokeLater(fireTableDataChanged);
    }

  /**
   * This makes a copy since the original may be "adjusted" by the user or in other ways
   * @param howmany
   * @return
   */
  private RoundRobinList getBestAddresses ( int howmany )
    {
    ArrayList<McwAddress> sorted = new ArrayList<McwAddress>();
    
    for ( McwAddress addr : addressList )
      {
      McwJobStats stats = addr.getRunStats();
      
      if ( ! stats.isAddressAlive() ) continue;
 
      sorted.add(addr);
      
      if ( sorted.size() >= howmany )
        break;
      }

    Collections.sort(sorted, sortByWorktime);
    
    return new RoundRobinList(sorted);
    }
  
    
  /**
   * Get all possible addresses using mim from the given host
   * NOTE that I am NOT giving up control of the thread, I do the work myself
   * @param address
   */
  private void getNewAddressesInitially (McwAddress address)
    {
    JobGetCache getAddresses = new JobGetCache(stat, address, MimEntityList.aeaddres);
    
    getAddresses.run(logger);
    
    // this will reset the fast scan at the beginning
    addressStatusIndex=addressStatusIndexNULL;
    }
  
  /**
   * Here I have to decide how and when to get new addresses
   * I should select all good addresses to work with and depending on how many I have I should decide
   * Note that I may decide to do something and then wait for next run
   * @throws SQLException 
   */
  private void getNewAddresses () throws SQLException
    {
    int wishCount = stat.aeParams.getBestAddressCount();
        
    // Pick up the best addresses I have
    RoundRobinList bestAddresses = getBestAddresses(wishCount);

    if ( bestAddresses.isEmpty() )
      {
      println("getNewAddresses: Cannot do anything, not any reachable address");
      return;
      }
    
    if ( getCount() < wishCount )
      {
      // If I have few addresses in the table I should try to get some more
      // once I have done this, I should let the algorithm run again
      // this path should happen just once in lifetime of application, the first run
      getNewAddressesInitially (bestAddresses.next());
      return;
      }
      
    // here I have some addresses to connect to, the next step in the lifecycle of application bootstrap is to get
    // the content for all entities. I cannot go in any random order since insert depends on foreign keys...
    if ( aedbUpki.getRowCount(dbase) > 10 ) return;
    
    for ( MimEntity entity : MimEntityList.entityList )
      {
      JobGetCache getentity = new JobGetCache(stat, bestAddresses.next(), entity);
      getentity.run(logger);
      }
    }

  private void syncBestAddresses()
    {
    Date now = new Date();
    
    if ( now.before(nextSyncBestAddressesTime)) return;
    
    println("syncBestAddresses(): RUN");
    
    // do a run every 120s, will be configurable
    nextSyncBestAddressesTime = new Date(now.getTime()+120*1000);
    
    // Pick up the best addresses I have
    RoundRobinList bestAddresses = getBestAddresses(stat.aeParams.getBestAddressCount());

    if ( bestAddresses.isEmpty() )
      {
      println("syncBestAddresses: Cannot do anything, not any reachable address");
      return;
      }
    
    while ( bestAddresses.hasNext() )
      {
      McwAddress address = bestAddresses.next();
      
      McwParams params = new McwParams(null);
      params.onExitWait_s=5;
      
      JobPostSync sync = new JobPostSync(stat, address, params);
      stat.mcwConsole.queue(sync);
      }
    
    }
    
  
  private void addressesKeeperRun_B () throws SQLException
    {
    if ( stat.mcwConsole.isRunModeAutomatic() )
      {
      addressStatusTester();
      
      getNewAddresses();
      
      syncBestAddresses ();
      }
    
    dbaseSyncRun();

    Aeutils.SwingInvokeLater(fireTableDataChanged);
    }
  
  /**
   * There are two things to synchronize, the first one is the dbase, saving data in dbase
   * The other thing is to regularly do a scan on addresses
   */
  private void addressKeeperRun_A()
    {
    try
      {
      dbase.connect(stat.config.mainDbaseProperty);
    
      if ( ! dbase.isConnected() )
        {
        println("addressesKeeper() dbase connect FAIL -> END");
        return;
        }
      
      println("addressesKeeper() dbase connected");
    
      dbaseLoad();
          
      while ( ! stat.waitHaveShutdownReq_s(5) )
        addressesKeeperRun_B();
      
      }
    catch ( Exception exc )
      {
      println("addressesKeeper()",exc);
      }
  
    // when sync run ends, it is time to close up
    dbase.close();
    
    // maybe nobody will see this
    println("addressesKeeper() END");
    }

  /**
   * This is in charge of stating jobs to test if an address is alive 
   * The first run should send quite a few, later, it should queue a different address every xxx seconds
   * MUST be on the same thread that changes the table, so, it knows that things are "stable"
   */
  private void addressStatusTester()
    {
    if ( addressStatusIndex < 0 )
      {
      addressStatusTesterFirst();
      return;
      }
    
    // manage the end of list, wrapping around
    if ( addressStatusIndex >= getCount() )
      addressStatusIndex = 0;

    // pick one and go to the next
    McwAddress address = getAddress(addressStatusIndex++);
    
    if ( address == null )
      {
      println("addressStatusTesterFirst: WArNING address==null");
      return;
      }
    
    JobGetStatus ping = new JobGetStatus(stat, address );
    stat.mcwConsole.queue(ping);
    }

  private void addressStatusTesterFirst()
    {
    int counter=0;

    println("addressStatusTesterFirst: CALL");
    
    for ( McwAddress address : addressList )
      {
      // this go trough all registered addresses and do an initial ping
      JobGetStatus ping = new JobGetStatus(stat, address );
      stat.mcwConsole.queue(ping);
      
      // TODO, need to make it a parameter, in config dbase
      if ( counter++ > 10 ) break;
      }
    
    // this signal that next time it is standard algorithm, continue from where it arrived
    addressStatusIndex=counter;
    }
    
private final class AddressesKeeper implements Runnable
  {
  public void run()
    {
    addressKeeperRun_A();
    }
  }


private final class FireRowInserted implements Runnable
  {
  int rowindex;
  
  FireRowInserted(int p_rowindex)
    {
    rowindex=p_rowindex;
    }
  
  public void run()
    {
    fireTableRowsInserted(rowindex,rowindex);
    }
  }

private final class FireTableDataChanged implements Runnable
  {
  public void run()
    {
    fireTableDataChanged();
    }
  }

private static final class SortByWorktime implements Comparator<McwAddress>
  {
  @Override
  public int compare(McwAddress a1, McwAddress a2)
    {
    McwJobStats s1=a1.getRunStats();
    McwJobStats s2=a2.getRunStats();
    
    long delta_ms = s1.worktime_ms - s2.worktime_ms;
    
    if ( delta_ms > Integer.MAX_VALUE )
      delta_ms = delta_ms - Integer.MAX_VALUE;
    
    if ( delta_ms < Integer.MIN_VALUE )
      delta_ms = delta_ms + Integer.MAX_VALUE;

    return (int)delta_ms;
    }
  }

private static final class RoundRobinList
  {
  private ArrayList<McwAddress>list;
  
  private int readIndex=0;
  
  RoundRobinList ( ArrayList<McwAddress>list )
    {
    this.list = list;
    }
  
  public boolean isEmpty ()
    {
    return list.isEmpty();
    }

  /**
   * The list has a next without wrapping if the readIndex is before the end of the list 
   * @return
   */
  public boolean hasNext ()
    {
    return readIndex < list.size();
    }
  
  public McwAddress next ()
    {
    if ( readIndex >= list.size() )
      readIndex=0;
    
    return list.get(readIndex++);
    }
  }

  }
