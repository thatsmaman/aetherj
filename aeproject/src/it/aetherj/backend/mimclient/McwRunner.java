/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.backend.mimclient;

import java.awt.BorderLayout;
import java.net.URL;
import java.security.PublicKey;
import java.util.Date;

import javax.swing.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.aetherj.backend.Stat;
import it.aetherj.backend.dbase.AeDbase;
import it.aetherj.backend.dbtbls.AedbOperations;
import it.aetherj.backend.gui.utils.ConsoleFrame;
import it.aetherj.backend.mimclient.ssl.McwJobStats;
import it.aetherj.backend.utils.CryptoEddsa25519;
import it.aetherj.boot.*;
import it.aetherj.protocol.*;
import it.aetherj.shared.Aeutils;
import it.aetherj.shared.v0.*;

/**
 * Basically, this has a duty to do and will try to do it
 * The reason it makes a JFrame is so I can close when the thread is done
 * 
 * Server responds to GETs with the caches and to POSTS with the live data from the database.
 */
public abstract class McwRunner implements Comparable<McwRunner>
  {
  private static final String classname="McwRunner";
  
  protected final Stat stat;
  protected final McwParams mcwParams;
  
  private final LimitedTextArea logArea;
  private final JCheckBox pauseShutdown;
  private final JTextField tickerField;
  private final JPanel workPanel;
  private final CryptoEddsa25519 crypto;
  
  protected McwAddress address;   // The address to work with
  protected AeDbase mimcDbase; 
  protected long job_start_time;

  private ConsoleFrame mcwFrame;  // if user ask to show, then this will be not null
  private Date runAt_time=new Date();    // when should this runner run ? by default, now
  private PrintlnProvider logConsole;  // provided at runtime
  private Throwable haveException;   // if I have a runtime exception, here it is
  private int tickCounter=0;
  
  public McwRunner(Stat stat,McwAddress address, McwParams params ) 
    {
    this.stat=stat;
    this.mcwParams=params;
    this.address=address;
    
    logArea = new LimitedTextArea(2000);
    pauseShutdown = new JCheckBox("Pause Shutdown");
    
    tickerField=new JTextField(5);
    tickerField.setEditable(false);
    
    workPanel = newWorkPanel();
    crypto = newCrypto();
    }
  
  private CryptoEddsa25519 newCrypto()
    {
    try
      {
      return new CryptoEddsa25519(stat);
      }
    catch ( Exception exc )
      {
      stat.println(classname+"newCrypto", exc);
      return null;
      }
    }

  protected MimPayload newMimPayload(MimFilterArray filters) throws JsonProcessingException
    {
    if ( filters == null )
      filters=new MimFilterArray();
    
    MimPayload payload = new MimPayload(mcwParams.entity);
  
    payload.address=stat.aeParams.getBackendAddress();
    payload.filters = filters.toArray();
    payload.ends_at=new MimTimestamp();
    payload.node_public_key = stat.aeParams.getMimPublicKey();
    payload.nonce = new MimNonce();
    payload.page_signature = newPayloadSignature(payload);
    payload.proof_of_work = newPowSigned(payload);

    return payload;
    }
  
  protected void resetJobStartTime ()
    {
    job_start_time=System.currentTimeMillis();
    }
  
  protected void setJobException ( Throwable exception )
    {
    haveException = exception;
    }
  
  protected PrintlnProvider getPrintln()
    {
    return logArea;
    }
    
  @Override
  public final int compareTo(McwRunner other)
    {
    return runAt_time.compareTo(other.getRunAt());
    }
  
  public Date getRunAt ()
    {
    return runAt_time;
    }
    
  public void setRunAt ( Date futureTime )
    {
    if ( futureTime == null )
      return;
    
    runAt_time = futureTime;
    }
    
  private JPanel newWorkPanel()
    {
    JPanel risul = new JPanel(new BorderLayout());
    
    risul.add(newNorthPanel(),BorderLayout.NORTH);
    risul.add(logArea.getComponentToDisplay(),BorderLayout.CENTER);
    
    return risul;
    }
  
  protected void incTick ()
    {
    tickerField.setText(Integer.toString(tickCounter++));
    }
  
  private JPanel newNorthPanel()
    {
    JPanel risul = new JPanel();
    
    risul.add(pauseShutdown);
    risul.add(tickerField);
    
    return risul;
    }
  
  public Integer getAddressId ()
    {
    if ( address == null )
      return null;
    
    return address.primary_id;
    }
    
  public McwAddress getAddress ()
    {
    return address;
    }
  
  protected void printlnConsole( String msg )
    {
    if ( logConsole != null )
      logConsole.println(msg);
    else
      println(msg);
    }

  protected void println ( String msg )
    {
    logArea.println(msg);
    }

  protected void println ( String msg, Throwable exc )
    {
    logArea.println(msg,exc);
    }

  /**
   * Do NOT cache this since it may be that I am changing the endpoint I am connectiong to
   * @return
   */
  public URL getUrl ()
    {
    try
      {
      URL pre = address.getUrl();
      
      String file = pre.getFile();
      
      if ( file != null && file.length() > 0 )
        file = "/"+file+mcwParams.getMethod();
      else
        file = mcwParams.getMethod();
      
      return new URL(pre.getProtocol(),pre.getHost(),pre.getPort(),file);
      }
    catch ( Exception exc )
      {
      println(classname+".getUrl",exc);
      return null;
      }
    }
  
  private MimPowValue newPowSigned ( MimPayload payload ) throws JsonProcessingException
    {
    ObjectMapper gs = new ObjectMapper();
    
    String powSource = gs.writeValueAsString(payload);
            
    println("POW source="+powSource);

    MimPowCalculator powc = new MimPowCalculator();
    
    MimPowValue pow = powc.newPowUnsigned(powSource, MimPowValue.pow_len_network, 60);

    MimSignature signature = crypto.mimSign(stat.aeParams.getPrivateKey(), pow.toString());
    
    return pow.getSigned(signature);
    }
  
  private MimSignature newPayloadSignature ( MimPayload payload ) throws JsonProcessingException
    {
    ObjectMapper gs = new ObjectMapper();
    
    String source = gs.writeValueAsString(payload);
            
    println("Payload Signaturesource="+source);

    return crypto.mimSign(stat.aeParams.getPrivateKey(), source);
    }
  
  protected boolean isPayloadSignatureCorrect ( MimPayload payload ) throws JsonProcessingException
    {
    if ( payload.page_signature == null || payload.page_signature.isEmpty() )
      return true;
    
    if ( payload.node_public_key == null || payload.page_signature.isEmpty() )
      return true;
    
    // save
    MimSignature saved = payload.page_signature;
    
    // zap it, golang does not have it into the marshalling, crappy golang
    payload.page_signature = null;
    
    PublicKey pk =  payload.node_public_key.getPublicKey();
    
    ObjectMapper gs = new ObjectMapper();
    String source = gs.writeValueAsString(payload);
    
    return crypto.mimVerify(pk, source, saved);
    }
  
  /**
   * This is importing from a "dialout" request, so, I already know the address...
   * I just need to update the node...
   * @param payload
   * @return number of rows imported
   */
  protected int mimParse ( MimPayload payload )
    {
    if ( payload == null ) 
      return 0;
    
    int imported=0;
    
    try 
      {
      // here I should update/insert information on this node
      // I need this so I pick up the node key, if it is the first time I connect there
      Integer node_id = stat.aedbFactory.aedbNode.dbaseSave(mimcDbase, payload);
      
      mimParse(payload.results);
      
      imported = mimParse(payload.response);
      
      stat.aedbFactory.aedbTstamp.dbaseSave(mimcDbase, node_id, payload.entity );
      }
    catch ( Exception exc )
      {
      println("mimParse", exc);
      // by doing this the feedback frame will open
      haveException=exc;
      }
    
    return imported;
    }
  
  private int mimParse ( MimResultsCache []r_cache )
    {
    if ( r_cache == null )
      return 0;
    
    int len=r_cache.length;
    
    for (int index=0; index<len; index++)
      mimParse(r_cache[index]);
    
    return len;
    }
  
  private void mimParse( MimResultsCache cache )
    {
    println("cache "+cache.response_url);
    }
    
  
  /**
   * Parse the given answer
   * @return the number of "objects" parsed
   */
  private int mimParse ( MimAnswer answer )
    {
    if ( answer == null )
      return 0;

    int counter=0;
    
    counter+=mimParse(answer.keys);
    counter+=mimParse(answer.boards);
    counter+=mimParse(answer.threads);
    counter+=mimParse(answer.addresses);
    counter+=mimParse(answer.posts);
    counter+=mimParse(answer.votes);
    
    // do NOT count the INDEXES for row imported !!
    mimParse(answer.boards_index);
    mimParse(answer.threads_index);
    mimParse(answer.posts_index);
    mimParse(answer.votes_index);
    mimParse(answer.keys_index);
    mimParse(answer.truststates_index);
    mimParse(answer.addresses_index);

    // do NOT count the manifest for row imported !!
    mimParse(answer.boards_manifest);
    mimParse(answer.threads_manifest);
    mimParse(answer.posts_manifest);
    mimParse(answer.votes_manifest);
    mimParse(answer.keys_manifest);
    mimParse(answer.truststates_manifest);
    mimParse(answer.addresses_manifest);

    return counter;  
    }
  
  private int mimParse ( MimPageManifest []entityArray )
    {
    if ( entityArray == null ) return 0;
    
    int len=entityArray.length;

    for (int index=0; index<len;index++)
      importEntity(entityArray[index]);
    
    return len;
    }

  private void importEntity ( MimPageManifest manifest )
    {
    try
      {
      AedbOperations oper = stat.aedbFactory.getAedb(mcwParams.entity);
  
      println(mcwParams.entity+" manifest "+manifest);
      
      // TODO
//      oper.dbaseSave(mimcDbase, manifest);
      }
    catch ( Exception exc )
      {
      println("importEntity", exc);
      // by doing this the feedback frame will open
      haveException=exc;
      }
    }

  
  private int mimParse ( MimGetEntity []entityArray )
    {
    if ( entityArray == null ) 
      return 0;
    
    int len=entityArray.length;

    for (int index=0; index<len;index++)
      importEntity(entityArray[index]);
    
    return len;
    }
  
  private void importEntity ( MimGetEntity obj )
    {
    try
      {
      AedbOperations oper = stat.aedbFactory.getAedb(obj.getMimEntity());
      oper.dbaseSave(mimcDbase, obj);
      }
    catch ( Exception exc )
      {
      println("importEntity", exc);
      // by doing this the feedback frame will open
      haveException=exc;
      }
    }

    

  private boolean hasPauseShutdown()
    {
    return pauseShutdown.isSelected();
    }
  
  private boolean haveWaitOnExit ()
    {
    // if any of the following is true I have to wait on exit
    return haveException != null || hasPauseShutdown() || mcwFrame != null || mcwParams.onExitWait_s > 0;
    }
  
  /**
   * This basically slow down shutdown and allow you to pause it
   * Plus it shows the Logging window
   */
  private void shutdownPacifier ()
    {
    if ( haveWaitOnExit() )
      {
      showFrame();
      
      // assume this is the wait time
      int wait_s=mcwParams.onExitWait_s;
      
      // adjust to a reasonable value if wrong
      if ( wait_s <= 0 ) wait_s = 10;
      
      for (int index=wait_s; index >0; )
        {
        Aeutils.sleepSec(1);
        
        incTick();
        
        if ( hasPauseShutdown() ) continue;
        
        logArea.print(index+" ");
        
        index--;
        }
      }
    
    SwingUtilities.invokeLater(new RemoveFromModelRunner());
    }
  
  /**
   * Subclasses must define this, the result is the connection status request
   * @return
   * @throws Exception
   */
  protected abstract int runJob () throws Exception;
  
  /**
   * Any thread that wish to "run" this job must call this one
   * @param logConsole
   */
  public final void run (PrintlnProvider logConsole)
    {
    try
      {
      this.logConsole = logConsole;
      
      SwingUtilities.invokeLater(new AddToModelRunner());
      
      mimcDbase = stat.newWorkDbase();

      if ( ! mimcDbase.isConnected() )
        {
        println("NO dbase available, aborting");
        return;        
        }
      
      // record the starting time of this job
      job_start_time=System.currentTimeMillis();

      int rescode=runJob();
      
      if ( rescode != McwJobStats.res_OK )
        println(McwJobStats.getResMessage(rescode));
      }
    catch ( Exception exc )
      {
      println(classname+".run()",exc);
      haveException=exc;
      }
    finally
      {
      mimcDbase.close();
      shutdownPacifier();
      }
    }
  
  @Override
  public String toString()
    {
    StringBuilder risul = new StringBuilder(500);
    
    risul.append("r="+address);
    
    return risul.toString();
    }
  
  
  public void showFrame ()
    {
    if ( SwingUtilities.isEventDispatchThread() )
      swingShowFrame();
    else
      Aeutils.SwingInvokeAndWait(new ShowFrameRunnable());
    
    }


  private void swingShowFrame ()
    {
    if ( mcwFrame == null )
      {
      mcwFrame = new ConsoleFrame(classname);
      mcwFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
      mcwFrame.setCenterPanel(workPanel);
      mcwFrame.pack();
      mcwFrame.setLocationRelativeTo(null);
      }
    
    mcwFrame.setVisible(true);
    }


private class AddToModelRunner implements Runnable
  {
  public void run()
    {
    stat.mcwRunningModel.addRunner(McwRunner.this);
    }
  }

private class RemoveFromModelRunner implements Runnable
  {
  public void run()
    {
    stat.mcwRunningModel.removeRunner(McwRunner.this);

    if ( mcwFrame != null )
      {
      mcwFrame.setVisible(false);
      mcwFrame.dispose();
      }
    }
  }

private final class ShowFrameRunnable implements Runnable
  {
  @Override
  public void run()
    {
    swingShowFrame();
    }
  }

  }
