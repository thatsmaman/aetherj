package it.aetherj.protocol;

import com.fasterxml.jackson.annotation.*;

/**
 * As from postrespgenerator.go look for resp.Endpoint
 * Also server.go 
 */
public class MimEndpoint
  {
  private String endpoint;
  
  public static final String post_response="post_response";
  public static final String endpoint_node="node";
  public static final String endpoint_entity="entity";
  public static final String bootstrappers="bootstrappers";
  
  @JsonCreator
  public MimEndpoint(String p_value)
    {
    endpoint = p_value;
    }
  
  /**
   * SInce this is a JsonValue it MUST return the value only
   */
  @JsonValue
  public String toString()
    {
    return endpoint;
    }
  }
