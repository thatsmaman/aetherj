/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.*;

/**
 * It is defined as simple string in Aethergo, I need a way to have a proper handling 

type Fingerprint string // 64 char, it is actually a hex of a binary hash

Fingerprints are how objects refer to other. 
A fingerprint is a SHA256 hash of the object. 
Signatures are how objects are linked to their creators. 
A signature is an ECDSA signature with the user's private key, and can only be created by the user creating the object. 
Signatures are optional, it's OK to be anonymous and thus have no key and no signatures. 
Proof of Work provides protection against spam and DDoS by creation of objects computationally expensive enough 
to make bulk creation infeasible.

 */
public final class MimFingerprint
  {
  public static final int fingerprint_bytes_len=32;   // 
  
  private byte []fingerprint;
    
  /**
   * there are quite a few places where a "null" fingerprint is needed
   */
  public MimFingerprint()
    {
    fingerprint=null;
    }
  
  /**
   * This wants an ALREADY CALCULATED fingerprint in HEX format
   */
  @JsonCreator
  public MimFingerprint(String fingerprint_hex)
    {
    if ( fingerprint_hex == null || fingerprint_hex.length() < fingerprint_bytes_len * 2)
      fingerprint=null;
    else
      fingerprint=MimHex.convStringToBin(fingerprint_hex);
    }

  /**
   * Used by dbase, when picking up values
   */
  public MimFingerprint(byte []fingerprint_bin)
    {
    fingerprint = fingerprint_bin;
    }

  /**
   * Update this fingerprint with the digested value from given input
   * @param input
   */
  public void update ( String input ) 
    {
    fingerprint=calculate(input);
    }

  
  /**
   * Needed to be able to have unique fingerprints in a list of fingerprints
   */
  @Override
  public boolean equals ( Object input )
    {
    if ( input == null ) 
      return false;

    // if it is the same pointer, then it is obviously the same
    if ( input == this )
      return true;
    
    if ( input instanceof MimFingerprint )
      {
      MimFingerprint ofinger = (MimFingerprint)input;
      // method takes care of null values
      return Arrays.equals(fingerprint, ofinger.fingerprint);
      }
    else
      {
      return false;
      }
    }
  
  
  
  private static byte []calculate ( String input ) 
    {
    MimDigest digestAlgo = new MimDigest();
    return digestAlgo.digestOnce(input);
    }

  public boolean isNull ()
    {
    return fingerprint==null;
    }
  
  public byte[] getFingerprint()
    {
    return fingerprint;
    }
  
  public boolean verify ( String input ) throws UnsupportedEncodingException
    {
    if ( fingerprint==null || input == null || input.length() < 1 )
      return false;
          
    return Arrays.equals(fingerprint, calculate(input));
    }
  
  @JsonValue
  public String toString()
    {
    if ( fingerprint == null )
      return "";
    
    return MimHex.convBinToHexString(fingerprint);
    }
  }
