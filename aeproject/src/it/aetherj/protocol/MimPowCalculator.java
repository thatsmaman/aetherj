/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.Random;

/**
 * This is used by anybody (even a web client) to calculate proof of work The
 * original is proofofwork.go this one retains the logic
 * Since this is used by BOTH Java server + GWT Client (web browser) you MUST not put dependent parts in it
 * NOTE that the way things are done you have to have the server provide the raw string for POW and the web client
 * to do the calculation that will be sent over the wire to server
 */
public class MimPowCalculator
  {
  // I want to have a bitmask to see if any of the leftside bits are 1
  // All bytes (even in LE CPU) have MSB on the left and if we just want ONE bit, the mask to apply is 0x80 
  private static final int MASKS [] = { 0, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };

  private final MessageDigest digestAlgo;
  private final Random randomAlgo;

  public MimPowCalculator() 
    {
    digestAlgo = newSha256();
    randomAlgo = new Random(System.currentTimeMillis());
    }
  
  private MessageDigest newSha256 ()
    {
    try
      {
      return MessageDigest.getInstance(AetherCo.DIGEST_ALGO);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      return null;
      }
    }
  
  /**
   * Pitfalls: encoding conversion, number of digest used
   * @param input
   * @return
   */
  private byte [] powDigest ( String input ) 
    {
    try
      {
      // every time digest() is called the "engine" is reset
      byte [] out = digestAlgo.digest(input.getBytes(AetherCo.POW_CHARSET));
      out = digestAlgo.digest(out);
      return digestAlgo.digest(out);
      }
    catch ( Exception exc )
      {
      // hopefully this is portable in the browser too...
      exc.printStackTrace();
      return new byte[0];
      }
    }
  
  private static final String LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  private static final int    SEED_LEN = 16;

  public static String newRandomSeed (Random random) 
    {
    final int charsLen = LETTERS.length();
    
    StringBuilder seedString = new StringBuilder(SEED_LEN);
    
    for (int index = 0; index < SEED_LEN; index++) 
      seedString.append(LETTERS.charAt(random.nextInt(charsLen)));
    
    return seedString.toString();     
    }
  
  /**
   * The test starts from the beginning of the array, index 0
   * @param buffer
   * @param bytesCount how many bytes to check for zero
   * @param bitsCount how man of the remaining bits to check
   * @return true if it has enough zeros at the beginning
   */
  private boolean hasEnoughZeros ( byte [] buffer, int bytesCount, int bitsCount )
    {
    int index;
    
    for ( index=0; index<bytesCount; index++)
      if ( buffer[index] != 0 )
        return false;

    // Leave the generic algorithm, it works even when bitsCount==0
    int lastByte = buffer[index] & 0xFF;

    // it has all bits if the last byte anded with the mask is actually zero
    return ( lastByte & MASKS[bitsCount]) == 0;
    }
    
  
  /**
   * Difficulty is the number of bits of the signature that have to be zero to demonstrate pow
   * A normal value is 21 or 18
   * @param input
   * @param difficulty
   * @param bailout_s number of seconds to bail out of calculation
   * @return the calculated pow values
   * @throws UnsupportedEncodingException
   */
  public MimPowValue newPowUnsigned ( String input, int difficulty, int bailout_s )
    {
    if ( difficulty >= 100 )
      throw new IllegalArgumentException("difficulty "+difficulty+" is too big");
    
    if ( bailout_s < 10 || bailout_s > 6000 )
      throw new IllegalArgumentException("bailout_s "+bailout_s+" is <=2 || > 6000");

    String salt = newRandomSeed(randomAlgo);
    
    // the future time to check to decide if timeout
    long timeout_future = System.currentTimeMillis() + bailout_s * 1000;
    
    String inputToBePoWd = difficulty + input + salt;
    
    int w_zeroBytesCount = difficulty / 8;
    int w_zeroBitsCount = difficulty % 8;
    
    int counter=0;
    int timeoutPacifier=0;
    
    for(;;)
      {
      byte []hash = powDigest (inputToBePoWd + counter);
      
      if ( hasEnoughZeros(hash,w_zeroBytesCount, w_zeroBitsCount))
        break;
      
      counter++;

      // if no enough cycles have passed for testing the timeout
      if ( timeoutPacifier++ < 5000 ) continue;
      
      timeoutPacifier=0;
      
      if ( System.currentTimeMillis() > timeout_future)
        throw new IllegalArgumentException("difficulty is too high for the allotted time");
      }
    
    return new MimPowValue(difficulty,salt,counter);
    }
  
  /**
   * @param input
   * @return
   */
  public boolean isPowValid ( String input, MimPowValue pow)
    {
    int difficulty = pow.getDifficulty();
    
    String inputToBePoWd = difficulty + input + pow.getSalt();
    
    int w_zeroBytesCount = difficulty / 8;
    int w_zeroBitsCount = difficulty % 8;

    byte []hash = powDigest (inputToBePoWd + pow.getCounter());
    
    return hasEnoughZeros(hash,w_zeroBytesCount, w_zeroBitsCount);

    }
  
  
  }
