/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

import com.fasterxml.jackson.annotation.*;

/**
 * This replaces a generic string when talking about "identifiers"
 * It is a way to make sure that you are actually passing a MimEntiry identifier around
 */
public class MimEntity implements Comparable<MimEntity>
  {
  public static final int entity_len_max=20;
  
  private final String aentity;  // The Aether entity value
  private final String amethod;  // the Web method to call for post requests, including initial slash
  
  @JsonCreator
  public MimEntity(String p_value)
    {
    this(p_value,null);
    }
  
  public MimEntity(String p_aentity, String p_amethod)
    {
    if ( p_aentity == null ) 
      throw new IllegalArgumentException("MimEntity: value == null ");

    aentity = p_aentity;
    amethod=p_amethod;
    }
  
  public String getValue ()
    {
    return aentity;
    }
  
  public String getMethod ()
    {
    return amethod;
    }
  
  
  @Override
  public int compareTo(MimEntity input)
    {
    if ( input == null ) return -1;
    
    return aentity.compareTo(input.aentity);
    }

  /**
   * Equality is based on the value not on the object itself
   */
  @Override
  public final boolean equals ( Object input )
    {
    if ( input == null ) 
      return false;

    if ( input == this )
      {
      // if it is the same pointer, then it is obviously the same
      return true;
      }
    
    if ( input instanceof MimEntity )
      {
      MimEntity a_input = (MimEntity)input;
      return aentity.equals(a_input.aentity);
      }
    else if ( input instanceof String )
      {
      // as a bonus I allow comparing a string
      return aentity.equals(input);
      }
    else
      {
      return false;
      } 
    }
        
  /**
   * SInce this is a JsonValue it MUST return the value only
   */
  @JsonValue
  public String toString()
    {
    return aentity;
    }
  }
