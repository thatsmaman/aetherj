/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.protocol;

import com.fasterxml.jackson.annotation.*;

/**
 * Helper to properly format the POW    
 * This has to stay here since it will need to be used GWT client side  
 * NOTE the use of JsonCreator and JsonValue to hide the entire inner behavior  
 * The contract is that if any  value is invalid an empty string is emitted  
 */
public class MimPowValue 
  {
  // used to allocate space in DB
  public static final int pow_ascii_len=1024;
  
  public static final int pow_len_posts=21;
  public static final int pow_len_network=16;
  
  private static final String begins="MIM1:";
  
  private int difficulty;
  private String salt;
  private int counter;
  private String signature;

  /**
   * It is legal to have an empty pow value, you need it since the GO logic is to have a Json with an empty string 
   * It would have been easier if no pow field was present at all.
   */
  public MimPowValue()
    {
    }
  
  @JsonCreator
  public MimPowValue(String formatted)
    {
    if ( formatted == null ) return;
    
    if ( ! formatted.startsWith(begins)) return;
    
    try
      {
      String [] split = formatted.split(":");
      difficulty = Integer.parseInt(split[1]);
      salt= split[5];
      counter = Integer.parseInt(split[6]); 
      signature= split[7];
      }
    catch ( Exception exc )
      {
      }
    }

  /**
   * This is when you calculate Pow
   * @param difficulty
   * @param salt
   * @param counter
   */
  public MimPowValue ( int difficulty, String salt, int counter )
    {
    this.difficulty=difficulty;
    this.salt=salt;
    this.counter=counter;
    }
  
  public int getDifficulty()
    {
    return difficulty;
    }
  
  public int getCounter()
    {
    return counter;
    }

  public String getSalt()
    {
    return salt;
    }

  public MimPowValue getSigned ( MimSignature signature )
    {
    this.signature=signature.toString();

    return this;
    }
    
  @JsonValue
  public String toString()
    {
    if ( counter == 0 )
      return "";
          
    StringBuilder risul = new StringBuilder(500);
    
    risul.append(begins);
    risul.append(difficulty);
    risul.append("::::");
    risul.append(salt);
    risul.append(":");
    risul.append(counter);
    risul.append(":");

    if ( signature != null )
      risul.append(signature);
    
    return risul.toString();
    }

  }

