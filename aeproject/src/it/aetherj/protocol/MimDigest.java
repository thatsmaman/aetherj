package it.aetherj.protocol;

import java.security.MessageDigest;

/**
 * Quite often I need to create an engine for sha256 and there is the usual issue of the exception
 * So, wrap it up and enclose all peculiarities here, including the digesting of strings
 */
public class MimDigest
  {
  public static final String digest_charset="UTF-8";

  private final MessageDigest digestAlgo;

  public MimDigest() 
    {
    digestAlgo = newSha256();
    }
  
  private MessageDigest newSha256 ()
    {
    try
      {
      return MessageDigest.getInstance(AetherCo.DIGEST_ALGO);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      return null;
      }
    }

  public byte[] digestOnce (String source)
    {
    if ( source == null )
      return null;
    
    try
      {
      byte []b_input = source.getBytes(digest_charset);
      return digestAlgo.digest(b_input);
      }
    catch ( Exception exc )
      {
      exc.printStackTrace();
      return new byte[0];
      }

    }
    
  
  }
