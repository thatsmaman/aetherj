/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared;

import it.aetherj.boot.*;

public final class Aedbg extends Debug 
  {
  public static final int DB_update            = 0x00000001;      // debug dbase update.
  public static final int DB_dbase             = 0x00000002;      // dbase generic
  public static final int DB_createtable       = 0x00000004;      // dbase table automagic creation 
  public static final int HTTP_tx              = 0x00000008;      // HTTP tx
  public static final int HTTP_rx              = 0x00000010;      // HTTP rx
  public static final int SCHEDULER            = 0x00000020;
  
  public static final int entity_Node          = 0x00000040;
  public static final int entity_Addresses     = 0x00000080;
  public static final int entity_Upki          = 0x00000100;
  public static final int entity_Board         = 0x00000200;
  public static final int entity_Thread        = 0x00000400;
  public static final int entity_Post          = 0x00000800;
  public static final int entity_Vote          = 0x00001000;
  
  public static final int entity_ANY           = 0x1FC0;
  
  public Log getLog ()
    {
    return log;
    } 
    
  public Aedbg(Log log)
    {
    super(log);
    }
    
  }
