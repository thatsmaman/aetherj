/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared;

import it.aetherj.backend.BackendVersion;

/**
 * It is just easier to have a holder to pass around instead of two strings
 * The issue is that I also have to consider the dbase version
 */
public final class ApplicationVersion
  {
  private final String backendVersion;     // MUST be yyyyMMdd so it can be converted to int and compared
  
  private String dbaseVersion;

  public ApplicationVersion() 
    {
    backendVersion = BackendVersion.VERSION;
    dbaseVersion   = "DB?";
    }
  
  public String getBackendVersion ()
    {
    return backendVersion;
    }
    
  public void setDbaseVersion (String version)
    {
    dbaseVersion=version;
    }
  
  public String getDbaseVersion ()
    {
    return dbaseVersion;
    }
  
  public String toString ()
    {
    StringBuilder risul = new StringBuilder(200);
    
    risul.append("backendVersion "+backendVersion+"\n");
    risul.append("dbaseVersion "+dbaseVersion);
    
    return risul.toString();
    }
  } 
