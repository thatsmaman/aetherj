package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 
 // Manifest type
type PageManifest struct {
  Page     uint64               `json:"page_number"`
  Entities []PageManifestEntity `json:"entities"`
}

type PageManifestEntity struct {
  Fingerprint Fingerprint `json:"fingerprint"`
  LastUpdate  Timestamp   `json:"last_update"`
  

 */

public class MimPageManifest
  {
  public int page_number;
  public MimPageManifestE []entities;
  
  @JsonIgnore
  public String toString()
    {
    if ( entities != null )
      return page_number+" e_count="+entities.length;
    else
      return page_number+" null entities";
    }
  }
