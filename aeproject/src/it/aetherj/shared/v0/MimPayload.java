/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import com.fasterxml.jackson.annotation.JsonInclude;

import it.aetherj.protocol.*;

/**
 * 

type ApiResponse struct {
  NodeId        Fingerprint   `json:"-"` // Generated and used at the ApiResponse signature verification, from the NodePublicKey. It doesn't transmit in or out, only generated on the fly. This blocks both inbound and outbound.
  NodePublicKey string        `json:"node_public_key,omitempty"`
  Signature     Signature     `json:"page_signature,omitempty"`
  ProofOfWork   ProofOfWork   `json:"proof_of_work"`
  Nonce         Nonce         `json:"nonce,omitempty"`
  EntityVersion int           `json:"entity_version,omitempty"`
  Address       Address       `json:"aeaddres,omitempty"`
  Entity        string        `json:"entity,omitempty"`
  Endpoint      string        `json:"endpoint,omitempty"`
  Filters       []Filter      `json:"filters,omitempty"`
  Timestamp     Timestamp     `json:"timestamp,omitempty"`
  StartsFrom    Timestamp     `json:"starts_from,omitempty"`
  EndsAt        Timestamp     `json:"ends_at,omitempty"`
  Pagination    Pagination    `json:"pagination,omitempty"`
  Caching       Caching       `json:"caching,omitempty"`
  Results       []ResultCache `json:"results,omitempty"`  // Pages
  ResponseBody  Answer        `json:"response,omitempty"` // Entities, Full size or Index versions.
}


 * 
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MimPayload implements MimGetEntity
  {
  public MimPublicKey node_public_key;   // need a value
  public MimSignature page_signature;    // normally need a value, leave null on construct 
  public MimPowValue proof_of_work=new MimPowValue();  // need a "" value to calculate pow
  public MimNonce nonce;                 // need a value
  public int      entity_version=1;
  public MimAddress address;             // must be not null at send time
  public MimEntity entity;               // what this thing is
  public MimEndpoint endpoint;           // coming from
  public MimFilter []filters; 
  public MimTimestamp timestamp=new MimTimestamp();   // MUST be NOW since it used by the nonce algorithm 
  public MimTimestamp starts_from;       // WHAT IS THIS FOR ???  
  public MimTimestamp ends_at;           // WHAT IS THIS FOR ???
  public MimPagination pagination=new MimPagination();
  public MimCaching caching = new MimCaching();
  public MimResultsCache []results;  // this may be null
  public MimAnswer response = new MimAnswer();
  
  public MimPayload()
    {
    // for Json serialization
    }
  
  public MimPayload(MimEntity entity)
    {
    this.entity = entity; 
    }
  
  /**
   * Subclasses must call super.getPowSource() as first call
   * @param result the string to be used in Pow Calculation
  public void getPowSource ( StringBuilder result )
    {
    result.append(node_public_key);
    result.append(nonce);
    address.getPowSource(result);
    result.append(entity);
    result.append(endpoint);
    result.append(timestamp);
    }
   */
      
  public String toString()
    {
    StringBuilder risul = new StringBuilder(200);
    
    risul.append("endpoint="+endpoint);
    risul.append("entity="+entity);
    risul.append("aeaddres.port="+address.port);
    
    return risul.toString();
    }

  @Override
  public MimEntity getMimEntity()
    {
    return entity;
    }
  }
