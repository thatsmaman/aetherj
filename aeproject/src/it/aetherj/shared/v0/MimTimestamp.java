/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.*;

/**
 * A timestamp_s in MIM is sent / received as a long with seconds since epoch
 * A class is needed to wrap behavior
 */
public class MimTimestamp
  {
  private long timestamp_s;
  
  public MimTimestamp()
    {
    timestamp_s = System.currentTimeMillis()/1000;
    }
  
  @JsonCreator
  public MimTimestamp(Long mimValue)
    {
    if ( mimValue == null )
      return;
    
    timestamp_s = mimValue.longValue();
    }
  
  /**
   * When you just want to create a new one
   * NOTE that this one does NOT accept null values
   */
  public MimTimestamp(long time_s)
    {
    timestamp_s=time_s;
    }

  public MimTimestamp(Timestamp from_sql)
    {
    if ( from_sql == null )
      return;
    
    timestamp_s = from_sql.getTime()/1000;
    }

  /**
   * A timestamp is considered null if it has a value of 0
   */
  public boolean isNull ()
    {
    return timestamp_s == 0;
    }
  
  public void setValue (long seconds_since_epoch)
    {
    timestamp_s=seconds_since_epoch;
    }
  
  /**
   * If you add a negative number you go back in time
   */
  public MimTimestamp add_seconds ( int seconds )
    {
    return new MimTimestamp(timestamp_s+seconds);
    }

  public MimTimestamp add_days ( int days )
    {
    int seconds = days * 24*60*60;
    
    return new MimTimestamp(timestamp_s+seconds);
    }

  public Date getDate ()
    {
    return new Date(timestamp_s * 1000);
    }
  
  public Timestamp getSql ()
    {
    return new Timestamp(timestamp_s * 1000);
    }
  
  @JsonValue
  public long getJsonValue()
    {
    return timestamp_s;
    }
  
  @Override
  public String toString()
    {
    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    return f.format(getDate());
    }

  }
