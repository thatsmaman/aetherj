/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import it.aetherj.protocol.*;

/**
 * Ok, found out this is the public key of users, basically, the main authentication method
 
type Key struct { // Mutables: Expiry, Info, Meta
  ProvableFieldSet
  Type          string      `json:"type"`
  Key           string      `json:"key"`
  Expiry        Timestamp   `json:"expiry"`
  Name          string      `json:"name"`
  Info          string      `json:"info"`
  EntityVersion int         `json:"entity_version"`
  Meta          string      `json:"meta"`
  RealmId       Fingerprint `json:"realm_id"`
  EncrContent   string      `json:"encrcontent"`
  UpdateableFieldSet
}

type UpdateableFieldSet struct { // Common set of properties for all objects that are updateable.
  LastUpdate        Timestamp   `json:"last_update"`
  UpdateProofOfWork ProofOfWork `json:"update_proof_of_work"`
  UpdateSignature   Signature   `json:"update_signature"`
}

I could extend a class to include all updateableFieldSet, BUT the ordering is wrong, they are ath the end, not at the beginning...
 
 
 *
 */
public class MimKey extends MimProvableFieldSet implements MimGetEntity
  {
  public String   type;        // normally ed25519
  public MimPublicKey key;        
  public MimTimestamp expiry;  // normally 0
  public String   name;        // this is the username registerend in Aether
  public String   info;        // whatever the user wrote when created account
  public int      entity_version;  // normally 1
  public String   meta="";         // empty
  public MimFingerprint realm_id;  // empty   
  public String   encrcontent="";  // empty
   
  // the following will be in more than one class...
  public MimTimestamp last_update;
  public MimPowValue  update_proof_of_work;
  public MimSignature update_signature;

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aekey;
    }
  
  
  
  }
