/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import it.aetherj.protocol.*;

/**
 
type Post struct { // Mutables: Body, Meta
  ProvableFieldSet
  Board          Fingerprint `json:"board"`
  Thread         Fingerprint `json:"thread"`
  Parent         Fingerprint `json:"parent"`
  Body           string      `json:"body"`
  Owner          Fingerprint `json:"owner"`
  OwnerPublicKey string      `json:"owner_publickey"`
  EntityVersion  int         `json:"entity_version"`
  Meta           string      `json:"meta"`
  RealmId        Fingerprint `json:"realm_id"`
  EncrContent    string      `json:"encrcontent"`
  UpdateableFieldSet
}

type UpdateableFieldSet struct { // Common set of properties for all objects that are updateable.
  LastUpdate        Timestamp   `json:"last_update"`
  UpdateProofOfWork ProofOfWork `json:"update_proof_of_work"`
  UpdateSignature   Signature   `json:"update_signature"`
}

I could extend a class to include all updateableFieldSet, BUT the ordering is wrong, they are ath the end, not at the beginning...
 
 
 *
 */
public class MimPost extends MimProvableFieldSet implements MimGetEntity
  {
  public MimFingerprint board;
  public MimFingerprint thread;
  public MimFingerprint parent;
  public String body="";
  public MimFingerprint owner;
  public MimPublicKey owner_publickey;
  public int    entity_version;
  public String meta="";
  public MimFingerprint realm_id;    
  public String encrcontent="";
   
  // the following will be in more than one class...
  public MimTimestamp last_update;
  public MimPowValue update_proof_of_work;
  public MimSignature update_signature;

  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aepost;
    }
  
  
  
  }
