/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

/**

type ResultCache struct { // These are caches shown in the index endpoint of a particular entity.
  ResponseUrl string    `json:"response_url"`
  StartsFrom  Timestamp `json:"starts_from"`
  EndsAt      Timestamp `json:"ends_at"`
}

 */
public class MimResultsCache
  {
  public static final int level_count=1;   // cannot understand if there are any more pages or not
  
  public String response_url;
  public MimTimestamp starts_from;
  public MimTimestamp ends_at;
  }
