/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.shared.v0;

import it.aetherj.protocol.*;

/**

// type Fingerprint [64]byte // 64 char ASCII
type Fingerprint string // 64 char ASCII
type Nonce string       // max 64 char ASCII
type Timestamp int64    // UNIX Timestamp
// type ProofOfWork [1024]byte
type ProofOfWork string // temp
// type Signature [512]byte
type Signature string // temp
type Location string



type Address struct { // Mutables: None
  Location           Location    `json:"location"`
  Sublocation        Location    `json:"sublocation"`
  LocationType       uint8       `json:"location_type"`
  Port               uint16      `json:"port"`
  Type               uint8       `json:"type"`
  LastSuccessfulPing Timestamp   `json:"-,omitempty"`
  LastSuccessfulSync Timestamp   `json:"-,omitempty"`
  Protocol           Protocol    `json:"protocol"`
  Client             Client      `json:"client"`
  EntityVersion      int         `json:"entity_version"`
  RealmId            Fingerprint `json:"realm_id"`
  Verified           bool        `json:"-,omitempty"` // This is normally part of the provable field set, but aeaddres is not provable, so provided here separately.
}

 */


public class MimAddress implements MimGetSignatureSource,MimGetEntity
  {
  public static final int location_type_IIPv4=4;
  public static final int location_type_IIPv6=6;
  
  public static final int type_DYNAMIC=2;
  
  public String location=""; 
  public String sublocation="";
  public int    location_type=location_type_IIPv4;  // assume IP v4
  public int    port;
  public int    type=type_DYNAMIC;
  public MimProtocol protocol=new MimProtocol();    // let me try to flatten this also
  public MimClient client=new MimClient();          // this can be flattened
  public int    entity_version=1;
  public MimFingerprint realm_id=new MimFingerprint();
  
  public MimAddress()
    {
    this(0);
    }

  public MimAddress (int listen_port)
    {
    port=listen_port;
    }
  
  @Override
  public MimEntity getMimEntity()
    {
    return MimEntityList.aeaddres;
    }

  @Override
  public void getPowSource(StringBuilder result)
    {
    result.append(location);
    result.append(port);
    result.append(entity_version);
    protocol.getPowSource(result);
    client.getPowSource(result);
    result.append(entity_version);
    }

  }
