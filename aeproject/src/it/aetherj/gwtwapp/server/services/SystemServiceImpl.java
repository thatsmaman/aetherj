/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server.services;

import java.sql.SQLException;
import java.util.ArrayList;

import it.aetherj.backend.dbase.Dbkey;
import it.aetherj.backend.dbtbls.*;
import it.aetherj.boot.dbase.*;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.RewebSystemService;
import it.aetherj.gwtwapp.server.RemoteServiceServletPublic;

public final class SystemServiceImpl extends RemoteServiceServletPublic implements RewebSystemService
  {
	private static final long serialVersionUID = 1L;
	private static final String classname = "SystemServiceImpl.";



  public AegwPageHelpListRes getPageHelpList(AegwPageHelpListReq req)
    {
    ServiceEnv env = new ServiceEnv(this);
    AegwPageHelpListRes risul = new AegwPageHelpListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
 
    try
      {
      String query = "SELECT * FROM "+AegwPageHelp.cn_tbl+" WHERE "+
          AegwPageHelp.cn_lang+"=? ORDER BY pagehelp_desc ASC";
      
      Dprepared prepared = env.dbase.getPreparedStatement(query);
      prepared.setString(1,""+env.getClientLocale());

      Brs ars = prepared.executeQuery();

      while ( ars.next() ) risul.add(getOnePageHelpRecord ( ars ));
      
      prepared.close();
      }
    catch ( Exception exc )
      {
      env.logError(classname+"getPageHelpList",exc);
      risul.setEnglishErrorMessage(exc.toString());          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  private AegwPageHelp getOnePageHelpRecord (Brs ars ) throws SQLException
    {
    AegwPageHelp row = new AegwPageHelp();
     
    row.pagehelp_id       = ars.getInteger(AegwPageHelp.cn_id);
    row.pagehelp_lang     = ars.getString(AegwPageHelp.cn_lang);
    row.pagehelp_key      = ars.getString(AegwPageHelp.cn_key);
    row.pagehelp_filename = ars.getString(AegwPageHelp.cn_filename);
    row.pagehelp_desc     = ars.getString(AegwPageHelp.cn_desc);

    return row;
    }



  public AegwLabelSaveRes saveLabelList(AegwLabelSaveReq req)
    {
    ServiceEnv env = new ServiceEnv(this);
    AegwLabelSaveRes risul = new AegwLabelSaveRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      for (int index=0; index<req.labelList.size(); index++)
        {
        String label_value = null;
        
        AegwLabel label = req.labelList.get(index);

        // if I am really saving the value then I need to actually get it
        if ( req.isSaveLabelValueRequest ) label_value = label.label_value;

        env.dbase.labelSave(env.getClientLocale(),label.label_english,label.label_from_page,label_value);
        }
      }
    catch ( Exception exc )
      {
      env.logError(classname+"saveLabelList",exc);
      risul.setEnglishErrorMessage(exc.toString());          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  private String getAvailableLanguages (ServiceEnv env) throws SQLException
    {
    String query = "SELECT DISTINCT "+Dbkey.lblmap_lang+" FROM "+Dbkey.lblmap_tbl;
    StringBuilder risul = new StringBuilder(80);
    
    Dprepared prepared = env.dbase.getPreparedStatement(query);
    Brs ars = prepared.executeQuery();

    boolean insertComma = false;
    
    while ( ars.next() ) 
      {
      if ( insertComma ) risul.append(",");

      risul.append( ars.getString(Dbkey.lblmap_lang) );
      
      insertComma=true;
      }
    
    prepared.close();
    
    return risul.toString();
    }

  public AegwLabelListRes getLabelList(AegwLabelListReq req)
    {
    ServiceEnv env = new ServiceEnv(this);
    AegwLabelListRes risul = new AegwLabelListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      String query = "SELECT * FROM mlang.label_tbl "+
        " LEFT OUTER JOIN (SELECT * from mlang.lblmap_tbl where lblmap_lang=? ) filtrata ON lblmap_label_id = label_id "+
        " INNER JOIN (SELECT * FROM mlang.lblsee_tbl WHERE lblsee_path=?) filtropath ON lblsee_label_id = label_id "+
        " ORDER BY label_english ";
      
      Dprepared prepared = env.dbase.getPreparedStatement(query);
      int insIndex=1;
      prepared.setString(insIndex++,""+env.getClientLocale());  // NOT possible to do wildcarding this way I catch it or it-IT and so on
      prepared.setString(insIndex++,req.lblsee_path);
      
      Brs ars = prepared.executeQuery();
      while ( ars.next() )
        {
        AegwLabel label = getDbaseLabel ( ars );
        labelAddUsedList ( env, label );
        risul.add( label );
        }
      prepared.close();
      }
    catch ( Exception exc )
      {
      env.logError(classname+"saveLabelList",exc);
      risul.setEnglishErrorMessage(exc.toString());          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }
  
  private AegwLabel getDbaseLabel (Brs ars )
    {
    AegwLabel risul      = new AegwLabel();
    risul.label_id        = ars.getInteger(Dbkey.label_id);
    risul.label_english   = ars.getString(Dbkey.label_english);
    risul.label_value     = ars.getString(Dbkey.lblmap_value);
    risul.label_last_day_use = ars.getTimestamp(Dbkey.label_last_use_day);
    return risul;
    }
  
  private void labelAddUsedList(ServiceEnv env, AegwLabel label )
    {
    StringBuilder risul = new StringBuilder(80);
    
    Brs ars = env.dbase.selectTableUsing(Dbkey.lblsee_tbl,Dbkey.lblsee_label_id,label.label_id);
    
    boolean addSeparator = false;
    while ( ars.next() )
      {
      if ( addSeparator ) risul.append("  ");
      risul.append(ars.getString(Dbkey.lblsee_path));
      addSeparator=true;
      }
    ars.close();
    
    label.label_used_in_pages=risul.toString();
    }

  public AegwComboRes getComboList(AegwComboReq req)
    {
    ServiceEnv env = new ServiceEnv(this);
    AegwComboRes risul = new AegwComboRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }

    try
      {
      Brs ars = getComboBrs ( env, req );
      
      if ( req.combo_key_type == AegwComboReq.key_type_string )
        risul.stringRowList = getComboStringList(ars,req.key_col,req.value_col);
      
      if ( req.combo_key_type == AegwComboReq.key_type_integer )
        risul.integerRowList = getComboIntegerList(ars,req.key_col,req.value_col);

      ars.close();
      }
    catch ( Exception exc )
      {
      env.logError(classname+"getComboList",exc);
      risul.setEnglishErrorMessage(exc.toString());          
      }

    env.dispose();
    risul.markEnd();
    return risul;
    }

  /**
   * @param env
   * @param req
   * @return
   * @throws SQLException
   */
  private Brs getComboBrs ( ServiceEnv env,AegwComboReq req ) throws SQLException
    {
    String query = getComboQuery(req);
      
    Dprepared prepared = env.dbase.getPreparedStatement(query);
    
    int insIndex=1;
    
    if ( req.select_key != null ) 
      {
      if ( req.combo_key_type == AegwComboReq.key_type_integer ) prepared.setInt(insIndex++,req.select_value_integer);
      if ( req.combo_key_type == AegwComboReq.key_type_string ) prepared.setString(insIndex++,req.select_value_string);
      }
      
    if ( req.select_and_key != null )
      {
      prepared.setInt(insIndex++,req.select_and_value);
      }
    
    return prepared.executeQuery();
    }


  private String getComboQuery ( AegwComboReq req )
    {
    switch (req.getComboSelector())
      {
      case AegwComboReq.combo_selector_first:
        req.combo_tbl  = "combo.regioni";
        req.key_col    = "ID_regione";
        req.value_col  = "nome";
        req.select_key = null;
        break;

      default:
        throw new IllegalArgumentException("SQL Injection detected, User backtraked, authority informed");
      }

    StringBuilder query = new StringBuilder();
    query.append("SELECT "+req.key_col );
    query.append(","+req.value_col );
    query.append(" FROM "+req.combo_tbl);
    if ( req.select_key != null ) query.append(" WHERE "+req.select_key+"=? ");
    if ( req.select_and_key != null ) query.append(" AND "+req.select_and_key+"=? ");
    query.append(" ORDER BY "+req.value_col);
    return query.toString();        
    }

  private ArrayList<AegwComboStringRow>getComboStringList(Brs ars, String key_col, String value_col)
    {
    ArrayList<AegwComboStringRow> risul = new ArrayList<AegwComboStringRow>();
    
    while ( ars.next() ) 
      {
      String key = ars.getString(key_col);
      String value = ars.getString(value_col);
      risul.add(new AegwComboStringRow(key,value));
      }
    
    return risul;
    }

  private ArrayList<AegwComboIntegerRow>getComboIntegerList(Brs ars, String key_col, String value_col)
    {
    ArrayList<AegwComboIntegerRow> risul = new ArrayList<AegwComboIntegerRow>();
    
    while ( ars.next() ) 
      {
      Integer key = ars.getInteger(key_col);
      String value = "["+key+"] "+ars.getString(value_col);
      risul.add(new AegwComboIntegerRow(key,value));
      }
    
    return risul;
    }

  
  private void putOneThreadRecord ( AegwThreadListRes risul, Brs ars ) throws SQLException
    {
    AegwThread row = new AegwThread();

    row.thread_id       = ars.getInteger(AedbThread.cn_id);
    row.thread_name     = ars.getString(AedbThread.cn_name);
    row.thread_board_name = ars.getString(AedbBoard.cn_name);
    row.thread_link     = ars.getString(AedbThread.cn_link);
    row.thread_body     = ars.getString(AedbThread.cn_body);

    risul.add(row);
    }

  
  
  @Override
  public AegwThreadListRes getThreadList ( AegwThreadListReq req)
    {
    ServiceEnv env = new ServiceEnv(this);
    AegwThreadListRes risul = new AegwThreadListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
  
    try
      {
      AedbThread aedb=(AedbThread)env.getAedb(AedbThread.mimEntity);
      
      Brs ars = aedb.select(env.dbase, req.board_id);
  
      while ( ars.next() )
        putOneThreadRecord(risul,ars);
  
      ars.close();
      }
    catch ( Exception exc )
      {
      env.logError(classname+"getComboList",exc);
      risul.setEnglishErrorMessage(exc.toString());          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }
  
  private void putOneBoardRecord ( AegwBoardListRes risul, Brs ars ) throws SQLException
    {
    AegwBoard row = new AegwBoard();

    row.board_id         = ars.getInteger(AedbBoard.cn_id);
    row.board_name       = ars.getString(AedbBoard.cn_name);
    row.board_owner_name = ars.getString(AedbUpki.cn_user_name);
    row.board_desc       = ars.getString(AedbBoard.cn_desc);

    risul.add(row);
    }
  
  @Override
  public AegwBoardListRes getBoardList ( AegwBoardListReq req)
    {
    ServiceEnv env = new ServiceEnv(this);
    AegwBoardListRes risul = new AegwBoardListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
  
    try
      {
      AedbBoard aedb=(AedbBoard)env.getAedb(AedbBoard.mimEntity);
      
      Brs ars = aedb.select(env.dbase);
  
      while ( ars.next() )
        putOneBoardRecord(risul,ars);
  
      ars.close();
      }
    catch ( Exception exc )
      {
      env.logError(classname+"getBoardList",exc);
      risul.setEnglishErrorMessage(exc.toString());          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }


  
  
  
  
  
  
  private void putOnePostRecord ( AegwPostListRes risul, Brs ars ) throws SQLException
    {
    AegwPost row = new AegwPost();

    row.post_id         = ars.getInteger(AedbPost.cn_id);
    row.post_body       = ars.getString(AedbPost.cn_body);
    row.post_parent_id  = ars.getInteger(AedbPost.cn_parent_id);
    row.post_thread_id  = ars.getInteger(AedbPost.cn_parent_id);
    row.post_desc       = ars.getString(AedbBoard.cn_desc);

    risul.add(row);
    }
  
  @Override
  public AegwPostListRes getPostList ( AegwPostListReq req)
    {
    ServiceEnv env = new ServiceEnv(this);
    AegwPostListRes risul = new AegwPostListRes();
    
    if ( ! env.isWsessionValid(req.webSession))
      {
      risul.setEnglishErrorMessage("Session is invalid");
      return risul;
      }
  
    try
      {
      AedbPost aedb=(AedbPost)env.getAedb(AedbPost.mimEntity);
      
      Brs ars = aedb.select(env.dbase,req.thread_id);
  
      while ( ars.next() )
        putOnePostRecord(risul,ars);
  
      ars.close();
      }
    catch ( Exception exc )
      {
      env.logError(classname+"getPostList",exc);
      risul.setEnglishErrorMessage(exc.toString());          
      }
  
    env.dispose();
    risul.markEnd();
    return risul;
    }

  
  
  
  
  
  
  
  
  
  
  }
