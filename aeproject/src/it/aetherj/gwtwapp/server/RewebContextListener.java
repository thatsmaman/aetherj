/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server;

import javax.servlet.*;

import it.aetherj.backend.Stat;
import it.aetherj.boot.LimitedTextArea;

/**
 * NOTE that this HAS to be in web.xml in the WEB-INF directory
 * Eg: 
 
  <listener>
    <description>Prepare an environment for Web</description>
    <listener-class>it.aetherj.frontend.server.RewebContextListener</listener-class>
  </listener>

 * This is used to "bind" instances from the Aether server to the web server part
 * What happens is that a shared Stat that is peculiar to the web services is created
 */
public final class RewebContextListener implements ServletContextListener
  {
  private static final String classname="RewebContextListener";
  
  public static final String KEY_RewebStat = "RewebStat";
  
  public void contextInitialized(ServletContextEvent event)
    {
    System.out.println(classname + "contextInitialized: CALLED");

    // now it is time to share this stat to the various servlets
    ServletContext context = event.getServletContext();
    
    Stat systemStat = (Stat)context.getAttribute("SystemStat");
		LimitedTextArea websrvLog = (LimitedTextArea)context.getAttribute("WebLogArea");
															
    if ( systemStat == null )
      throw new IllegalArgumentException("MISSING systemStat");
    
    context.setAttribute(KEY_RewebStat, newRewebStat(systemStat, websrvLog));
    }


  private RewebStat newRewebStat (Stat systemStat, LimitedTextArea websrvLog)
    {
    RewebStat stat = new RewebStat(systemStat.log);

    stat.log.println(classname+".newRewebStat: CALL ");
 
		stat.websrvLog         = websrvLog;
    stat.dbg               = systemStat.dbg;
    stat.appVersion        = systemStat.appVersion;
    stat.mainDbaseProperty = systemStat.config.mainDbaseProperty;
    stat.connpool          = new RewebConnpool(stat);
		stat.jrncServer        = systemStat.aebendControl;
		stat.labelFactory      = systemStat.labelFactory;
		stat.aedbFactory       = systemStat.aedbFactory;

    return stat;          
    }
  
  public void contextDestroyed(ServletContextEvent event)
    {
    System.out.println(classname + "contextDestroyed: CALLED");

    ServletContext context = event.getServletContext();

    RewebStat stat = (RewebStat)context.getAttribute(KEY_RewebStat);
    
    stat.connpool.close();
    }
  }
