/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server;

import it.aetherj.backend.dbase.labels.LabelFactory;
import it.aetherj.backend.dbtbls.AedbFactory;
import it.aetherj.backend.server.AebendController;
import it.aetherj.boot.*;
import it.aetherj.boot.cfgproperty.PropertyGroup;
import it.aetherj.shared.*;

/**
 * When the servlet context is initialized this is allocated and set as an attribute of the context
 * I will then get from this basically "static" class whatever I need to work with. 
 * I do not pass the original "system" stata since I wish to be able to control what I am sharing
 */
public class RewebStat
  {
  public final PrintlnProvider log;
  
  public Aedbg dbg;
	public LimitedTextArea    websrvLog;     // this goes to the webserver log window
  public ApplicationVersion appVersion;
  public PropertyGroup      mainDbaseProperty;
  public RewebConnpool      connpool;
	public AebendController       jrncServer;
	public AedbFactory        aedbFactory;    // Factory to access the database tables from server side
  public LabelFactory       labelFactory;   // for when label translation is required
  
  public RewebStat(PrintlnProvider log)
    {
    this.log = log;
    }
  }
