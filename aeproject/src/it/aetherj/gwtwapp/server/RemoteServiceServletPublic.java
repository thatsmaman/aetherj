/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.server;

import javax.servlet.http.HttpServletRequest;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * There is an issue regarding the management of servlet requests.
 * RIghtly so, GWT make the resource protected since it must be obtained "fresh" from the thread that is serving the request.
 * By making it protected they are suggesting that you should not cache the request somewhere.
 * I know this, but I need to access it into a class that is created every time a new request comes in.
 */
public class RemoteServiceServletPublic extends RemoteServiceServlet
  {
  private static final long serialVersionUID = 1L;

  public HttpServletRequest getServletRequest()
    {
    return getThreadLocalRequest();
    }
  }
