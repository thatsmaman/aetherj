/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.gwtwapp.client.uict.*;

/**
 * List the popular content bound to your user
 */
public final class AecliPostListPanel extends RecliGenericCenterPanel
  {
  private static final String classname="AecliPostListPanel.";
  
  private final PostCallback srvCallback = new PostCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();

  private final TextBox searchBox;

  private final DataGrid<AegwPost> w_cellTable;
  private final ListDataProvider<AegwPost> w_cellTableData;
  private AegwPostListRes w_List;
  
  
  public AecliPostListPanel(RecliStat stat)
    {
    super(stat,"Post List","post-list-help");
    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwPost>();
    w_cellTableData.addDataDisplay(w_cellTable);    
    
    
    searchBox = newTextBox(30);
    searchBox.addKeyDownHandler(new SearchKeyDownHandler());

    addTopPanelWidget(newSearchPanel());
    setInnerCenterPanel(w_cellTable);
    
    super.setNorthPanelSize(northPanelHeight);
    }
  
  private Grid newSearchPanel ()
    {
    Grid risul = new Grid(1,4);
    
    int rowIndex=0; int colIndex=0;
    risul.setWidget(rowIndex,colIndex++,getGwtLabel("Search"));
    risul.setWidget(rowIndex,colIndex++, searchBox);
//    risul.setWidget(rowIndex,colIndex++);

    return risul;
    }
  
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
  
  private ColumnImageClick<AegwPost> newJumpColumn ()
    {
    CellImageClickHandler clickJump = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        if ( !( dataObject instanceof AegwPost)  ) return;
//        jumpToImpianto((AegwPost) dataObject);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickJump);
      
    ColumnImageClick<AegwPost> risul = new ColumnImageClick<AegwPost>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwPost contact) { return stat.imageBoundle.newForward();   }
      };

    return risul;
    }
  
  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwPost> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals("aemsg_title"))
          w_List.sortImpiantoName(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  
  private DataGrid<AegwPost> newCellTable ()
    {
    DataGrid<AegwPost> risul = new DataGrid<AegwPost>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);
            
    // ---------------------------------------------------------------
    TextColumn<AegwPost> bodyColumn = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost imp) { return imp.post_body;   }
      };

    bodyColumn.setSortable(true);
    bodyColumn.setDataStoreName("aemsg_title");
    risul.addColumn(bodyColumn, getLabel("Title"));
    
    // ---------------------------------------------------------------
    ColumnImageClick<AegwPost>ajump = newJumpColumn();
    risul.addColumn(ajump);
    risul.setColumnWidth(ajump, 20.0, Unit.PX);    
    
    // ---------------------------------------------------------------
    TextColumn<AegwPost> parentIdColumn = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost contact) { return ""+contact.post_parent_id;   }
      };

    risul.addColumn(parentIdColumn, getLabel("Parent"));
    risul.setColumnWidth(parentIdColumn, 3.0, Unit.EM);    
                
/*
    TextColumn<AegwPost> impNote = new TextColumn<AegwPost>() 
      {
      @Override
      public String getValue(AegwPost imp) { return imp.post_desc;   }
      };

    risul.addColumn(impNote,getLabel("Site Note"));
*/
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
//    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);    
    
    return risul;
  	}
	
  protected String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    }

  public void postGuiRefreshReq ()
    {
    if ( w_List == null ) return;

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
		
    }
  
private final class PostCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwPostListRes)result;

    if ( isErrorMessage(w_List)) return;
    
    parseCallEndDate(w_List);    
    postGuiRefreshReq();
    }
  }




private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }

  }


@Override
public void postServerRefreshReq()
  {
  RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
  
  int thread_id=stat.statusBar.getThreadId();
  
  AegwPostListReq req = new AegwPostListReq(stat,thread_id);
  
  service.getPostList(req, srvCallback);
  }


  }  // END MAIN CLASS
