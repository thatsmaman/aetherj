/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.guipanels;

import java.util.ArrayList;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;
import it.aetherj.gwtwapp.client.uict.*;

/**
 * List the popular content bound to your user
 */
public final class AecliBoardListPanel extends RecliGenericCenterPanel
  {
  private static final String classname="AecliPopularListPanel.";
  
  private static final String cn_name = "board_name";   // the column name in gwt
  
  private final BoardCallback srvCallback = new BoardCallback();
  private final SearchLikeTimer searchLikeTimerTimer = new SearchLikeTimer();

  private final TextBox searchBox;

  private final DataGrid<AegwBoard> w_cellTable;
  private final ListDataProvider<AegwBoard> w_cellTableData;
  private AegwBoardListRes w_List;
  
  
  public AecliBoardListPanel(RecliStat stat)
    {
    super(stat,"Boards List","board-list-help");
    
    w_cellTable = newCellTable();
    w_cellTableData = new ListDataProvider<AegwBoard>();
    w_cellTableData.addDataDisplay(w_cellTable);    
    
    searchBox = newTextBox(30);
    searchBox.addKeyDownHandler(new SearchKeyDownHandler());
    
    addTopPanelWidget(newSearchPanel());
    setInnerCenterPanel(w_cellTable);
    
    super.setNorthPanelSize(northPanelHeight);
    }
  
  private Grid newSearchPanel ()
    {
    Grid risul = new Grid(1,2);
    
    int rowIndex=0; int colIndex=0;
    risul.setWidget(rowIndex,colIndex++,getGwtLabel("Search"));
    risul.setWidget(rowIndex,colIndex++, searchBox);
    
    return risul;
    }
  
  private DockPanelLayout newSearchTablePanel ()
    {
    DockPanelLayout risul = new DockPanelLayout();

    risul.addNorth(newSearchPanel(), 30);
    risul.add(w_cellTable);

    return risul;
    }
  	
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
      
  private ColumnImageClick<AegwBoard> newJumpColumn ()
    {
    CellImageClickHandler clickJump = new CellImageClickHandler()
      {
      @Override
      public void onClick(Object dataObject, int row)
        {
        if ( !( dataObject instanceof AegwBoard)  ) return;
        
        jumpToImpianto((AegwBoard) dataObject);
        }
      };
    
    CellImageClick jumpButton = new CellImageClick(clickJump);
      
    ColumnImageClick<AegwBoard> risul = new ColumnImageClick<AegwBoard>(jumpButton) 
      {
      @Override
      public ImageResource getValue(AegwBoard contact) { return stat.imageBoundle.newForward();   }
      };

    return risul;
    }
  
  private void filterLikeSetData ()
    {
    if ( w_List == null ) return;
    
    ArrayList<AegwBoard> list = w_List.getLike(searchBox.getText());
    
    w_cellTableData.setList(list);
    }
  
  private ColumnSortEvent.Handler newColumnSortHandler ()
    {
    ColumnSortEvent.Handler risul = new ColumnSortEvent.Handler()
      {
      @Override
      public void onColumnSort(ColumnSortEvent event)
        {
        if ( w_List == null ) return;
        
        String dbcolname = event.getColumn().getDataStoreName();
        
        if ( dbcolname == null ) return;
        
        if ( dbcolname.equals(cn_name))
          w_List.sortBoardName(event.isSortAscending());
        
        // the data... see what happens
        filterLikeSetData();
        }
      };
      
    return risul;
    }
  
  private DataGrid<AegwBoard> newCellTable ()
    {
    DataGrid<AegwBoard> risul = new DataGrid<AegwBoard>(100,stat.dataGridResource);

    risul.setAutoHeaderRefreshDisabled(true);
            
    // ---------------------------------------------------------------
    TextColumn<AegwBoard> nameColumn = new TextColumn<AegwBoard>() 
      {
      @Override
      public String getValue(AegwBoard imp) { return imp.board_name;   }
      };

    nameColumn.setSortable(true);
    nameColumn.setDataStoreName(cn_name);
    risul.addColumn(nameColumn, getLabel("Board Name"));
    risul.setColumnWidth(nameColumn, 15.0, Unit.EM);
    
    // ---------------------------------------------------------------
    ColumnImageClick<AegwBoard>ajump = newJumpColumn();
    risul.addColumn(ajump);
    risul.setColumnWidth(ajump, 20.0, Unit.PX);    
    
    // ---------------------------------------------------------------
    TextColumn<AegwBoard> ownerColumn = new TextColumn<AegwBoard>() 
      {
      @Override
      public String getValue(AegwBoard contact) { return contact.board_owner_name;   }
      };

    risul.addColumn(ownerColumn, getLabel("Owner Name"));
    risul.setColumnWidth(ownerColumn, 15.0, Unit.EM);
                
    // ---------------------------------------------------------------
    TextColumn<AegwBoard> boardDesc = new TextColumn<AegwBoard>() 
      {
      @Override
      public String getValue(AegwBoard imp) { return imp.board_desc;   }
      };

    risul.addColumn(boardDesc,getLabel("Description"));
    
    risul.addColumnSortHandler(newColumnSortHandler());
    
//    risul.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);    
    
    return risul;
  	}
	
  protected String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  public void guiClear()
    {
    }
  
  
  public void jumpToImpianto ( AegwBoard board )
    {
    // set this impianto as the selected one
    stat.statusBar.setSelected(board);
    
    // ask for refresh of the Thread of this board
    stat.threadPanel.postServerRefreshReq();
    
    // in the meantime show this page
    stat.loggedinPage.setCenterPanel(stat.threadPanel);
    }


  public void postGuiRefreshReq ()
    {
    if ( w_List == null ) return;

    int rowCountData = w_List.getListSize();
    
    w_cellTable.setRowCount(rowCountData, true);    
    w_cellTable.setVisibleRange(0, rowCountData);
    filterLikeSetData();
    }
  

private final class SearchLikeTimer extends Timer
  {
  @Override
  public void run()
    {
    filterLikeSetData();
    }
  }


private final class SearchKeyDownHandler implements KeyDownHandler
  {
  @Override
  public void onKeyDown(KeyDownEvent event)
    {
    searchLikeTimerTimer.cancel();
    searchLikeTimerTimer.schedule(1000);
    }
  }


  @Override
  public void postServerRefreshReq()
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwBoardListReq req = new AegwBoardListReq(stat);
    
    service.getBoardList(req, srvCallback);
    }

private final class BoardCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    w_List = (AegwBoardListRes)result;
  
    if ( isErrorMessage(w_List)) return;
    
    parseCallEndDate(w_List);    
    postGuiRefreshReq();
    }
  }


  }  // END MAIN CLASS
