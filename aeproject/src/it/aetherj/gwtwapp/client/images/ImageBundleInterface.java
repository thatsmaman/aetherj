/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.images; 

import com.google.gwt.resources.client.*;


/**
 * You can simply use return new Image (stat.imageBoundle.btn_chart_line()); to create a new image
 */
public interface ImageBundleInterface extends ClientBundle
   {
   @Source ("edit.gif")
   public abstract ImageResource newEdit();
   
   @Source ("online.gif")
   public abstract ImageResource newOnline();
   
   @Source ("offline.gif")
   public abstract ImageResource newOffline();

   @Source ("info.png")
   public abstract ImageResource newInfo();

   @Source ("pagehelp.png")
   public abstract ImageResource newHelp();

   @Source ("back.gif")
   public abstract ImageResource newBack();

   @Source ("forward.gif")
   public abstract ImageResource newForward();
  
   @Source ("logoff.gif")
   public abstract ImageResource newLogoff();
   
   @Source ("refresh.png")
   public abstract ImageResource newRefresh();

   @Source ("save.png")
   public abstract ImageResource newSave();

   }
