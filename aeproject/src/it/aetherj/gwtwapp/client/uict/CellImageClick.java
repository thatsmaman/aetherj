/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.uict;

import com.google.gwt.cell.client.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.*;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safecss.shared.SafeStyles;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.*;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

/**
 * A Cell that shows an image and accept to be click
 */
public class CellImageClick extends AbstractCell<ImageResource> 
   {
   private final CellImageClickHandler clickHandler;
   
   public CellImageClick() 
    	{
      /*
       * Sink the click and keydown events. We handle click events in this
       * class. AbstractCell will handle the keydown event and call
       * onEnterKeyDown() if the user presses the enter key while the cell is
       * selected.
       */
      super("click", "keydown");

      this.clickHandler = null;
    	}

   public CellImageClick(CellImageClickHandler clickHandler) 
     {
     super("click", "keydown");

     this.clickHandler = clickHandler;
     }
    
    public interface TemplateCic extends SafeHtmlTemplates 
    	{
    	@Template("<div class=\"cellImageClick\">{0}</div>")
      SafeHtml cell(SafeHtml value);
    	}

    private static TemplateCic template = GWT.create(TemplateCic.class);    
    
     public interface TemplateStyle extends SafeHtmlTemplates 
      {
      @Template("<div class=\"cellImageClick\" style=\"{0}\">{1}</div>")
      SafeHtml cell(SafeStyles styles, SafeHtml value);
      }

    private static TemplateStyle templateStyle = GWT.create(TemplateStyle.class);    

    
    private boolean isClick ( NativeEvent event )
    	{
    	if ( event == null ) return false;
    	
    	String type = event.getType();
    	
    	if ( type == null ) return false;
    	
    	return type.equals("click");
    	}

    /**
     * Called when an event occurs in a rendered instance of this Cell. The
     * parent element refers to the element that contains the rendered cell, NOT
     * to the outermost element that the Cell rendered.
     */
    @Override
    public void onBrowserEvent(Context context, Element parent, ImageResource value, NativeEvent event, ValueUpdater<ImageResource> valueUpdater) 
    	{
      // Let AbstractCell handle the keydown event.
      super.onBrowserEvent(context, parent, value, event, valueUpdater);

      // Handle the click event.
      if ( ! isClick(event) ) return;
      
		  // Ignore clicks that occur outside of the outermost element.
		  EventTarget eventTarget = event.getEventTarget();
			
		  if (parent.getFirstChildElement().isOrHasChild(Element.as(eventTarget))) doAction(context);
    	}


    @Override
    public void render(Context context, ImageResource value, SafeHtmlBuilder sb) {
      /*
       * Always do a null check on the value. Cell widgets can pass null to
       * cells if the underlying data contains a null, or if the data arrives
       * out of order.
       */
      if (value == null) return;

      // If the value comes from the user, we escape it to avoid XSS attacks.
      SafeHtml safeValue = AbstractImagePrototype.create(value).getSafeHtml();

      SafeHtml rendered = template.cell(safeValue);
      
      sb.append(rendered);
    	}

    /**
     * onEnterKeyDown is called when the user presses the ENTER key will the
     * Cell is selected. You are not required to override this method, but its a
     * common convention that allows your cell to respond to key events.
     */
    @Override
    protected void onEnterKeyDown(Context context, Element parent, ImageResource value, NativeEvent event, ValueUpdater<ImageResource> valueUpdater) 
    	{
    	doAction(context);
    	}


    private void doAction(Context context) 
    	{
    	if ( clickHandler == null )
    	   {
         // Alert the user that they selected a value.
         Window.alert("doAction col=" + context.getColumn());
         return;
    	   }
    	
    	Object anobj = context.getKey();
    	
    	if ( anobj == null ) return;
    	
    	clickHandler.onClick(anobj, context.getColumn() );
    	}
	 }

