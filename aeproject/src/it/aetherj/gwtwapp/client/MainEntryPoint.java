/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client;

import com.google.gwt.core.client.*;

import it.aetherj.gwtwapp.client.images.ImageBundleInterface;
import it.aetherj.gwtwapp.client.services.RewebServiceFactory;
import it.aetherj.gwtwapp.client.uict.DataGridResourceCss;
import it.aetherj.gwtwapp.client.varie.*;
import it.aetherj.protocol.MimPowCalculator;


public final class MainEntryPoint implements EntryPoint
  {
  private RecliStat stat;  // never reassign it !

  /** 
   * The entry point method, called automatically by loading a module 
   * that declares an implementing class as an entry-point
   */
  public void onModuleLoad()
    {
    // MUST be the first one
    stat = new RecliStat();

		// this is a simple message to verify that the root logger works OK
		stat.logOnBrowser("RootLogger: OK");

    stat.dataGridResource  = GWT.create(DataGridResourceCss.class);
    stat.imageBoundle      = GWT.create(ImageBundleInterface.class);
    
    stat.serviceFactory = new RewebServiceFactory(stat); 
    stat.utils          = new RecliUtils(stat);
    
    
    try
      {
      stat.mimPow = new MimPowCalculator();
      stat.logOnBrowser("MimPowCalculator: created");
      
      /*   
           byte [] t1 = stat.mimPow.digest("ciao");
      
      stat.logOnBrowser("t1.len:"+t1.length);
      
      stat.logOnBrowser("Seed="+stat.mimPow.newRandomSeed());
      
      String risul = stat.mimPow.newPowUnsigned("prova",10,20);
      
      stat.logOnBrowser("risul="+risul);
*/   

      }
    catch ( Exception exc )
      {
      stat.logOnBrowser("MimPowCalculator: "+exc);
      }
    
    RecliDictionaryPage dictionaryPage = new RecliDictionaryPage(stat);
    stat.setRootCanvas(dictionaryPage);
		}
	}
