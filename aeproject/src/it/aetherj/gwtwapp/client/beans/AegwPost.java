/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;

import com.google.gwt.regexp.shared.RegExp;

/**
 * A Post
 */
public class AegwPost implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public Integer   post_id;           // used when I am saving the translation
  public Integer   post_thread_id;    // 
  public Integer   post_owner_id;
  public Integer   post_parent_id;    // to do a tree, parent id of the given post
  public String    post_body;
  public String    post_desc;         // can be null to indicate that the value is label_english
  
  public volatile boolean isUpdated;    // used in GUI to know if a content has changed
  
  public boolean like ( RegExp expr )
    {
    if ( expr.test(post_body)) return true;
    
    if ( expr.test(post_desc)) return true;
    
    return false;
    }

  
  public final String toString()
    {
    return "id="+post_id+" desc="+post_desc;
    }
  }
