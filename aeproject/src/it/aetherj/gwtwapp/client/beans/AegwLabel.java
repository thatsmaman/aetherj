/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * This is a label that should be updated/saved into the multilang dictionary in the server.
 */
public class AegwLabel implements Serializable
  {
  private static final long serialVersionUID = 1L;
  
  public Integer   label_id;           // used when I am saving the translation
  public String    label_english;
  public String    label_from_page;
  public String    label_value;         // can be null to indicate that the value is label_english
  public Timestamp label_last_day_use;  // when was the label last used
  public String    label_used_in_pages; // used to manage label translation
  
  public volatile boolean isUpdated;    // used in GUI to know if a content has changed
  
  public final String toString()
    {
    return "id="+label_id+" eng="+label_english+" val="+label_value;
    }
  }
