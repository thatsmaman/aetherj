/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

import java.util.HashMap;

public final class AegwDictionary extends ServiceFeedback
  {
  private static final long serialVersionUID = 1L;
  
  public String  dbaseVersion;              // I may as well return the values here
  public String  backendVersion;            // I may as well return the values here
  public String  client_locale;             // basically it is the locale eg: it-IT or hu-HU amd so on just for diaplay purposes !!!
  public HashMap<String,String> labelMap;

  public AegwDictionary()
    {
    labelMap = new HashMap<String,String>(2000);
    }
  
  public void putLabel ( String label_english, String label_locale )
    {
    labelMap.put(label_english, label_locale );      
    }
  
  public String getLabel ( String label_english )
    {
    return labelMap.get(label_english);      
    }

  @Override
  public String toString ()
    {
    return "Feedback["+super.toString()+"] locale="+client_locale+" map size="+labelMap.size();  
    }
  }
