/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.beans;

public class AegwLoginRes extends ServiceFeedback 
  {
  private static final long serialVersionUID = 1L;
  
  public WebSession webSession;
  public String     user_login;          // the "login"
  public String     user_name;           // Cognome and nome of the loggedin user

	public int     ceana_idx;            
	
	public String toString ()
		{
		StringBuilder risul = new StringBuilder(500);
		risul.append(webSession.toString());
		risul.append(" ceana_idx="+ceana_idx+" user_login="+user_login+" user_name="+user_name);
		
		return risul.toString();
		}
	
	}
