/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.varie;

import java.util.*;

import com.google.gwt.user.client.Timer;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync;
import it.aetherj.gwtwapp.client.ui.RecliGenericCallback;

/**
 * This is used to commit labels info to dbase once every so often the idea is that this is called to record the usage of a given label/page
 * It creates a HashMap of Label,Page and if a key is not preset it will add it to a queue of labels to commit.
 */
public final class DictionaryAssistant
  {
  public static final String classname="DictionaryAssistant.";
  
  private final RecliStat stat; 
  private final HashMap<String,String>mappedMap = new HashMap<String,String>(3000);
  private final LabelSaveCallback labelSaveCallback = new LabelSaveCallback();
  private final LabelSaveTimer labelSaveTimer = new LabelSaveTimer();

  private ArrayList<AegwLabel>queuedLabels = new ArrayList<AegwLabel>(100);
  
  public DictionaryAssistant(RecliStat stat)
    {
    this.stat = stat;      

    labelSaveTimer.schedule(10000); 
    }
  
  public void addLabel ( String label_english, String from_class )
    {
    String key = label_english+from_class;
    
    // if this label is already added to the map of mapped labels then there is nothing to do
    if ( mappedMap.containsKey(key) ) return;
    
    // add the key to the map values, thre is really no value to save
    mappedMap.put(key,null);
    
    AegwLabel label      = new AegwLabel();
    label.label_english   = label_english;
    label.label_from_page = "/Reweb/"+from_class;     // add a prefix to differentiate from Jrnc
    label.label_value     = null;
    queuedLabels.add(label);
    
    if ( queuedLabels.size() > 90 ) saveCurrentLabelsRequest();
    }
  
  private boolean saveCurrentLabelsRequest ()
    {
    // test if nothing to do at the moment
    if ( queuedLabels == null || queuedLabels.size() < 1 ) return false;
    
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    AegwLabelSaveReq req = new AegwLabelSaveReq(stat);
    req.isSaveLabelValueRequest = false;
    req.labelList = queuedLabels;
    service.saveLabelList(req, labelSaveCallback);

    // now detach the previous array list from the current class
    queuedLabels = new ArrayList<AegwLabel>(100);
    
    return true;
    }
  
private final class LabelSaveTimer extends Timer
  {
  public void run()
    {
    if ( stat.login_res == null || stat.login_res.user_name == null )
      {
      // thre is no login info, just reschedule the timer in 10 seconds time
      labelSaveTimer.schedule(10000); 
      return;
      }


    // if there is nothing to save, it is my duty to schedule an event for later.
    if ( ! saveCurrentLabelsRequest () )  labelSaveTimer.schedule(10000); 
      
    }
  }

private final class LabelSaveCallback  extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwLabelSaveRes conn_status = (AegwLabelSaveRes)result;

    if ( isErrorMessage(conn_status)) return;
    
//    GWT.log(classname+"SAVED",null);
    
    // I could slow things down if nothing is happening...
    labelSaveTimer.schedule(10000); 
    }
  }
  
  
  }
