/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.services;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import it.aetherj.gwtwapp.client.RecliStat;

/**
 * Nothing special, just some wrapper to avoid writing the same code over and over.
 */
public final class RewebServiceFactory
  {
	private final RecliStat stat;
		
  public static final String dbk_primary_id = "primary_id";
  
	public RewebServiceFactory ( RecliStat stat )
		{
		this.stat = stat;			
		}
	  
  public String getPageHelpServletUrl()
    {
    return GWT.getModuleBaseURL()+"servlets/getPageHelp";
    }

  public String getPagehelpUrl ()
    {
    return GWT.getModuleBaseURL()+"servlets/getPageHelp";
    }
  
  public String getPagehelpUrl (String pagehelp_key)
    {
    return getPagehelpUrl()+"#"+pagehelp_key;
    }

  
  public LoginServiceAsync newLoginService()
    {
    LoginServiceAsync service = (LoginServiceAsync) GWT.create(LoginService.class);
    ServiceDefTarget endpoint = (ServiceDefTarget) service;
    String moduleRelativeURL = GWT.getModuleBaseURL() + "services/LoginService";
    endpoint.setServiceEntryPoint(moduleRelativeURL);
    return service;
    }
  
    
  public RewebSystemServiceAsync newSystemService ()
    {
    RewebSystemServiceAsync service = (RewebSystemServiceAsync)GWT.create(RewebSystemService.class);
    ServiceDefTarget endpoint = (ServiceDefTarget)service;
    String moduleRelativeURL = GWT.getModuleBaseURL() + "services/SystemService";
    endpoint.setServiceEntryPoint(moduleRelativeURL);    
    return service;
    }
  
  
  
  }
