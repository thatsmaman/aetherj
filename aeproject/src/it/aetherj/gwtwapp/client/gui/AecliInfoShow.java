/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.*;
import it.aetherj.gwtwapp.client.beans.AegwLoginRes;
import it.aetherj.gwtwapp.client.ui.*;

public final class AecliInfoShow extends RecliGenericCenterPanel
  {
  private static final String classname="AecliInfoShow.";
  
  private final Label backendVersion;
  private final Label dbaseVersion;
  private final Label gwcliVersion;
  private final Label webUserName;  
  private final Label webUserLogin; 
  
  
  public AecliInfoShow(RecliStat stat)
    {
    super(stat,"System Information","system_info-help");

    backendVersion = new Label();
    dbaseVersion   = new Label();
    webUserName    = new Label();
    webUserLogin   = new Label();
    gwcliVersion   = new Label();

    setInnerCenterPanel(newWorkPanel());
    }

  public void refreshSysinfo (AegwLoginRes res)
    {
    if ( res != null )
      {
      webUserName.setText(res.user_name);
      webUserLogin.setText(res.user_login);
      }
    
    if ( stat.dictionary != null )
      {
      backendVersion.setText(stat.dictionary.backendVersion);
      dbaseVersion.setText(stat.dictionary.dbaseVersion);
      }
    
    gwcliVersion.setText(GwtwappVersion.VERSION);
    }

  private Widget newWorkPanel ()
    {
    FlexTableEditable risul = new FlexTableEditable(this);
    
    int rowIndex=0; int colIndex=0;
    
    risul.setLabelCell(rowIndex,colIndex++,"Web User Name");
    risul.setWidget(rowIndex,colIndex++,webUserName);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Web User Login");
    risul.setWidget(rowIndex,colIndex++,webUserLogin);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Backend Version");
    risul.setWidget(rowIndex,colIndex++,backendVersion);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"GWT Client Version");
    risul.setWidget(rowIndex,colIndex++,gwcliVersion);

    rowIndex++; colIndex=0;
    risul.setLabelCell(rowIndex,colIndex++,"Dbase Version");
    risul.setWidget(rowIndex,colIndex++,dbaseVersion);

    return risul;
    }
  
  
  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }
  
  protected String getLabel(String input)
    {
    return stat.getLabel(input,classname); 
    }


  public void guiClear()
    {
    }

  public void postGuiRefreshReq ()
    {
    }
  
  public void postServerRefreshReq()
    {
    }

  }
