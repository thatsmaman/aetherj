/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.gui;

import java.util.Date;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.LoginServiceAsync;
import it.aetherj.gwtwapp.client.ui.*;

/**
 * This holds the two bars at the top of the page.
 * One of the BIG issues is to align things properly....
 * It seems that the only safe way is use Grid, as much as possible....
 */
public final class AecliStatusBar extends AecliGenericRootPage
  {
  private static final String classname="AecliStatusBar.";
  
  public static final int BAR_height_px = 55;

  private final ButtonsListener buttonsListener = new ButtonsListener();
  
  private final Label   boardNameLabel;
  private final Label   threadTitleLabel;
  private final TextBox messageLabel;         // holds the message on activity
  private final TextBox errorLabel; 
  private final Label   timestampLabel; 
  
  private final Timer connectionCheckTimer;
  private final ConnectionCheckCallback connectionCheckCallback = new ConnectionCheckCallback();
  private final DateTimeFormat activityTimeFormat = DateTimeFormat.getFormat("H:mm:ss");

  private final PushButton refresh_btn;
  private final PushButton info_btn,logoff_btn;

  private final Panel workPanel;
  
  private AegwBoard aegwBoard;
  private AegwThread aegwThread;
  
  /**
   * Constructor
   * @param stat
   */
  public AecliStatusBar(RecliStat stat)
    {
    super(stat);

    boardNameLabel = newGwtLabelBold();
    threadTitleLabel = newGwtLabelBold();
    messageLabel = new TextBox();
    messageLabel.setWidth("15em");
    
    errorLabel = new TextBox();
    errorLabel.setWidth("8em");
    
    timestampLabel = new Label();
    
    refresh_btn    = stat.utils.newRefreshButton(buttonsListener);
    info_btn       = stat.utils.newInfoButton(buttonsListener);
    logoff_btn     = stat.utils.newLogoffButton(buttonsListener);
    
    workPanel = newWorkPanel();
    
    connectionCheckTimer = new ConnectionCheckTimer();
    connectionCheckTimer.schedule(1000); // in millisec, following schedule will happen when the message is Rx
    }

  protected String getLabel (String input )
    {
    return stat.getLabel(input,classname); 
    }

  @Override
  public Image newPageButtonImage ()
    {
    return null;
    }

  private void refresh ()
    {
    boardNameLabel.setText(getBoardName());
    threadTitleLabel.setText(getThreadTitle());
    }
  
  private String getBoardName ()
    {
    return aegwBoard != null ? aegwBoard.board_name : "";
    }
  
  public int getBoardId ()
    {
    return aegwBoard != null ? aegwBoard.board_id : 0;
    }
  
  private String getThreadTitle ()
    {
    return aegwThread != null ? aegwThread.thread_name : "";
    }
  
  public int getThreadId ()
    {
    return aegwThread != null ? aegwThread.thread_id : 0;
    }
  
  
  
  public void setRefreshButtonVisible ( boolean visible )
    {
    refresh_btn.setVisible(visible);
    }
    
  private Panel newWorkPanel()
    {
    Grid risul = new Grid(1,9);
    risul.addStyleName("connectionStatusPanel");

    int rowIndex=0, colIndex=0;
    risul.setWidget(rowIndex,colIndex++,refresh_btn);
    risul.setWidget(rowIndex,colIndex++,getSpace("1em"));
    risul.setWidget(rowIndex,colIndex++,boardNameLabel);
    risul.setWidget(rowIndex,colIndex++,newGwtLabelBold(" , "));
    risul.setWidget(rowIndex,colIndex++,threadTitleLabel);
    risul.setWidget(rowIndex,colIndex++,newActivityItem());
    risul.setWidget(rowIndex,colIndex++,newActivityErrorItem());
    risul.setWidget(rowIndex,colIndex++,info_btn);
    risul.setWidget(rowIndex,colIndex++,logoff_btn);

    return risul;
    }

  /* this does NOT work
  private Panel newWorkPanel()
    {
    FlowPanel risul = new FlowPanel();
    risul.addStyleName("connectionStatusPanel");

    risul.add(refresh_btn);
    risul.add(getSpace("1em"));
    risul.add(boardNameLabel);
    risul.add(newGwtLabelBold(" , "));
    risul.add(threadTitleLabel);
    risul.add(newActivityItem());
    risul.add(newActivityErrorItem());
    risul.add(info_btn);
    risul.add(logoff_btn);

    return risul;
    }
*/
  
  public void setSelected ( AegwBoard board )
    {
    aegwBoard=board;
    aegwThread=null;
    
    refresh();
    }

  public void setSelected ( AegwThread thread )
    {
    aegwThread=thread;
    
    refresh();
    }

  private Grid newActivityErrorItem()
    {
		Grid risul = new Grid(1,2);

    risul.setWidget(0,0,newGwtLabelBold("Error"));
    risul.setWidget(0,1,errorLabel);
    
    return risul;
    }

  private Grid newActivityItem()
    {
		Grid risul = new Grid(1,4);

    risul.setWidget(0,0,newGwtLabelBold("Message"));
    risul.setWidget(0,1,messageLabel);
    // this should put some padding between the two
    risul.setWidget(0,2,getSpace("1em"));
		risul.setWidget(0,3,timestampLabel);

    return risul;
    }
  
  public Panel getPanelToDisplay()
    {
    return workPanel;
    }

  private String getActivityTime ( Date time )
    {
    if ( time == null ) return "";
    
    return activityTimeFormat.format(time);
    }
  

  private void logoff_fun ()
    {
    // Clear previous login information
    stat.loginPage.clearForm();
    
    // show login page
    stat.setRootCanvas(stat.loginPage);
    }

  
private final class ButtonsListener implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    Object source = event.getSource();
    
    if ( source == logoff_btn ) 
      logoff_fun ();
    
    if ( source == info_btn ) 
      stat.loggedinPage.setCenterPanel(stat.sysinfoPage);
    }
  }


	/**
	 * if a loggedinpage is defined returns the  ident that we are currently in so I can ask for status info
	 * @return
	 */
	private String getActivityGwtIdent()
		{
		AecliLoggedinPage alo = stat.loggedinPage; 			

		if ( alo == null ) return null;
		
		RecliGenericCenterPanel apro = alo.getCurrentCenterPage();
		
		if ( apro == null ) return null;
		
//		stat.logOnBrowser("getActivityGwtIdent: apro.class="+apro.getClass().getName());
		
		return apro.getGwtIdent();
		}

	/**
	 * let me see if the current page reflects the returned info, if any
	 * If so then ask for refresh if the returned modification date is later than the stored date
	 * TODO it should be noted that the info should go in the page.... so I probably have to extend root page...
	 * @param res
	 */
  private void parseActivityStatusRes ( AegwActivityStatus  res )
  	{
  	// nothing to do if there is no res
  	if ( res == null ) return;	
  		
  	String gwtIdent_in = res.gwtIdent;
  	
//  	stat.logOnBrowser(classname+"parseActivityStatusRes: res="+res);
  	
  	// also if there is no ident...
  	if ( gwtIdent_in == null ) return;
  		
		AecliLoggedinPage alo = stat.loggedinPage; 			

		// nothing to do if there is no logged in page (maybe possible ?)
		if ( alo == null ) return;
		
		RecliGenericCenterPanel apro = alo.getCurrentCenterPage();
  	
		// no provider for root page ?
		if ( apro == null ) return;
  
		String want_gwtIdent = apro.getGwtIdent();
		
		if ( want_gwtIdent == null )
			{
			// this is generally strange, I did a request for a gwt ident and now I do not have it anymore ???
			stat.logOnBrowser(classname+"parseActivityStatusRes: res="+res+" but want_gwtIdent==null");
			return;
			}
			
		// if the returned ident does not belong to the current page
		if ( ! gwtIdent_in.equals(want_gwtIdent)) return;
  		
//    stat.logOnBrowser(classname+"parseActivityStatusRes: res="+res+" match apro");
    
		// the given activity date is no later than the one stored 
		if ( ! apro.isActivityDateLater(res.updateDate)) return;
  		
		// ask for gui to refresh
    apro.postServerRefreshReq();
  	}
	
private final class ConnectionCheckTimer extends Timer
  {
  public void run()
    {
    LoginServiceAsync service = stat.serviceFactory.newLoginService();
    ConnectionStatusReq req = new ConnectionStatusReq(stat);
    
    req.setActivity( getActivityGwtIdent());
    
    service.getConnectionStatus(req, connectionCheckCallback);
    }
  }

private final class ConnectionCheckCallback  extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    AegwbConnectionStatus conn_status = (AegwbConnectionStatus)result;

    if ( isErrorMessage(conn_status)) return;

    messageLabel.setText(conn_status.imp_msg);   
    errorLabel.setText(conn_status.imp_msg_error);
    timestampLabel.setText(getActivityTime(conn_status.imp_msg_date));
    		
		parseActivityStatusRes ( conn_status.activityStatus);
		
    // normally reschedule in two seconds
    int rescheduleMilliseconds = 2000;
        
    // I could slow things down if nothing is happening...
    connectionCheckTimer.schedule(rescheduleMilliseconds); 
    }
  }


  }  // END MAIN CLASS
