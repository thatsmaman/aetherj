/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.user.client.ui.*;

public class IconButton extends PushButton
  {
  public IconButton()
    {
    super();      
    }
  
  public IconButton(Image icon)
    {
    super(icon);
    }
   
  public void setIcon ( Image icon )
    {
    Face face = getUpFace();
    
    // should never be not null, but who knows
    if ( face != null ) face.setImage(icon);
    

/*
 * for GWT older
 *     
    // make sure that any previous association of icon is gone.
    icon.removeFromParent();

    // get the element of this button
    Element element = getElement();

    // can this happen ???
    if ( element == null ) return;
    
    // get the icon that was associated with this element, if any
    Node oldIcon = element.getFirstChild();
    if ( oldIcon == null )
      element.appendChild( icon.getElement());
    else
      element.replaceChild(icon.getElement(),oldIcon);
*/      
    }
  }
