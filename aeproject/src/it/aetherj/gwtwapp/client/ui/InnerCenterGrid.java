/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

/**
 * A Grid with better support
 */
public class InnerCenterGrid extends Grid
  {
  private static final String classname="InnerCenterGrid.";

  public static final String CSS_width_100 = "100%";
  public static final String CSS_width_auto = "";
  
  private final AecliGenericRootPage parent;
  private final RowFormatter rowFormatter;
  private final ColumnFormatter colFormatter;
  private final CellFormatter cellFormatter;

  
  public InnerCenterGrid(AecliGenericRootPage parent, int colCount, String css_width )
    {
    super(1,colCount);

    this.parent = parent;
    
    addStyleName("grid-table");  // a grid is a table, but tables are used quite a lot, so you have to add a dependend CSS
    
    if ( css_width != null && css_width.length() > 1 ) setWidth(css_width);
    
    rowFormatter  = getRowFormatter();
    colFormatter  = getColumnFormatter();
    cellFormatter = getCellFormatter();
      
    rowFormatter.addStyleName(0,"grid-table-header");    
    }
  
  /**
   * Thre is really very little you can use
   * See: http://www.quirksmode.org/css/columns.html
   * @param col
   * @param style_name
   */
  public void addColStyle ( int col, String style_name )
    {
    colFormatter.addStyleName(col,style_name);      
    }
  
  /**
   * Clear the content
   */
  public void clearData ()
    {
    resizeRows(1);      
    }
  
  public void setHeading ( int column, String text )
    {
    // translate text, if there is something ot translate
    if ( text != null && text.length() > 0 ) text = parent.getLabel(text);
    
    super.setText(0,column,text);      
    }
  
  public void setHeading ( int column, Widget widget )
    {
    super.setWidget(0,column,widget);      
    }

  public void setBoolean (int row, int column, Boolean value, BooleanImageProvider images )
    {
    Image icon = images.getImage(value);
    setWidget(row,column,icon);
    }
  
  public CheckBox setBoolean (int row, int column, Boolean value )
    {
    CheckBox risul = new CheckBox();

    if ( value != null ) risul.setValue(value);

    setWidget(row,column,risul);

    return risul;
    }
  
  public void setDouble ( int row, int column, Double value )
    {
    setText(row,column,parent.formatDouble(value));
    }

  public void setDouble ( int row, int column, Double value, String css_style )
    {
    cellFormatter.addStyleName(row+1,column,css_style);
    setDouble(row,column,value);
    }
  
  public void setTemperature ( int row, int column, Double value )
    {
    setText(row,column,parent.formatTemperature(value));
    }

  public void setTemperature ( int row, int column, Double value, String css_style )
    {
    cellFormatter.addStyleName(row+1,column,css_style);
    setTemperature(row,column,value);
    }

	public void removeDataRow ( int data_row )
		{
		removeRow(data_row+1);		
		}
	
	
	public void addDataRow ()
		{
		int rows = getRowCount();
		
		super.resizeRows(rows+1);
		}
	
  public int getRowCountData ()
    {
    int rowCount = getRowCount();

    if ( rowCount > 1 ) return rowCount-1;
    
    return 0;
    }
  
  public void setRowCountData ( int row_data )
    {
    super.resizeRows(row_data+1);    
    }
  
  public void setInteger ( int row, int column, Integer value )
    {
    if ( value == null  )
      {
      setText(row,column,"");        
      return;
      }
    
    setText(row,column,value.toString());
    }
  
  public void setInteger ( int row, int column, Integer value, String css_style )
    {
    cellFormatter.addStyleName(row+1,column,css_style);
    setInteger(row,column, value );
    }

  /**
	 * Sets the current in the table assuming that row zero is the first row for current data
	 * If you say row zero, it will be automatically added to row one, since row zero is left for heading.
	 * It follows that the ONLY way to set the heading is to use setHeading
	 * @param row
	 * @param column
	 * @param text
	 */
  public void setText(int row, int column, String text )
    {
    int grid_row = row + 1;
    
    if ( text == null ) text = "";
    
    super.setText(grid_row,column,text);

/*
    if ( (grid_row % 2) != 0 )
      rowFormatter.addStyleName(grid_row,"grid-table-row-odd");
    else
      rowFormatter.addStyleName(grid_row,"grid-table-row-even");
*/
    }
  
	
	/**
	 * add teh default row style for this component
	 * @param row
	 */
	public void setRowStyle ( int row )
		{
    if ( (row % 2) != 0 )
      setRowStyle(row,"grid-table-row-odd");
    else
      setRowStyle(row,"grid-table-row-even");
		}
	
	public void setRowStyle ( int row, String css_style )
		{
    int grid_row = row + 1;
    
    if ( css_style == null ) return;
    
		rowFormatter.setStyleName(grid_row,css_style);
		}
	
  public void setImageButton ( int row, int column, Image icon, ClickHandler handler )
    {
    if ( icon == null ) return;
    
    if ( handler != null ) icon.addClickHandler(handler);

    setWidget(row,column,icon);
    }
  
  public void setImage ( int row, int column, Image icon )
    {
    if ( icon == null ) return;

    setWidget(row,column,icon);
    }

  public void setWidget(int row, int column, Widget widget )
    {
    super.setWidget(row+1,column,widget);      
    }

  public Widget getWidget(int row, int column )
    {
    return super.getWidget(row+1,column);      
    }
  
  public void setEditableString ( int row, int column, String current_value )
    {
    TextBox input = new TextBox();
    input.setText(current_value);
    setWidget(row,column,input);  
    }
  
  /**
   * Tries to get the data at the given row/col (in user data) as a string
   * @param row
   * @param column
   * @return
   */
  public String getString ( int row, int column )
    {
    Widget widget = getWidget(row,column);

    if ( widget == null ) return null;

    if ( widget instanceof TextBox )
      {
      TextBox item = (TextBox)widget;
      return item.getValue();
      }
    
    if ( widget instanceof PasswordTextBox )
      {
      PasswordTextBox item = (PasswordTextBox)widget;
      return item.getValue();
      }
    
    Window.alert(classname+"getString: unknown widget class="+widget.getClass());
    return null;
    }

  public Boolean getBoolean ( int row, int column )
    {
    Widget widget = getWidget(row,column);

    if ( widget == null ) return null;

    if ( widget instanceof CheckBox )
      {
      CheckBox item = (CheckBox)widget;
      return item.getValue();
      }

    Window.alert(classname+"getBoolean: unknown widget class="+widget.getClass());
    return null;
    }

  public Integer getInteger ( int row, int column )
    {
    Widget widget = getWidget(row,column);

    if ( widget == null ) return null;

    if ( widget instanceof ComboBox )
      {
      ComboBox item = (ComboBox)widget;
      return item.getSelectedIntegerKey();
      }

    if ( widget instanceof ListBox )
      {
      ListBox item = (ListBox)widget;
      return Integer.valueOf(item.getSelectedIndex());
      }

    Window.alert(classname+"getInteger: unknown widget class="+widget.getClass());
    return null;
    }




  }

