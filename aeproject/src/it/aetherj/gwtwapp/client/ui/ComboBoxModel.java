/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

/**
 * This class will hold the data for the combo boxes it is attached to
 */
public class ComboBoxModel
  {
  private static final String classname="ComboBoxModel.";
  
  private final ArrayList<ComboBox>comboBoxList;
  private final ArrayList<ComboBoxKeyValue>keyValueList;
  
  /**
   * Constructor
   */
  public ComboBoxModel()
    {
    comboBoxList = new ArrayList<ComboBox>();
    keyValueList = new ArrayList<ComboBoxKeyValue>();
    }

  /**
   * If you want that a combo is updated when a new dataset arrives you
   * have to register with the model.
   * Actually it is the combo that register itself.
   * @param comboBox
   */
  void registerComboBox ( ComboBox comboBox ) 
    {
    if ( comboBox == null ) 
      {
      // this should really never happen
      Window.alert(classname+"registerComboBox: ERROR comboBox==null");
      return;
      }
    
    int listSize = comboBoxList.size();
    if ( listSize > 200 )
      {
      // this should really never happen
      Window.alert(classname+"registerComboBox: WARNING comboBox.size()="+listSize);
      // but I go on so I can finish whatever I am doing
      }
    
    if ( comboBoxList.contains(comboBox) )
      {
      // this should really never happen
      Window.alert(classname+"registerComboBox: NOTICE already contains="+comboBox);
      return;
      }
    
    comboBoxList.add(comboBox);
    }
  
  /**
   * If you ever garbage collect the combo you MUST deregister from the model !!!
   * I can add it to finalize but it is a bit riscy
   * @param comboBox
   * @return
   */
  boolean removeComboBox (ComboBox comboBox )
    {
    if ( comboBox == null ) return false;
    
    GWT.log(classname+"removeComboBox: "+comboBox,null);
    
    return comboBoxList.remove(comboBox);
    }
  
  public void addKeyValue (int key, String value)
    {
    addKeyValue(Integer.valueOf(key),value);
    }  
  
  public void addKeyValue (Object key, String value)
    {
    if ( value == null ) return;
    
    GenericKeyValue item = new GenericKeyValue(key,value);
    
    keyValueList.add(item);      
    }

	/**
	 * You MUST call refreshCombo once you have done
	 * @param keyValue
	 */
	public void addKeyValue ( ComboBoxKeyValue keyValue )
		{
		if ( keyValue == null ) return;
		
    keyValueList.add(keyValue);      
		}
	
	/**
	 * @param keyValueList
	 */
  public void setKeyValueList ( ArrayList<? extends ComboBoxKeyValue> keyValueList )
    {
		if ( keyValueList == null ) return;

    clearKeyValueList();
    addKeyValue(null,"");
    this.keyValueList.addAll(keyValueList);
    }
  

	/**
	 * This ADD the given list to the model, you MUST clear the list first if you need to and you MUST
	 * call refreshCombo once you have done
	 * @param keyValueList
	 */
	public void addKeyValueList ( ArrayList<? extends ComboBoxKeyValue> keyValueList )
		{
		if ( keyValueList == null ) return;
		
    this.keyValueList.addAll(keyValueList);
		}

  
  /**
   * You can use this one to clear up the model before adding items manually.
   * Remeber to refresh the combo at the end.
   */
  public void clearKeyValueList()
    {
    keyValueList.clear();
    }
  
  
  /**
   * Once you have done messing the data model you can ask the combo to refresh
   * Do not refresh at each insert, it will get quite flashy
   */
  public void refreshCombo ()
    {
    // now I need to go to all the combo boxes and refresh the content
    for ( int index=0; index<comboBoxList.size(); index++)
      {
      ComboBox item = comboBoxList.get(index);
      item.fillCombo();
      }
    }

  /**
   * Attempt to find, if any, the index associated to the null key
   * @return -1 if there is no null key available
   */
  int getNullKeyIndex ()
    {
    int items_count = getKeyValueCount();
    for (int index=0; index<items_count; index++)
      {
      ComboBoxKeyValue value = getKeyValue(index);
      if ( value.getComboKey() == null ) return index;
      }
      
    return -1;
    }

  /**
   * Returns the true key index 0 onward if it finds the key match, otherwise -1
   * @param key
   * @return
   */
  int getKeyIndex ( Object key )
    {
    // I know, see the setKeyValueList that the first element is the null one.
    if ( key == null ) return getNullKeyIndex();
    
    int items_count = getKeyValueCount();
    for (int index=0; index<items_count; index++)
      {
      ComboBoxKeyValue value = getKeyValue(index);
      if ( key.equals(value.getComboKey())) return index;
      }
    
    return -1;
    }
  
  public ComboBoxKeyValue getValueFromKey ( Object key )
    {
    if ( key == null ) return null;
    
    int items_count = getKeyValueCount();
    for (int index=0; index<items_count; index++)
      {
      ComboBoxKeyValue value = getKeyValue(index);

      if ( key.equals(value.getComboKey())) return value;
      }
    
    return null;
    }
  
  /**
   * Combo boxex may ask how big the data value is
   * @return
   */
  int getKeyValueCount ()
    {
    return keyValueList.size();      
    }
  
  boolean isEmpty ()
    {
    return getKeyValueCount() <= 0;  
    }

  /**
   * Then they can get one by one the elements.
   * @param index
   * @return
   */
  ComboBoxKeyValue getKeyValue ( int index )  
    {
    return keyValueList.get(index);      
    }
  

public static class GenericKeyValue implements ComboBoxKeyValue
  {
  private final Object key;
  private final String value;
  
  public GenericKeyValue(int key, String value)
    {
    this.key = Integer.valueOf(key);
    this.value = value;
    }

  public GenericKeyValue(Object key, String value)
    {
    this.key = key;
    this.value = value;
    }
  
  public Object getComboKey()
    {
    return key;
    }

  public String getComboValue()
    {
    return value;
    }
  }


  }
