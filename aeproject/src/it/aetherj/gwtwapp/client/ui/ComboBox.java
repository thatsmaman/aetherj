/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.ListBox;

public class ComboBox extends ListBox
  {
  private final ComboBoxModel comboModel;
  private final ComboChangeHandler comboChangeHandler;
  
  private Object selected_key;
	private ChangeHandler userChangeHandler;
  
  public ComboBox(ComboBoxModel comboModel)
    {
    this.comboModel = comboModel;
    this.comboChangeHandler = new ComboChangeHandler();
    
    // register to receive data change events
    comboModel.registerComboBox(this);
    
    // register to receive change events
    addChangeHandler(comboChangeHandler);
    
    // if there is any data in the model then fill teh combo up
    fillCombo();
    }
	
	
	public ComboBox setChangeHandler ( ChangeHandler changeHandler )
		{
		userChangeHandler = changeHandler;
		return this;	
		}
	
	
  /**
   * This is needed to match the model, so the actual object are the same.
   * @param selected_key
   */
  public void setSelectedKey(int selected_key )
    {
    setSelectedKey(Integer.valueOf(selected_key));   
    }
  
  /**
   * The program decides what should be my selected key.
   * I should go into the model and find out what is the index
   * if nothing is selected then it should display nothing...
   * @param selected_key
   */
  public void setSelectedKey ( Object selected_key )
    {
    this.selected_key = selected_key;     
    refreshSelectedKey();
    }
  
  /**
   * At any time you can call this one to make sure the combo is showing the currently selected key
   */
  private void refreshSelectedKey ()
    {
    int new_index = comboModel.getKeyIndex(selected_key);
    
    if ( new_index < 0 )
      {
      // if the combo model is totally empty there is nothing to do, really.
      if ( comboModel.isEmpty ()) return;
      
      // we cannot find this key in the combo, it is safe to say that the combo gets on the first slot
      // No, really, what happens if I set a default key and at time of set I do not have the data ?
      // the default gets zapped, but later the data arrives and could be good 
      // However, if data arrives and still the key is not present I am selecting something that is not selected..
      }
    
    // this does not generate a selected change
    setSelectedIndex(new_index);
    }
  
  public Object getSelectedKey ()
    {
    return selected_key;      
    }
  
  public String getSelectedStringKey ()
    {
    if ( selected_key == null ) return null;
    
    if ( selected_key instanceof String  ) return (String)selected_key;
    
    return null;
    }
  
  public Integer getSelectedIntegerKey ()
    {
    if ( selected_key == null ) return null;
    
    if ( selected_key instanceof Integer  ) return (Integer)selected_key;
    
    return null;
    }
	
	public int getSelectedIntKey ( int defValue )
		{
    if ( selected_key == null ) return defValue;
    
    if ( ! ( selected_key instanceof Integer ) ) return defValue;
		
		return ((Integer)selected_key).intValue();
		}

  /**
   * If this combo selected key is an integere and is not null it will return its value
   * othervise it returns the default value
   * @param defaultValue
   * @return
   */
  public int getSelectedKey (int defaultValue )
    {
    Integer selected = getSelectedIntegerKey();
    
    if ( selected==null) return defaultValue;
    
    return selected.intValue();
    }

  /**
   * At any time you can call this one to fill the combo with the model values
   * At the end I MUST refresh the selected key, if there is a null key and a null value go there
   * otherwise go to the first valid value and set the selected key accordingly !
   */
  void fillCombo ()
    {
    super.clear();  // clear all items from the combo
    int item_count = comboModel.getKeyValueCount();
    for (int index=0; index<item_count; index++ )
      {
      ComboBoxKeyValue value = comboModel.getKeyValue(index);
      super.addItem(value.getComboValue());
      }
    
    refreshSelectedKey();
    }
  
  public void finalize() throws Throwable
    { 
    super.finalize();
    comboModel.removeComboBox(this);
    }
  
  public String toString()
    {
    return "combo id="+selected_key;      
    }

private final class ComboChangeHandler implements ChangeHandler
  {
  public void onChange(ChangeEvent changeEvent)
    {
    int index = getSelectedIndex();
    if ( index < 0 || index >=comboModel.getKeyValueCount() ) return;
    
    ComboBoxKeyValue keyval = comboModel.getKeyValue(index);
    if ( keyval == null ) return;
    
    selected_key = keyval.getComboKey();
		
		if ( userChangeHandler != null ) 
			{
			userChangeHandler.onChange(null);
			}
    }
  }
  
  
  }
