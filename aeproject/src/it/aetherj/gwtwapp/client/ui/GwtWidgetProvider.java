/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.user.client.ui.Widget;

/**
 * This is used to simplify the binding of layout without exposing the inner complexity
 * Copyright Damiano Bolla, Engineering Ideas S.n.c
 * @author damiano
 */
public interface GwtWidgetProvider
  {
  /**
   * A class that implements this interface has a generic handle to get what should be display
   * @return a cell that can be put into another panel
   */
  public Widget getPanelToDisplay();
  }
