/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import java.util.Date;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.beans.*;
import it.aetherj.gwtwapp.client.services.RewebSystemServiceAsync; 


/**
 * ALl my pages have the top part with the jump to the help and a generic label
 * I wish to construct something that handles that
 */
public abstract class RecliGenericCenterPanel extends AecliGenericRootPage
  {
  private static final String classname="RecliGenericCenterPanel";
  
  public static final int northPanelHeight=60;
  
  private final DockPanelSplit workPanel;    // panel to put pages into the center 
  
  private final FlowPanel topButtonPanel;      // pages can put buttons here
  private final String help_tag,page_desc;
  private final Image helpButton;
	
  protected PushButton getallButton,saveButton;      // so I have a single logic to check for enabled/visible

  private Widget northPanel;         // so i can resize it'
  private Date activityEndDate;      // the last activity date received from server
  
  /**
   * Only subclasses should be able to call constructor
   * @param stat
   * @param page_desc
   * @param help_tag
   */
  protected RecliGenericCenterPanel(RecliStat stat, String page_desc, String help_tag )
    {
    super(stat);
    
    this.page_desc = page_desc;
    this.help_tag = help_tag;
    
    this.helpButton = newHelpButton();
    this.topButtonPanel = newTopButtonPanel();          

    workPanel = newWorkPanel();
    }
  
  
  /**
   * In some way the caller knows that there is a last activity date referred to this "page"
   * It then ask to this page if the given date is newer than the one stored
   * This call also stores the given date as the current one
   * @param activityEndDate should not be null, if it is return will be false
   * @return true if the given date is newer than current stored one
   */
  public boolean isActivityDateLater ( Date lastActivityDate_in )
  	{
  	// It just means that the server never has made a request
  	if ( lastActivityDate_in == null ) return false;
  	
  	if ( this.activityEndDate == null )
  		{
  		this.activityEndDate = 	lastActivityDate_in;
  		// statistically speaking it is likely that the first time there is a new info the page is already "refreshed"
      stat.logOnBrowser(classname+"isActivityDateLater: lastAcivityDate was null");
      // so, it should be OK not to refresh
  		return false;
  		}
  	
  	long last_in = lastActivityDate_in.getTime();
  	long last_have = activityEndDate.getTime();
  	
  	// I can now store the received value
  	this.activityEndDate = 	lastActivityDate_in;

  	boolean risul = last_in > last_have;

//  	stat.logOnBrowser(classname+"isActivityDateLater: lastAcivityDate 'risul="+risul);
  	
  	return risul;
  	}
  
  
  /**
   * subclasses should return a valid ident .... hmmmm, I have to actually send the ident down
   * TODO: this should be clarified...
   * @return
   */
  public String getGwtIdent ()
  	{
  	return null;	
  	}

  
	protected void verifySaveGetallButtonVisible ()
		{
//		if ( getallButton != null )	getallButton.setVisible( ! stat.isReadOnly() );
			
		if ( saveButton != null )	saveButton.setVisible( ! stat.isReadOnly() );
		}

  private FlowPanel newTopButtonPanel ()
    {
    FlowPanel risul = new FlowPanel(); 
    risul.addStyleName("generic-top-button-panel");
    return risul;
    }
  
  private final Image newHelpButton()
    {
    Image risul = new Image(stat.imageBoundle.newHelp());
    risul.addClickHandler(new HelpButtonListener());
    return risul;
    }
  
  /**
   * You can use this one to parse the call end date and decide what to do next...
   */
  protected void parseCallEndDate (ServiceFeedback feedback)
    {
    if ( feedback == null ) return;
    
    Date callEndDate = feedback.getCallEndDate();
    
    if ( callEndDate == null ) return;
    
    if ( activityEndDate == null ) activityEndDate = callEndDate;
    }


  /**
   * Creates a Button that will refresh the page once clicked.
   * @return
   */
  protected Widget newRefreshButton ()
    {
    RefreshButtonListener listener = new RefreshButtonListener();
    Widget risul = stat.utils.newRefreshButton(listener);
    return risul;
    }
  


  private Widget newDescAndHelp ()
    {
    // apparently this is the only way to have things aligned properly, at least for now...
    Grid risul = new Grid(1,2);

    int rowIndex=0; int colIndex=0;
    risul.setWidget(rowIndex,colIndex++, getGwtLabelBold(page_desc));
    
    risul.setWidget(rowIndex,colIndex++,helpButton);  

    return risul;
    }

  
  private Widget newNorthPanel()
    {
    Grid risul = new Grid(1,2);
    risul.setWidth("100%");

    int rowIndex=0; int colIndex=0;
    risul.setWidget(rowIndex,colIndex++,topButtonPanel);

    risul.setWidget(rowIndex,colIndex++,newDescAndHelp());
    risul.getCellFormatter().setAlignment(0,1,HasHorizontalAlignment.ALIGN_RIGHT,HasVerticalAlignment.ALIGN_MIDDLE);
    
    return risul;    
    }

  private DockPanelSplit newWorkPanel()
    {
    DockPanelSplit risul = new DockPanelSplit(4);

    northPanel = newNorthPanel();

    risul.addNorth(northPanel, 80);
		
		// center panel will be added later

    return risul;
    }

  protected void setNorthPanelSize ( double size )
    {
    workPanel.setWidgetSize(northPanel, size);
    }

  /**
   * Anybody should be able to request that the page ask for data refresh from the server
   * this will start a non blocking request whose response will be handled later.
   */
  public abstract void postServerRefreshReq ();
  
  /**
   * Anybody should be able to ask for the gui page refresh, it should update the 
   * GUI with the data currently held in the implementation model.
   */
  public abstract void postGuiRefreshReq ();
  
  /**
   * Anybody should be able to clear the GUI, actually clearing the GUI means clearing the model
   * and then refresh the content. But the implementation can be done in any way you wish
   */
  public abstract void guiClear();

  /**
   * Allow to set the inner center panel
   */
  protected void setInnerCenterPanel(Widget centerWidget)
    {
    workPanel.setCenter(centerWidget);
    }
  
  protected void setInnerCenterPanel(GwtWidgetProvider panelProvider)
    {
		if ( panelProvider == null ) return;
		
    workPanel.setCenter(panelProvider.getPanelToDisplay());
    }

  /**
   * Add a widget to the top part area of the panel.
   * It could be a button, an image or an input box, what you want.
   * a style named top-generic-center-panel-button will be added to the widget
   * @param button
   */
  protected void addTopPanelWidget( Widget button )
    {
    button.addStyleName("top-generic-center-panel-button");
    topButtonPanel.add(button);
    }

  public final void postComboIntegerDataRefresh ( AegwComboReq req, RecliGenericCallback callback )
    {
    RewebSystemServiceAsync service = stat.serviceFactory.newSystemService();
    req.combo_key_type = AegwComboReq.key_type_integer;
    service.getComboList(req, callback);
    }
 
  @Override
  public Panel getPanelToDisplay()
    {
    return workPanel;
    }

  /**
	 * Queue a request for data on the given pgrp_id for the given range, it can be a single value
	 * @param pgtb_id
	 * @param data_range
  protected void newGetDataRequest (int pgtb_id, String data_range)
    {
    RewebPlcunitServiceAsync service = stat.serviceFactory.newPlcunitService();
    RewebPlcGetReq req = new RewebPlcGetReq(stat,pgtb_id,data_range);
    service.getPlcData(req, new PlcGetDataReadyCallback());
    }
   */

  /**
   * Queue a request for data on the given rpc_table for the given range, it can be a single value
   * NOTE: Data is transferred from CEMI to the DBASE
   * @param areq a request that will be handled differently depending on the called group
   */
  protected void newGetCemiDataRequest ( Object areq )
    {
//    RewebPlcemiServiceAsync service = stat.serviceFactory.newPlcemiService();
//    service.getPlcemiData(areq, new PlcGetDataReadyCallback());
    }

  
  protected final Integer getPortValue ( TextBox abox )
    {
    Integer risul = null;
    
    String port = abox.getText();
    if ( port.length() >= 1 )
      {
      char port_char = port.charAt(0);        
      risul = Integer.valueOf(port_char);
      }
      
    return risul;
    }
  
  

private class HelpButtonListener implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    Window.open(stat.serviceFactory.getPagehelpUrl(help_tag),"_blank","");
    }
  }




private class RefreshButtonListener implements ClickHandler
  {
  public void onClick(ClickEvent event)
    {
    postServerRefreshReq();
    }
  }

/*
private class PlcGetDataReadyCallback extends RecliGenericCallback
  {
  public void onSuccess(Object result)
    {
    RewebPlcGetRes res = (RewebPlcGetRes)result;

    if ( isErrorMessage(res)) return;

    GWT.log("request post ok ",null);
    }
  }
*/




  }
