/*******************************************************************************
 * Copyright (C) 2020-present Damiano Bolla, Cologna Veneta, IT
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package it.aetherj.gwtwapp.client.ui;

import com.google.gwt.user.client.ui.*;

import it.aetherj.gwtwapp.client.RecliStat;
import it.aetherj.gwtwapp.client.gui.*;


/**
 * The idea is that this provides a default loggedin page with all amenities.
 * The status of the application is in the stat that is given.
 * You can create many of this and still get the same situation
 * Copyright Damiano Bolla, Engineering Ideas S.n.c
 * @author Damiano
 */
public final class RecliLoggedinPage implements GwtWidgetProvider
  {
  private final RecliStat stat; 
	
	private final DockPanelLayout workPanel;            // Widgets should be added/ removed here

	private RecliGenericCenterPanel curCenterProvider=null;  // the provider for the widget, if any
  
  public RecliLoggedinPage(RecliStat stat)
    {
    this.stat = stat;

    stat.dummyPanel         = new AecliDummyPage(stat);
    stat.statusBar         = new AecliStatusBar(stat);
		stat.sysinfoPage       = new AecliInfoShow(stat);
																 
    // this MUST be last, after all the pages have been allocated
    stat.navigationButtons = new NavigationButtons(stat);
   
    workPanel = newWorkPanel();
    
    stat.sysinfoPage.refreshSysinfo(stat.login_res);
    
    // set a center page, this should also ask for menu on/off...
    setCenterPanel(stat.popularPanel);

    // request for refresh
    stat.popularPanel.postServerRefreshReq();
    }

  /**
   * returns the current defined WidgetIdent provider, whatever that is
   * @return may return null
   */
  public RecliGenericCenterPanel getCurrentCenterPage ()
  	{
  	return curCenterProvider;
  	}

  private DockPanelLayout newWorkPanel()
    {
    DockPanelLayout risul = new DockPanelLayout();

    Panel status = stat.statusBar.getPanelToDisplay();
    
    risul.addNorth(new ScrollPanel(status),AecliStatusBar.BAR_height_px);
    
    Panel navtree = stat.navigationButtons.getPanelToDisplay();
    
    risul.addSinistra(new ScrollPanel(navtree), 100);

    return risul;
    }

  @Override
  public Widget getPanelToDisplay()
    {
    return workPanel;
    }

  public void setCenterPanel(GwtWidgetProvider provider)
    {
		if ( provider == null ) return;
		
		Widget newWidget = provider.getPanelToDisplay();
		
    workPanel.setCenter(newWidget);
    
    curCenterProvider = null;
    }

  /**
   * Automagically use this when you have a generic
   * @param provider
   */
  public void setCenterPanel(RecliGenericCenterPanel provider)
    {
		if ( provider == null ) return;

		setCenterPanel( (GwtWidgetProvider) provider);

		curCenterProvider = provider;
    }

  
  
  
  }
